﻿
using System.Collections.Generic;

namespace Omnium.Business.Contracts
{
    public interface CompanyUpdatedEvent
    {
        string Id { get; }
        string CompanyCode { get; }
        string Name { get; }
        string Street { get; }
        string City { get; }
        string ZipCode { get; }
        string CountryId { get; }
        string Status { get; }
        public List<EntityPlatform> Platforms { get; }
    }
}
