﻿namespace Omnium.Business.Contracts
{
    public interface PlatformCreatedEvent
    {
        string Id { get; }
        string Name { get; }
        int PlatformType { get; }
        string IntegrationData { get; } 
        string Status { get; }
    }

}
