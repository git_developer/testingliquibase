﻿namespace Omnium.Business.Contracts
{
    public interface CountryUpdatedEvent
    {
        string Id { get; }
        string Name { get; }
        string ISO2 { get; }
        string ISO3 { get; }
        string Status { get; }
    }


}
