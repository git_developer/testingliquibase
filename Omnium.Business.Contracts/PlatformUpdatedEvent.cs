﻿namespace Omnium.Business.Contracts
{
    public interface PlatformUpdatedEvent
    {
        string Id { get; }
        string Name { get; }
        int PlatformType { get; }
        string IntegrationData { get; }
        string Status { get; }
    }

}
