﻿namespace Omnium.Business.Contracts
{
    public interface EntityPlatform
    {
        string EntityId { get; }
        string PlatformId { get; }
        string Status { get; }
    }
}
