-- liquibase formatted sql

--changeset bob:1
create table test_1(
id int primary key,
name varchar(255),
age varchar(50)
);
-- rollback drop table test_1;

--changeset bob:2
alter table test_1
alter column age type integer using (age::integer);
-- rollback alter table test_1 alter column age type varchar(50);

--changeset bob:3
insert into
test_1
values
(1, 'David Fernandes', 25),
(2, 'Donald Duck', 54),
(3, 'Joseph Stalin', 100);
-- rollback delete from test_1;

--changeset bob:4
insert into
test_1
values
(4, 'JJ', 65),
(5, 'John Doe', 54),
(6, 'Mary Doe', 44);
-- rollback delete from test_1 where ID=4;delete from test_1 where ID=5;delete from test_1 where ID=6;

--changeset bob:5
create table test_2(
id int primary key,
name varchar(255),
age varchar(50)
);
-- rollback drop table test_2;