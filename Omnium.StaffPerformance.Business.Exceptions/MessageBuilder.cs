﻿using Omnium.StaffPerformance.Business.Exceptions.Properties;

namespace Omnium.StaffPerformance.Business.Exceptions
{
    public enum ECode { RequiredField = 1000, InvalidField = 1001, Unexpected = 1002,
                        EntityXDoesNotExist = 1003, WrongDateFormat=2401, BadAuthentication=2402, POSError=2403, CompanyNotFound=2404, InvalidAttendanceType=2405, 
                        SameAttendanceType=2406, HolidayParametersInvalid=2407, ExtAPIError=2408, EntityXNotFound=2409, EntityXAlreadyExists=2410, AssociationXAlreadyExists=2411,
                        ScheduleIntersection=2412 }

    public static class MessageBuilder
    {
        public static string GetMessage(ECode pMessageCode, params string[] pParams)
        {
            var code = (int)pMessageCode;
            var messageCode = string.Format("str{0}", code);
            var message = string.Format(Resources.ResourceManager.GetString(messageCode), pParams);
            return message;
        }
    }
}
