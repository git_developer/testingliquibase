﻿
namespace Omnium.StaffPerformance.Business.Exceptions
{
    public class SameAttendanceTypeCustomException : CustomException
    {
        public SameAttendanceTypeCustomException() : base(ECode.SameAttendanceType) { }
    }
}
