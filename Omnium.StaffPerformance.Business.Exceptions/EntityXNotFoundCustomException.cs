﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Omnium.StaffPerformance.Business.Exceptions
{
    public class EntityXNotFoundCustomException : CustomException
    {
        public string Entity { get; private set; }
        public string Field { get; private set; }

        /// <summary>
        /// Creates a Entity Not Found Custom Exception
        /// </summary>
        /// <param name="pField">the field used to search</param>
        public EntityXNotFoundCustomException(string pEntity,string pField) : base(ECode.EntityXNotFound)
        {
            Entity = pEntity;
            Field = pField;
        }

        public override string Message => MessageBuilder.GetMessage(ECode.EntityXNotFound,Entity, Field);
    }
}
