﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Omnium.StaffPerformance.Business.Exceptions
{
    public class ScheduleIntersectionCustomException : CustomException
    {
        public ScheduleIntersectionCustomException() : base(ECode.ScheduleIntersection) { }
    }
}
