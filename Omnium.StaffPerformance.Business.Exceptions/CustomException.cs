﻿using System;

namespace Omnium.StaffPerformance.Business.Exceptions
{
    public abstract class CustomException : Exception
    {
        public ECode Code { get; private set; }

        internal CustomException(ECode pCode) : base()
        {
            Code = pCode;
        }

        internal CustomException(ECode pCode, string pMessage) : base(pMessage)
        {
            Code = pCode;
        }

        public override string Message
        {
            get
            {
                return MessageBuilder.GetMessage(Code);
            }
        }

    }

}
