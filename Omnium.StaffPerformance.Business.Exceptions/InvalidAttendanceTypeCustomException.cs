﻿
namespace Omnium.StaffPerformance.Business.Exceptions
{
    public class InvalidAttendanceTypeCustomException : CustomException
    {
        public InvalidAttendanceTypeCustomException() : base(ECode.InvalidAttendanceType) { }
    }
}
