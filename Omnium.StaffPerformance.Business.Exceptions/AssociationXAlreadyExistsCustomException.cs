﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Omnium.StaffPerformance.Business.Exceptions
{
    public class AssociationXAlreadyExistsCustomException : CustomException
    {
        public string Association { get; private set; }

        public AssociationXAlreadyExistsCustomException(string pAssociation) : base(ECode.AssociationXAlreadyExists)
        {
            Association = pAssociation;
        }
        public override string Message => MessageBuilder.GetMessage(ECode.AssociationXAlreadyExists, Association);
    }
}
