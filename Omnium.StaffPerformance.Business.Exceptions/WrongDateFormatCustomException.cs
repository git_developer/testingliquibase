﻿
namespace Omnium.StaffPerformance.Business.Exceptions
{
    public class WrongDateFormatCustomException : CustomException
    {
        public string myDate { get; private set; }
        public string myFormat { get; private set; }

        /// <summary>
        /// Creates a Wrong Date Format Custom Exception
        /// </summary>
        /// <param name="pDate">the date in the wrong format</param>
        /// <param name="pFormat">the desired format</param>
        public WrongDateFormatCustomException(string pDate, string pFormat) : base(ECode.WrongDateFormat) 
        {
            myDate = pDate;
            myFormat = pFormat;
        }

        public override string Message => MessageBuilder.GetMessage(ECode.WrongDateFormat, myDate, myFormat);
    }
}
