﻿
namespace Omnium.StaffPerformance.Business.Exceptions
{
    public class ExternalAPICustomException : CustomException
    {

        public string myPlatform { get; private set; }
        public string myErrorMessage { get; private set; }

        /// <summary>
        /// Creates an External API Custom Exception
        /// </summary>
        /// <param name="pPlatform">the platform where the exception occured</param>
        /// <param name="pErrorMessage">the error message coming from it</param>
        public ExternalAPICustomException(string pPlatform, string pErrorMessage) : base(ECode.ExtAPIError)
        {
            myPlatform = pPlatform;
            myErrorMessage = pErrorMessage;
        }

        public override string Message => MessageBuilder.GetMessage(ECode.ExtAPIError, myPlatform, myErrorMessage);
    }
}
