﻿
namespace Omnium.StaffPerformance.Business.Exceptions
{
    public class POSCustomException : CustomException
    {
        public string myPlatform { get; private set; }
        public string myErrorMessage { get; private set; }

        /// <summary>
        /// Creates a POS Custom Exception
        /// </summary>
        /// <param name="pPlatform">the platform where the exception occured</param>
        /// <param name="pErrorMessage">the error message coming from it</param>
        public POSCustomException(string pPlatform, string pErrorMessage) : base(ECode.POSError)
        {
            myPlatform = pPlatform;
            myErrorMessage = pErrorMessage;
        }

        public override string Message => MessageBuilder.GetMessage(ECode.POSError, myPlatform, myErrorMessage);
    }
}
