﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Omnium.StaffPerformance.Business.Exceptions
{
    public class EntityXAlreadyExistsCustomException : CustomException
    {
        public string Entity { get; private set; }

        public EntityXAlreadyExistsCustomException(string pEntity) : base(ECode.EntityXAlreadyExists)
        {
            Entity = pEntity;
        }
        public override string Message => MessageBuilder.GetMessage(ECode.EntityXAlreadyExists, Entity);
    }
}
