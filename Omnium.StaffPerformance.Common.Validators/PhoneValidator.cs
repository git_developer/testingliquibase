﻿namespace Omnium.StaffPerformance.Common.Validators
{
    /// <summary>
    /// Check is a phone or mobilephonenumber is valid
    /// </summary>
    internal class PhoneValidator : IValidator<string>
    {
        public bool IsValid(string pEntity)
        {
            return int.TryParse(pEntity, out _);
        }
    }

}
