﻿using System.Text.RegularExpressions;

namespace Omnium.StaffPerformance.Common.Validators
{
    /// <summary>
    /// Check is an email is valid
    /// </summary>
    internal class EmailValidator : IValidator<string>
    {
        public bool IsValid(string pEntity)
        {
            const string pattern = @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z";

            Regex regex = new Regex(pattern, RegexOptions.IgnoreCase);
            return regex.IsMatch(pEntity);
        }
    }

}
