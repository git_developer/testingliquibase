﻿
namespace Omnium.StaffPerformance.Common.Validators
{
    public static class ValidatorFactory
    {
        /// <summary>
        /// Checks if a Email is valid
        /// </summary>
        /// <param name="pEmail">to be validated</param>
        /// <returns></returns>
        public static bool IsEmailValid(string pEmail)
        {
            var validator = new EmailValidator();
            return validator.IsValid(pEmail);
        }

        /// <summary>
        /// Checks if a phone or mobile phone number is valid
        /// </summary>
        /// <param name="pPhoneOrMobilePhoneNumber">number to be validated</param>
        /// <returns></returns>
        public static bool IsPhoneOrMobilePhoneValid(string pPhoneOrMobilePhoneNumber)
        {
            var validator = new PhoneValidator();
            return validator.IsValid(pPhoneOrMobilePhoneNumber);
        }
  
    }
}
