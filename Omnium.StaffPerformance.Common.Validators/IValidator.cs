﻿
namespace Omnium.StaffPerformance.Common.Validators
{
    public interface IValidator<T>
    {
        /// <summary>
        /// Checks if a entity is valid. Returns true if is valid otherwise returns false
        /// </summary>
        /// <param name="pEntity">entity to be validated</param>
        /// <returns>Returns true if is valid otherwise returns false</returns>
        bool IsValid(T pEntity);
    }

}
