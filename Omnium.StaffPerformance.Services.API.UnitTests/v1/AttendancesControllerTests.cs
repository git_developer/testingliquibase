﻿using Microsoft.AspNetCore.Mvc;
using Moq;
using Omnium.StaffPerformance.Business.Domain.Entities;
using Omnium.StaffPerformance.Business.Domain.Repositories;
using Omnium.StaffPerformance.Business.Domain.Services.Attendances;
using Omnium.StaffPerformance.Business.Exceptions;
using Omnium.StaffPerformance.Services.API.Controllers.v1;
using Omnium.StaffPerformance.Services.API.Models;
using System;
using System.Collections.Generic;
using Xunit;

namespace Omnium.StaffPerformance.Services.API.UnitTests.v1
{
    public class AttendancesControllerTests
    {
        /// <summary>
        /// Ensures the POST endpoint returns the created attendance as AttendanceResponseDto and with the same information as the request
        /// </summary>
        [Fact]
        public void Post_CreatingAttendance_ReturnsAttendanceCreated()
        {
            var company = new Company("1", "1", "Omnium", "Street", "City", "zipcode", "testPt", "A");
            var dbAttendance = new Attendance(company,"test", new DateTime(2021,5,26,14,13,12),EAttendanceType.Exit,"A");

            var attendanceRepositoryMock = new Mock<IAttendanceRepository>();
            attendanceRepositoryMock.Setup(r => r.GetLastAttendanceByEmployeeId(It.IsAny<String>())).Returns(dbAttendance);

            var dbEmployee = new Employee(company, "Test Subject", "Test Subject", "M", "email", "967453648", "223451234", DateTime.Now, "I");

            var employeeRepositoryMock = new Mock<IEmployeeRepository>();
            employeeRepositoryMock.Setup(r => r.GetEmployeeById(It.IsAny<String>())).Returns(dbEmployee);

            var unitOfWorkMock = new Mock<IUnitOfWork>();
            unitOfWorkMock.Setup(r => r.Attendances).Returns(attendanceRepositoryMock.Object);
            unitOfWorkMock.Setup(r => r.Employees).Returns(employeeRepositoryMock.Object);

            var attendanceService = new AttendanceService(unitOfWorkMock.Object);

            var controller = new AttendancesController(attendanceService);

            var request = new AttendanceDto
            {
                EmployeeId = "test",
                Date="27/05/2021-14:13:12",
                Type=0
            };
            var response = controller.Post(request);
            var responseAsOkObjectResult = response.Result as OkObjectResult;

            Assert.NotNull(responseAsOkObjectResult.Value);
            Assert.IsType<AttendanceResponseDto>(responseAsOkObjectResult.Value);

            var responseValueAsResponseDto = (AttendanceResponseDto) responseAsOkObjectResult.Value;

            Assert.True(String.Equals(request.EmployeeId, responseValueAsResponseDto.EmployeeId));
            Assert.True(String.Equals(request.Date, responseValueAsResponseDto.Date));
            Assert.True(String.Equals(request.Type, responseValueAsResponseDto.Type));
        }
        /// <summary>
        /// Ensures the POST endpoint throws an exception when we create an attendance with a non existing employee
        /// </summary>
        [Fact]
        public void Post_CreatingAttendanceWithNonExistingEmployee_ThrowsAnException()
        {
            var company = new Company("1", "1", "Omnium", "Street", "City", "zipcode", "testPt", "A");
            var dbAttendance = new Attendance(company,"testShouldNotExist", new DateTime(2021, 5, 26, 14, 13, 12), EAttendanceType.Exit, "A");

            var attendanceRepositoryMock = new Mock<IAttendanceRepository>();
            attendanceRepositoryMock.Setup(r => r.GetLastAttendanceByEmployeeId(It.IsAny<String>())).Returns(dbAttendance);

            var employeeRepositoryMock = new Mock<IEmployeeRepository>();
            employeeRepositoryMock.Setup(r => r.GetEmployeeById(It.IsAny<String>())).Returns(null as Employee);

            var unitOfWorkMock = new Mock<IUnitOfWork>();
            unitOfWorkMock.Setup(r => r.Attendances).Returns(attendanceRepositoryMock.Object);
            unitOfWorkMock.Setup(r => r.Employees).Returns(employeeRepositoryMock.Object);

            var attendanceService = new AttendanceService(unitOfWorkMock.Object);

            var controller = new AttendancesController(attendanceService);

            var request = new AttendanceDto
            {
                EmployeeId = "testShouldNotExist",
                Date = "27/05/2021-14:13:12",
                Type = 0
            };
            Assert.Throws<EntityXNotFoundCustomException>(() => controller.Post(request));
        }
        /// <summary>
        /// Ensures the POST endpoint throws an exception when we create an attendance with an invalid type of attendance(not 0 or 1)
        /// </summary>
        [Fact]
        public void Post_CreatingAttendanceWithInvalidAttendanceType_ThrowsAnException()
        {
            var company = new Company("1", "1", "Omnium", "Street", "City", "zipcode", "testPt", "A");
            var dbAttendance = new Attendance(company,"test", new DateTime(2021, 5, 26, 14, 13, 12), EAttendanceType.Exit, "A");

            var attendanceRepositoryMock = new Mock<IAttendanceRepository>();
            attendanceRepositoryMock.Setup(r => r.GetLastAttendanceByEmployeeId(It.IsAny<String>())).Returns(dbAttendance);

            var dbEmployee = new Employee(company, "Test Subject", "Test Subject", "M", "email", "967453648", "223451234", DateTime.Now, "I");

            var employeeRepositoryMock = new Mock<IEmployeeRepository>();
            employeeRepositoryMock.Setup(r => r.GetEmployeeById(It.IsAny<String>())).Returns(dbEmployee);

            var unitOfWorkMock = new Mock<IUnitOfWork>();
            unitOfWorkMock.Setup(r => r.Attendances).Returns(attendanceRepositoryMock.Object);
            unitOfWorkMock.Setup(r => r.Employees).Returns(employeeRepositoryMock.Object);

            var attendanceService = new AttendanceService(unitOfWorkMock.Object);

            var controller = new AttendancesController(attendanceService);

            var request = new AttendanceDto
            {
                EmployeeId = "test",
                Date = "27/05/2021-14:13:12",
                Type = 23
            };
            Assert.Throws<InvalidAttendanceTypeCustomException>(() => controller.Post(request));
        }
        /// <summary>
        /// Ensures the POST endpoint throws an exception when we create an attendance with the same attendance type as last attendance of that employee
        /// </summary>
        [Fact]
        public void Post_CreatingAttendanceWithTheSameAttendanceTypeAsLastAttendance_ThrowsAnException()
        {
            var company = new Company("1", "1", "Omnium", "Street", "City", "zipcode", "testPt", "A");
            var dbAttendance = new Attendance(company,"test", new DateTime(2021, 5, 26, 14, 13, 12), EAttendanceType.Exit, "A");

            var attendanceRepositoryMock = new Mock<IAttendanceRepository>();
            attendanceRepositoryMock.Setup(r => r.GetLastAttendanceByEmployeeId(It.IsAny<String>())).Returns(dbAttendance);

            var dbEmployee = new Employee(company, "Test Subject", "Test Subject", "M", "email", "967453648", "223451234", DateTime.Now, "I");

            var employeeRepositoryMock = new Mock<IEmployeeRepository>();
            employeeRepositoryMock.Setup(r => r.GetEmployeeById(It.IsAny<String>())).Returns(dbEmployee);

            var unitOfWorkMock = new Mock<IUnitOfWork>();
            unitOfWorkMock.Setup(r => r.Attendances).Returns(attendanceRepositoryMock.Object);
            unitOfWorkMock.Setup(r => r.Employees).Returns(employeeRepositoryMock.Object);

            var attendanceService = new AttendanceService(unitOfWorkMock.Object);

            var controller = new AttendancesController(attendanceService);

            var request = new AttendanceDto
            {
                EmployeeId = "test",
                Date = "27/05/2021-14:13:12",
                Type = 1
            };
            Assert.Throws<SameAttendanceTypeCustomException>(() => controller.Post(request));
        }

        //GET Endpoint

        /// <summary>
        /// Ensures the correct functioning of the get endpoint when given a valid interval and empoyeeId with existing records of attendances in that period
        /// </summary>
        [Fact]
        public void Get_GetAttendancesWithinInterval_ReturnsAttendanceList()
        {
            var company = new Company("1", "1", "Omnium", "Street", "City", "zipcode", "testPt", "A");

            var dbAttendance1 = new Attendance(company,"test", new DateTime(2021, 5, 26, 14, 13, 12), EAttendanceType.Entry, "A");
            var dbAttendance2 = new Attendance(company,"test", new DateTime(2021, 5, 26, 15, 13, 12), EAttendanceType.Exit, "A");

            List<Attendance> listReturn = new List<Attendance>();
            listReturn.Add(dbAttendance1);
            listReturn.Add(dbAttendance2);

            var attendanceRepositoryMock = new Mock<IAttendanceRepository>();
            attendanceRepositoryMock.Setup(r => r.GetAttendancesByEmployeeId(It.IsAny<String>())).Returns(listReturn);

            var dbEmployee = new Employee(company, "Test Subject", "Test Subject", "M", "email", "967453648", "223451234", DateTime.Now, "I");

            var employeeRepositoryMock = new Mock<IEmployeeRepository>();
            employeeRepositoryMock.Setup(r => r.GetEmployeeById(It.IsAny<String>())).Returns(dbEmployee);

            var unitOfWorkMock = new Mock<IUnitOfWork>();
            unitOfWorkMock.Setup(r => r.Attendances).Returns(attendanceRepositoryMock.Object);
            unitOfWorkMock.Setup(r => r.Employees).Returns(employeeRepositoryMock.Object);

            var attendanceService = new AttendanceService(unitOfWorkMock.Object);

            var controller = new AttendancesController(attendanceService);

            var response1= new AttendanceResponseDto
            {
                EmployeeId = "test",
                Date = "26/05/2021-14:13:12",
                Type = 0,
                Status="A"
            };
            var response2 = new AttendanceResponseDto
            {
                EmployeeId = "test",
                Date = "26/05/2021-15:13:12",
                Type = 1,
                Status="A"
            };
            List<AttendanceResponseDto> listResponses = new List<AttendanceResponseDto>();
            listResponses.Add(response1);
            listResponses.Add(response2);

            var response = controller.Get("test","22/05/2021-12:12:12", "31/05/2021-12:12:12");
            var responseAsOkObjectResult = response.Result as OkObjectResult;

            Assert.NotNull(responseAsOkObjectResult.Value);
            Assert.IsType<List<AttendanceResponseDto>>(responseAsOkObjectResult.Value);

            var responseValueAsResponseDto = (List<AttendanceResponseDto>)responseAsOkObjectResult.Value;
            int pos = 0;

            foreach(var r in listResponses)
            {
                Assert.True(String.Equals(r.EmployeeId, responseValueAsResponseDto[pos].EmployeeId));
                Assert.True(String.Equals(r.Date, responseValueAsResponseDto[pos].Date));
                Assert.True(String.Equals(r.Type, responseValueAsResponseDto[pos].Type));
                Assert.True(String.Equals(r.Status, responseValueAsResponseDto[pos].Status));
                pos++;
            }  
        }
        /// <summary>
        /// Ensures the get endpoint when given an interval with the start date after the end date will return an empty list always
        /// </summary>
        [Fact]
        public void Get_GetAttendancesWithinIntervalWithStartDateAfterEndDate_ReturnsEmptyAttendanceList()
        {
            var company = new Company("1", "1", "Omnium", "Street", "City", "zipcode", "testPt", "A");
            var dbAttendance1 = new Attendance(company,"test", new DateTime(2021, 5, 26, 14, 13, 12), EAttendanceType.Entry, "A");
            var dbAttendance2 = new Attendance(company,"test", new DateTime(2021, 5, 26, 15, 13, 12), EAttendanceType.Exit, "A");

            List<Attendance> listReturn = new List<Attendance>();
            listReturn.Add(dbAttendance1);
            listReturn.Add(dbAttendance2);

            var attendanceRepositoryMock = new Mock<IAttendanceRepository>();
            attendanceRepositoryMock.Setup(r => r.GetAttendancesByEmployeeId(It.IsAny<String>())).Returns(listReturn);

            var dbEmployee = new Employee(company, "Test Subject", "Test Subject", "M", "email", "967453648", "223451234", DateTime.Now, "I");

            var employeeRepositoryMock = new Mock<IEmployeeRepository>();
            employeeRepositoryMock.Setup(r => r.GetEmployeeById(It.IsAny<String>())).Returns(dbEmployee);

            var unitOfWorkMock = new Mock<IUnitOfWork>();
            unitOfWorkMock.Setup(r => r.Attendances).Returns(attendanceRepositoryMock.Object);
            unitOfWorkMock.Setup(r => r.Employees).Returns(employeeRepositoryMock.Object);

            var attendanceService = new AttendanceService(unitOfWorkMock.Object);

            var controller = new AttendancesController(attendanceService);

            var response = controller.Get("test", "31/05/2021-12:12:12", "22/05/2021-12:12:12");
            var responseAsOkObjectResult = response.Result as OkObjectResult;

            Assert.IsType<List<AttendanceResponseDto>>(responseAsOkObjectResult.Value);

            var responseAsList = (List<AttendanceResponseDto>)responseAsOkObjectResult.Value;

            Assert.True(responseAsList.Count==0);

        }
        /// <summary>
        /// Ensures the Get endpoint throws an exception when the given employeeId does not represent a valid employee
        /// </summary>
        [Fact]
        public void Get_GetAttendancesWithNonExistingEmployee_ThrowsException()
        {
            var employeeRepositoryMock = new Mock<IEmployeeRepository>();
            employeeRepositoryMock.Setup(r => r.GetEmployeeById(It.IsAny<String>())).Returns(null as Employee);

            var unitOfWorkMock = new Mock<IUnitOfWork>();
            unitOfWorkMock.Setup(r => r.Employees).Returns(employeeRepositoryMock.Object);

            var attendanceService = new AttendanceService(unitOfWorkMock.Object);

            var controller = new AttendancesController(attendanceService);

            Assert.Throws<EntityXNotFoundCustomException>(() => controller.Get("test", "22/05/2021-12:12:12", "22/05/2021-12:12:12"));
        }
    }
}
