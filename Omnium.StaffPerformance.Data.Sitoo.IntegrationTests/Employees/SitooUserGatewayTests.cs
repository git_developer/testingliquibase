﻿using Omnium.StaffPerformance.Business.Domain.Entities;
using Omnium.StaffPerformance.Business.Exceptions;
using Omnium.StaffPerformance.Data.AWS.SecretManagers;
using Omnium.StaffPerformance.Data.Sitoo.Employees;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace Omnium.StaffPerformance.Data.Sitoo.IntegrationTests.Employees
{
    public class SitooUserGatewayTests
    {

        /// <summary>
        /// Ensures that a valid user is created in sitoo
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async Task CreateUserAsync_CreateValidUser_ReturnUserId()
        {
            var tuple = await GetSitooConfigurations();

            var productGateway = new SitooUserGateway(tuple.Item1, tuple.Item2);

            var company = new Company("1", "1", "Omnium", "Street", "City", "zipcode", "testPt", "A");

            var employee = new Employee(company, "Test Subject", "Test Subject", "M", GenerateRandomEmail(), "967453648", "223451234", DateTime.Now, "I");

            var userId = await productGateway.CreateUserAsync(employee);

            Assert.NotNull(userId);
        }

        /// <summary>
        /// Ensures that a valid user created with invalid tokens throw an exception
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async Task CreateUserAsync_CreateValidUserWithInvalidToken_ThrowsException()
        {
            var tuple = await GetSitooConfigurations();

            var productGateway = new SitooUserGateway(tuple.Item1, "TestToken");

            var company = new Company("1", "1", "Omnium", "Street", "City", "zipcode", "testPt", "A");

            var employee = new Employee(company, "Test Subject", "Test Subject", "M", GenerateRandomEmail(), "967453648", "223451234", DateTime.Now, "I");

            await Assert.ThrowsAsync<POSCustomException>(() => productGateway.CreateUserAsync(employee));
        }

        /// <summary>
        /// Ensures that a valid user created with Short name having a single name throw an exception
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async Task CreateUserAsync_CreateValidUserWithInvalidShortName_ThrowsException()
        {
            var tuple = await GetSitooConfigurations();

            var productGateway = new SitooUserGateway(tuple.Item1, "TestToken");

            var company = new Company("1", "1", "Omnium", "Street", "City", "zipcode", "testPt", "A");

            var employee = new Employee(company, "TestSubject", "Test Subject", "M", GenerateRandomEmail(), "967453648", "223451234", DateTime.Now, "I");

            await Assert.ThrowsAsync<IndexOutOfRangeException>(() => productGateway.CreateUserAsync(employee));
        }

        public string GenerateRandomEmail()
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

            Random random = new Random();

            var charArray = Enumerable.Repeat(chars, 5).Select(s => s[random.Next(s.Length)]).ToArray();

            var nameAndDomain = new string(charArray);

            return $"{nameAndDomain}@{nameAndDomain}.com";

        }

        /// <summary>
        /// Returns from the AWS SecretManager the Sitoo Configurations such as BaseURL, APIKey
        /// </summary>
        /// <returns></returns>
        private async Task<Tuple<string, string>> GetSitooConfigurations()
        {
            var secretManager = new AWSSecretManagerCacheGateway("DevProductsServiceSecrets");
            var secret = await secretManager.GetSecretAsync();
            var jsonDocument = System.Text.Json.JsonDocument.Parse(secret);
            var tuple = Tuple.Create(jsonDocument.RootElement.GetProperty("SitooBaseURL").GetString(), jsonDocument.RootElement.GetProperty("SitooAPIKey").GetString());

            return tuple;
        }

    }
}
