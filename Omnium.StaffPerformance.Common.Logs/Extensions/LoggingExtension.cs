﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Omnium.StaffPerformance.Common.Logs.Enrichers;
using Omnium.StaffPerformance.Common.Logs.JsonHelpers;
using Omnium.StaffPerformance.Common.Logs.Models;
using Serilog;
using Serilog.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Omnium.StaffPerformance.Common.Logs.Extensions
{
    public static class LoggingExtensions
    {
        private static Dictionary<string, string> SENSITIVE_FIELDS;

        /// <summary>
        /// Adds the LogId encricher
        /// </summary>
        /// <param name="pEnrich"></param>
        /// <returns></returns>
        public static LoggerConfiguration WithLogId(this LoggerEnrichmentConfiguration pEnrich)
        {
            if (pEnrich == null)
            {
                throw new ArgumentNullException(nameof(pEnrich));
            }

            return pEnrich.With<LogIdLogEventEnricher>();
        }

        /// <summary>
        /// Adds the Code encricher
        /// </summary>
        /// <param name="pEnrich"></param>
        /// <returns></returns>
        public static LoggerConfiguration WithCode(this LoggerEnrichmentConfiguration pEnrich)
        {
            if (pEnrich == null)
            {
                throw new ArgumentNullException(nameof(pEnrich));
            }

            return pEnrich.With<CodeLogEventEnricher>();
        }

        /// <summary>
        ///  Initialize the Serilog log property
        /// </summary>
        /// <param name="pServiceCollection"></param>
        /// <param name="pConfiguration"></param>
        public static void AddSerilog(this IServiceCollection pServiceCollection, IConfiguration pConfiguration)
        {
            Log.Logger = new LoggerConfiguration()
                       .ReadFrom.Configuration(pConfiguration)
                       .CreateLogger();
        }

        /// <summary>
        /// Adds the fields that must not be serialize to the logs
        /// </summary>
        /// <param name="pConfiguration">Application configuration properties.</param>
        /// <param name="pSentiveFieldsKey">Key for the Sensitive Fields by default is 'SensitiveFields'</param>
        public static void AddSensitiveFields(this IServiceCollection pServiceCollection, IConfiguration pConfiguration, string pSentiveFieldsKey = "SensitiveFields")
        {
            SENSITIVE_FIELDS = pConfiguration.GetSection(pSentiveFieldsKey).Get<Dictionary<string, string>>() ?? new Dictionary<string, string>();
        }

        /// <summary>
        /// Writes a log event with sensitive information with log information level
        /// </summary>
        /// <param name="pLevel">log level</param>
        /// <param name="pLogData">data to log</param>
        public static void LogSensitiveInformation(this Serilog.ILogger pLogger, LogData pLogData)
        {
            const int MAX_DEPTH = 5;

            var builder = new StringBuilder();

            using (var stringWriter = new StringWriter(builder))
            using (var jsonWriter = new DepthJsonWriter(stringWriter))
            {

                bool include() => jsonWriter.CurrentDepth <= MAX_DEPTH;
                var serializerSettings = new JsonSerializerSettings
                {
                    ContractResolver = new SensitiveJsonContractResolver(SENSITIVE_FIELDS, include),
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                    Formatting = Formatting.None
                };

                var serializer = JsonSerializer.CreateDefault(serializerSettings);
                serializer.Serialize(jsonWriter, pLogData);

                var json = builder.ToString();
                pLogger.Information("{Data}", json);
            }

        }

        /// <summary>
        /// Writes a log event with sensitive information with log error level
        /// </summary>
        /// <param name="pLevel">log level</param>
        /// <param name="pLogData">data to offuscated</param>
        public static void LogSensitiveError(this Serilog.ILogger pLogger, LogData pLogData)
        {
            const int MAX_DEPTH = 5;

            var builder = new StringBuilder();

            using (var stringWriter = new StringWriter(builder))
            using (var jsonWriter = new DepthJsonWriter(stringWriter))
            {
                bool include() => jsonWriter.CurrentDepth <= MAX_DEPTH;
                var serializerSettings = new JsonSerializerSettings { ContractResolver = new SensitiveJsonContractResolver(SENSITIVE_FIELDS, include), ReferenceLoopHandling = ReferenceLoopHandling.Ignore, MaxDepth = 2, Formatting = Formatting.None };
                var serializer = JsonSerializer.CreateDefault(serializerSettings);

                serializer.Serialize(jsonWriter, pLogData);

                var json = builder.ToString();
                pLogger.Error("{Data}", json);
            }
        }

    }
}
