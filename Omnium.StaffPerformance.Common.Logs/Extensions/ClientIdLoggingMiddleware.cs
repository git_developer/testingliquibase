﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Primitives;
using Serilog.Context;
using System;
using System.Threading.Tasks;

namespace Omnium.StaffPerformance.Common.Logs.Extensions
{
    public class ClientIdLoggingMiddleware
    {
        private const string CLIENT_ID = "ClientId";

        private readonly RequestDelegate myNext;

        public ClientIdLoggingMiddleware(RequestDelegate pNext)
        {
            myNext = pNext ?? throw new ArgumentNullException(nameof(pNext));
        }

        public async Task Invoke(HttpContext context)
        {
            //Depending of the Authentication method this could change
            context.Request.Headers.TryGetValue(CLIENT_ID, out StringValues values);
            var clientId = values.Count > 0 ? values[0] : null;

            using (LogContext.PushProperty(CLIENT_ID, clientId))
            {
                await myNext.Invoke(context);
            }

        }
    }
}
