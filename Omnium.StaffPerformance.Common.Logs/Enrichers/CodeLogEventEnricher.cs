﻿using Serilog.Core;
using Serilog.Events;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace Omnium.StaffPerformance.Common.Logs.Enrichers
{
    public class CodeLogEventEnricher : ILogEventEnricher
    {
        public const string PropertyName = "Code";

        /// <summary>
        /// Enrich the log event.
        /// </summary>
        /// <param name="pLogEvent">The log event to enrich.</param>
        /// <param name="pPropertyFactory">Factory for creating new properties to add to the event.</param>
        public void Enrich(LogEvent pLogEvent, ILogEventPropertyFactory pPropertyFactory)
        {
            pLogEvent.AddPropertyIfAbsent(GetLogEventProperty(pPropertyFactory));
        }

        private LogEventProperty GetLogEventProperty(ILogEventPropertyFactory pPropertyFactory)
        {
            return CreateProperty(pPropertyFactory);
        }

        [MethodImpl(MethodImplOptions.NoInlining)]
        private static LogEventProperty CreateProperty(ILogEventPropertyFactory pPropertyFactory)
        {
            var assemblyName = Assembly.GetExecutingAssembly().GetName().Name;
            var value = assemblyName.Split('.')[1];
            return pPropertyFactory.CreateProperty(PropertyName, value);
        }

    }
}
