﻿using Omnium.StaffPerformance.Common.Logs.Extensions;
using Omnium.StaffPerformance.Common.Logs.Models;
using PostSharp.Aspects;
using PostSharp.Serialization;
using Serilog;


namespace Omnium.StaffPerformance.Common.Logs.Aspects
{
    
    [PSerializable]
    public class LoggingAspect : OnMethodBoundaryAspect
    {
        private LogData myLogStructure = new LogData();
        public override void OnSuccess(MethodExecutionArgs args)
        {
            myLogStructure.PayloadEntry = args.Arguments;
            myLogStructure.PayloadReturn = args.ReturnValue;
            myLogStructure.Description = $"Method {args.Method.Name} in {args.Method.DeclaringType.FullName} was executed with sucess.";

            Log.Logger.LogSensitiveInformation(myLogStructure);
        }

        public override void OnException(MethodExecutionArgs args)
        {
            myLogStructure.PayloadEntry = args.Arguments;
            myLogStructure.PayloadReturn = args.ReturnValue;
            myLogStructure.Description = $"An exception was thrown in {args.Method.Name} in {args.Method.DeclaringType.FullName}.The exception was {args.Exception.GetType()} the stack trace is {args.Exception.StackTrace}";

            Log.Logger.LogSensitiveError(myLogStructure);
        }
    }
}
