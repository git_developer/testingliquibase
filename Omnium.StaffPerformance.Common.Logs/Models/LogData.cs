﻿using Newtonsoft.Json;
using PostSharp.Serialization;

namespace Omnium.StaffPerformance.Common.Logs.Models
{
    [PSerializable]
    public struct LogData
    {
        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("payload_entry")]
        public object PayloadEntry { get; set; }

        [JsonProperty("payload_return")]
        public object PayloadReturn { get; set; }
    }
}
