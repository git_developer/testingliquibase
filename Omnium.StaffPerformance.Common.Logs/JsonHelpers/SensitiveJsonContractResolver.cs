﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace Omnium.StaffPerformance.Common.Logs.JsonHelpers
{
    public class SensitiveJsonContractResolver : DefaultContractResolver
    {
        private readonly Dictionary<string, string> mySensitivesFields;
        private readonly Func<bool> myIncludeProperty;

        public SensitiveJsonContractResolver(Dictionary<string, string> pSensitivesFields, Func<bool> includeProperty)
        {
            myIncludeProperty = includeProperty ?? throw new ArgumentNullException(nameof(includeProperty));
            mySensitivesFields = pSensitivesFields ?? new Dictionary<string, string>();
        }

        /// <summary>
        ///  Creates a Newtonsoft.Json.Serialization.JsonProperty for the given System.Reflection.MemberInfo.
        ///  Is the property is considered sensitive the value will be replace by a placeholder
        /// </summary>
        /// <param name="member">The member to create a Newtonsoft.Json.Serialization.JsonProperty for.</param>
        /// <param name="memberSerialization">The member's parent Newtonsoft.Json.MemberSerialization.</param>
        /// <returns></returns>
        protected override JsonProperty CreateProperty(MemberInfo member, MemberSerialization memberSerialization)
        {
            var jProperty = base.CreateProperty(member, memberSerialization);

            var shouldSerialize = jProperty.ShouldSerialize;
            jProperty.ShouldSerialize = obj => myIncludeProperty() && (shouldSerialize == null || shouldSerialize(obj));

            var isInvalid = IsSensitive(member.Name);

            if (isInvalid)
            {
                jProperty.ValueProvider = new SensitiveValueProvider(member as PropertyInfo);
            }

            return jProperty;
        }

        private bool IsSensitive(string pPropertyName)
        {
            return mySensitivesFields.ContainsKey(pPropertyName.ToLower());
        }

    }
}
