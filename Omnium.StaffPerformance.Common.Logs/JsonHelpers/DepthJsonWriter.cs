﻿using Newtonsoft.Json;
using System.IO;

namespace Omnium.StaffPerformance.Common.Logs.JsonHelpers
{
    public class DepthJsonWriter : JsonTextWriter
    {
        public DepthJsonWriter(TextWriter textWriter) : base(textWriter) { }

        public int CurrentDepth { get; private set; }

        public override void WriteStartObject()
        {
            CurrentDepth++;
            base.WriteStartObject();
        }

        public override void WriteEndObject()
        {
            CurrentDepth--;
            base.WriteEndObject();
        }
    }
}
