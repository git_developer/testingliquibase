﻿using Newtonsoft.Json.Serialization;
using System.Reflection;

namespace Omnium.StaffPerformance.Common.Logs.JsonHelpers
{
    /// <summary>
    ///  Provides methods to get the values of sensitive data
    /// </summary>
    internal class SensitiveValueProvider : IValueProvider
    {
        private const string PLACEHOLDER_STRING = "XXXXXX";

        private readonly PropertyInfo myTargetProperty;

        public SensitiveValueProvider(PropertyInfo targetProperty)
        {
            myTargetProperty = targetProperty;
        }

        public void SetValue(object target, object value)
        {
            myTargetProperty.SetValue(target, value);
        }

        public object GetValue(object target)
        {
            return PLACEHOLDER_STRING;
        }
    }
}
