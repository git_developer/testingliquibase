﻿
namespace Omnium.StaffPerformance.Data.Sitoo.Models
{
    public struct SitooError
    {
        public int StatusCode { get; set; }
        public string ErrorText { get; set; }
    }
}
