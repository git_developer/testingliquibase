﻿
namespace Omnium.StaffPerformance.Data.Sitoo.Models
{
    public struct SitooUserResponse
    {
        public int StatusCode { get; set; }
        public string Return { get; set; }
        public string ErrorText { get; set; }
    }
}
