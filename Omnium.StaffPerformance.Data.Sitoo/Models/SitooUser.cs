﻿
namespace Omnium.StaffPerformance.Data.Sitoo.Models
{
    public struct SitooUser
    {   
        public string Email { get; set; }
        public string Externalid { get; set; }
        public string NameFirst { get; set; }
        public string NameLast { get; set; }
        public string Mobile { get; set; }
        public string Phone { get; set; }
        public string Notes { get; set; }
        public string CompanyId { get; set; }
    }
}
