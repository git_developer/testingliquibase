﻿using Omnium.StaffPerformance.Business.Domain.Contracts;
using Omnium.StaffPerformance.Business.Domain.Entities;
using Omnium.StaffPerformance.Business.Exceptions;
using Omnium.StaffPerformance.Data.Sitoo.JsonPolicies;
using Omnium.StaffPerformance.Data.Sitoo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Json;
using System.Text.Json;
using System.Threading.Tasks;

namespace Omnium.StaffPerformance.Data.Sitoo.Employees
{
    public class SitooUserGateway : IUserGateway
    {

        private readonly string myBaseURL;
        private readonly string myAPIKey;
        /// <summary>
        /// Creates a Sitoo User Gateway instance
        /// </summary>
        /// <param name="pBaseURL">Base URL for Sitoo's endpoints</param>
        /// <param name="pAPIKey">API token used for accessing the API</param>
        public SitooUserGateway(string pBaseURL, string pAPIKey)
        {
            myBaseURL= string.IsNullOrEmpty(pBaseURL) ? throw new ArgumentNullException(nameof(pBaseURL)) : pBaseURL;
            myAPIKey = string.IsNullOrEmpty(pAPIKey) ? throw new ArgumentNullException(nameof(pAPIKey)) : pAPIKey;
        }

        /// <summary>
        /// Creates an user in Sitoo
        /// </summary>
        /// <param name="pEmployee">Employee object to be created in Sitoo</param>
        /// <returns>Created user ID</returns>
        public async Task<string> CreateUserAsync(Employee pEmployee)
        {
            var sitooUserId = string.Empty;

            using (var httpClient= new HttpClient())
            {
                httpClient.BaseAddress = new Uri(myBaseURL);

                httpClient.DefaultRequestHeaders.Add("Authorization", $"Basic {myAPIKey}");

                var configurations = GetPOSConfigurations();

                var namesplit = pEmployee.NameShort.Split(' ', 2);

                var sitooUser = new SitooUser
                {
                    Email = pEmployee.Email,
                    NameFirst = namesplit[0],
                    NameLast = namesplit[1],
                    Mobile = pEmployee.MobilePhoneNumber,
                    Phone = pEmployee.PhoneNumber,
                    Notes = pEmployee.Gender,
                    CompanyId= pEmployee.CompanyId
                };

                var payload = new SitooUser[] { sitooUser };

                var jsonOptions = new JsonSerializerOptions()
                {
                    PropertyNamingPolicy = new LowerCaseJsonNamingPolicy()
                };

                var companyId = configurations["CompanyId"];

                var httpResponse = await httpClient.PostAsJsonAsync($"sites/{companyId}/users.json",payload, jsonOptions);

                if (httpResponse.IsSuccessStatusCode)
                {
                    var response = await httpResponse.Content.ReadFromJsonAsync<IEnumerable<SitooUserResponse>>();

                    if (response.FirstOrDefault().StatusCode == (int)HttpStatusCode.OK)
                    {
                        var sitooResponseId = response.FirstOrDefault().Return;
                        sitooUserId = sitooResponseId.ToString();
                    }
                    else
                    {
                        var error = new SitooError();
                        error.StatusCode = 400;
                        error.ErrorText = response.FirstOrDefault().ErrorText;
                        throw new POSCustomException("Sitoo", error.ErrorText);
                    }
                }
                else
                {
                    var error = await GetErrorAsync(httpResponse);
                    throw new POSCustomException("Sitoo", error.ErrorText);
                }
            }
            return sitooUserId;
        }

        /// <summary>
        /// Turns the status code into an error to be returned 
        /// </summary>
        /// <param name="pHttpResponseMessage">Response from Sitoo API</param>
        /// <returns>Error model</returns>
        private async Task<SitooError> GetErrorAsync(HttpResponseMessage pHttpResponseMessage)
        {
            var error = new SitooError();

            switch (pHttpResponseMessage.StatusCode)
            {
                case HttpStatusCode.BadRequest:
                    error = await pHttpResponseMessage.Content.ReadFromJsonAsync<SitooError>();
                    error.StatusCode = 400;
                    break;

                case HttpStatusCode.Unauthorized:
                    error.ErrorText = MessageBuilder.GetMessage(ECode.BadAuthentication);
                    error.StatusCode = 401;
                    break;

                case HttpStatusCode.NotFound:
                    error.ErrorText = MessageBuilder.GetMessage(ECode.CompanyNotFound);
                    error.StatusCode = 404;
                    break;

                case HttpStatusCode.InternalServerError:
                    error.ErrorText = "Internal Server Error";
                    error.StatusCode = 500;
                    break;
            }

            return error;
        }

        /// <summary>
        /// Creates the POS configuration to be used in the API
        /// </summary>
        /// <returns>Dictionary with the configurations</returns>
        private Dictionary<string,string> GetPOSConfigurations()
        {
            //hardcoded value while waiting for the static reference module developments
            var configurations = new Dictionary<string, string>
            {
                { "CompanyId", "1" }
            };

            return configurations;
        }
    }
}
