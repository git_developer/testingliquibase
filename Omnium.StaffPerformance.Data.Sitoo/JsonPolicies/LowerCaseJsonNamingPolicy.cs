﻿using System.Text.Json;

namespace Omnium.StaffPerformance.Data.Sitoo.JsonPolicies
{
    public class LowerCaseJsonNamingPolicy : JsonNamingPolicy
    {
        public override string ConvertName(string name) => name.ToLower();
    }
}
