﻿using Omnium.StaffPerformance.Business.Domain.Contracts;
using Omnium.StaffPerformance.Business.Domain.Entities;
using Omnium.StaffPerformance.Business.Exceptions;
using Omnium.StaffPerformance.Data.Nager.JsonPolicies;
using Omnium.StaffPerformance.Data.Nager.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net;
using System.Net.Http;
using System.Net.Http.Json;
using System.Text.Json;
using System.Threading.Tasks;

namespace Omnium.StaffPerformance.Data.Nager.Holidays
{
    public class NagerHolidayGateway: IHolidayGateway
    {

        private readonly string myBaseUrl;
        /// <summary>
        /// Creates a Nager Holiday Gateway Instance
        /// </summary>
        /// <param name="pBaseUrl">Base URL for Nager's Holiday API</param>
        public NagerHolidayGateway(string pBaseUrl)
        {
            myBaseUrl = pBaseUrl;
        }
        /// <summary>
        /// Gets the existing holidays from a country in a certain year and returns the list of them
        /// </summary>
        /// <param name="pYear">year we want the holidays to be in</param>
        /// <param name="pCountry">Country of the holidays we want to return</param>
        /// <returns>List with the holiday information</returns>
        public async Task<List<Holiday>> GetHolidaysAsync(int pYear, string pCountry)
        {
            using(var httpClient= new HttpClient())
            {
                httpClient.BaseAddress = new Uri(myBaseUrl);

                var jsonOptions = new JsonSerializerOptions()
                {
                    PropertyNamingPolicy = new LowerCaseJsonNamingPolicy()
                };

                    var httpResponse = await httpClient.GetAsync($"v3/publicholidays/{pYear}/{pCountry}");

                if (httpResponse.IsSuccessStatusCode)
                {
                    var response= await httpResponse.Content.ReadFromJsonAsync<List<NagerHoliday>>(jsonOptions);

                    List<Holiday> listResponse = new List<Holiday>();

                    foreach (var holiday in response)
                    {
                        if (holiday.Global && !holiday.Types.Contains("Optional"))
                        {
                            var date = DateTime.ParseExact(holiday.Date, "yyyy-MM-dd", CultureInfo.InvariantCulture);
                            Holiday h = new Holiday(date, holiday.Name, holiday.CountryCode, "A");
                            listResponse.Add(h);
                        }
                    }
                            return listResponse;
                }
                else
                {
                    var error = await GetErrorAsync(httpResponse);
                    throw new ExternalAPICustomException("Nager", error.ErrorText);
                }
            }
        }

        /// <summary>
        /// Turns the status code into an error to be returned 
        /// </summary>
        /// <param name="pHttpResponseMessage">Response from Nager API</param>
        /// <returns>Error model</returns>
        private async Task<NagerErrorReturn> GetErrorAsync(HttpResponseMessage pHttpResponseMessage)
        {
            var error = new NagerErrorReturn();

            switch (pHttpResponseMessage.StatusCode)
            {
                case HttpStatusCode.BadRequest:
                    var errorResponse = await pHttpResponseMessage.Content.ReadFromJsonAsync<NagerError>();
                    error.ErrorText = errorResponse.Title; 
                    error.StatusCode = 400;
                    break;

                case HttpStatusCode.Unauthorized:
                    error.ErrorText = MessageBuilder.GetMessage(ECode.BadAuthentication);
                    error.StatusCode = 401;
                    break;

                case HttpStatusCode.NotFound:
                    error.ErrorText = MessageBuilder.GetMessage(ECode.HolidayParametersInvalid);
                    error.StatusCode = 404;
                    break;

                case HttpStatusCode.InternalServerError:
                    error.ErrorText = "Internal Server Error";
                    error.StatusCode = 500;
                    break;
            }

            return error;
        }
    }
}
