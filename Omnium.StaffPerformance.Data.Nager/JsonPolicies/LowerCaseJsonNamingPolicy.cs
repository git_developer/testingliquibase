﻿using System;
using System.Linq;
using System.Text.Json;

namespace Omnium.StaffPerformance.Data.Nager.JsonPolicies
{
    public class LowerCaseJsonNamingPolicy : JsonNamingPolicy
    {
        public override string ConvertName(string name) => 
            name switch
        {
            null => throw new ArgumentNullException(nameof(name)),
            "" => throw new ArgumentException($"{nameof(name)} cannot be empty", nameof(name)),
            _ => name.First().ToString().ToLower() + name.Substring(1)
        };
    }
}
