﻿
namespace Omnium.StaffPerformance.Data.Nager.Models
{
    public struct NagerError
    {
        public string Type { get; set; }
        public string Title { get; set; }
        public int Status { get; set; }
        public string TraceId { get; set; }

    }
}
