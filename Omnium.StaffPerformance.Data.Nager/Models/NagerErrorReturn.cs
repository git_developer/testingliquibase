﻿
namespace Omnium.StaffPerformance.Data.Nager.Models
{
    public struct NagerErrorReturn
    {
        public int StatusCode { get; set; }
        public string ErrorText { get; set; }
    }
}
