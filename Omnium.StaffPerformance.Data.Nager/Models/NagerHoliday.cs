﻿using System.Collections.Generic;

namespace Omnium.StaffPerformance.Data.Nager.Models
{
    public struct NagerHoliday
    {
        public string Date { get; set; }
        public string LocalName { get; set; }
        public string Name { get; set; }
        public string CountryCode { get; set; }
        public bool Fixed { get; set; }
        public bool Global { get; set; }
        public List<string> Counties { get; set; }
        public List<string> Types { get; set; }
    }
}
