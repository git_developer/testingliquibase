using Omnium.StaffPerformance.Business.Domain.Entities;
using Omnium.StaffPerformance.Business.Domain.IntegrationTests.Helpers;
using Omnium.StaffPerformance.Business.Domain.Services.Platforms;
using Omnium.StaffPerformance.Data.Postgres.Repositories;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace Omnium.StaffPerformance.Business.Domain.IntegrationTests.Services.Platforms
{
    public class PlatformsServiceTests
    {

        /// <summary>
        /// Ensures that when a platform doesn't exist is created in the database
        /// </summary>
        [Fact]
        public void AddOrUpdate_AddingANewPlatform_MustAddThePlatform()
        {
            using (var dbContext = PostgresDataSource.GetPostgresContext())
            {
                var unitOfWork = new UnitOfWork(dbContext);
                var service = new PlatformService(unitOfWork);
                var newPlatform = new Platform(Guid.NewGuid().ToString().ToUpper(), $"Sitoo-{DateTime.Now:yyyyMMssHHmmss}", EPlatformType.ECommerce, "{\"Data\":\"123\"}", "A");

                service.AddOrUpdate(newPlatform);

                var dbPlatform = unitOfWork.Platforms.GetById(newPlatform.Id);

                Assert.Equal(newPlatform, dbPlatform, new PlatformEqualityComparator());
            }
        }

        /// <summary>
        /// Ensures that when a platform exist is updated in the database
        /// </summary>
        [Fact]
        public async Task AddOrUpdate_UpdateAPlatform_MustUpdateThePlatform()
        {
            using (var dbContext = PostgresDataSource.GetPostgresContext())
            {
                var unitOfWork = new UnitOfWork(dbContext);
                var service = new PlatformService(unitOfWork);

                var platformToUpdate = (await unitOfWork.Platforms.GetPlatformsAsync(1, 1).ConfigureAwait(false))
                                                                  .FirstOrDefault();

                var newPlatform = new Platform(platformToUpdate.Id, $"Sitoo-{DateTime.Now:yyyyMMssHHmmss}", EPlatformType.ECommerce, "{\"Data\":\"123\"}", "A");
                service.AddOrUpdate(newPlatform);

                var dbPlatform = unitOfWork.Platforms.GetById(newPlatform.Id);
                Assert.Equal(newPlatform, dbPlatform, new PlatformEqualityComparator());
            }
        }
    }
}
