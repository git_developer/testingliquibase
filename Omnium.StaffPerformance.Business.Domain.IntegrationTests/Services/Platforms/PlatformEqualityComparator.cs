﻿using Omnium.StaffPerformance.Business.Domain.Entities;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace Omnium.StaffPerformance.Business.Domain.IntegrationTests.Services.Platforms
{
    public class PlatformEqualityComparator : IEqualityComparer<Platform>
    {

        public bool Equals(Platform pX, Platform pY)
        {
            var isIdEquals = pX.Id.Equals(pY.Id);
            var isIntegrationDataEquals = pX.IntegrationData.Equals(pY.IntegrationData);
            var isNameEquals = pX.Name.Equals(pY.Name);
            var isPlatformTypeEquals = pX.PlatformType.Equals(pY.PlatformType);
            var isStatusEquals = pX.Status.Equals(pY.Status);


            return isIdEquals  && isIntegrationDataEquals && isPlatformTypeEquals && isNameEquals && isStatusEquals;
        }

        public int GetHashCode([DisallowNull] Platform pObj)
        {
            return pObj.Id.GetHashCode() + pObj.IntegrationData.GetHashCode() + pObj.PlatformType.GetHashCode() + pObj.Name.GetHashCode() + pObj.Status.GetHashCode();
        }
    }
}
