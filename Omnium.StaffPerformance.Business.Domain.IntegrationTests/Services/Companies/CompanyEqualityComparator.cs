﻿using Omnium.StaffPerformance.Business.Domain.Entities;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace Omnium.StaffPerformance.Business.Domain.IntegrationTests.Services.Companies
{
    public class CompanyEqualityComparator : IEqualityComparer<Company>
    {

        public bool Equals(Company pX, Company pY)
        {
            var isIdEquals = pX.Id.Equals(pY.Id);
            var isCityEquals = pX.City.Equals(pY.City);
            var isCompanyCodeEquals = pX.CompanyCode.Equals(pY.CompanyCode);
            var isCountryIdEquals = pX.CountryId.Equals(pY.CountryId);
            var isNameEquals = pX.Name.Equals(pY.Name);
            var isStatusEquals = pX.Status.Equals(pY.Status);
            var isStreetEquals = pX.Street.Equals(pY.Street);
            var isZipCodeEquals = pX.ZipCode.Equals(pY.ZipCode);
            var areCompanyPlatformsEquals = (pX.CompanyPlatforms?.Count ?? 0) == (pY.CompanyPlatforms?.Count ?? 0);


            return isIdEquals && isCityEquals && isCompanyCodeEquals && isCountryIdEquals && isNameEquals && isStatusEquals && isStreetEquals && isZipCodeEquals && areCompanyPlatformsEquals;
        }

        public int GetHashCode([DisallowNull] Company pObj)
        {
            return pObj.Id.GetHashCode() + pObj.City.GetHashCode() + pObj.CompanyCode.GetHashCode() + pObj.CountryId.GetHashCode() + pObj.Name.GetHashCode() + pObj.Status.GetHashCode() + pObj.Street.GetHashCode() + pObj.ZipCode.GetHashCode() + pObj.CompanyPlatforms.GetHashCode();
        }
    }
}
