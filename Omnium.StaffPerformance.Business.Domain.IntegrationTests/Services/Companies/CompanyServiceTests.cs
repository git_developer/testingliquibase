using Omnium.StaffPerformance.Business.Domain.Entities;
using Omnium.StaffPerformance.Business.Domain.IntegrationTests.Helpers;
using Omnium.StaffPerformance.Business.Domain.Services.Companies;
using Omnium.StaffPerformance.Data.Postgres.Repositories;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace Omnium.StaffPerformance.Business.Domain.IntegrationTests.Services.Companies
{
    public class CompaniesServiceTests
    {

        /// <summary>
        /// Ensures that a company that doesn't exist is created
        /// </summary>
        [Fact]
        public void AddOrUpdate_AddingANewCompany_MustAddTheCompany()
        {
            using (var dbContext = PostgresDataSource.GetPostgresContext())
            {
                var unitOfWork = new UnitOfWork(dbContext);
                var service = new CompanyService(unitOfWork);
                var newCompany = new Company(Guid.NewGuid().ToString().ToUpper(), "1", $"Omnium-{DateTime.Now:yyyyMMddHHmmss}", "Street", "City", "zipcode", "testPt", "A");

                service.AddOrUpdate(newCompany);

                var dbCompany = unitOfWork.Companies.GetById(newCompany.Id);

                Assert.Equal(newCompany, dbCompany, new CompanyEqualityComparator());
            }
        }

        /// <summary>
        /// Ensures that a company that exist is updated
        /// </summary>
        [Fact]
        public async Task AddOrUpdate_UpdateACompany_MustUpdateTheCompany()
        {
            using (var dbContext = PostgresDataSource.GetPostgresContext())
            {
                var unitOfWork = new UnitOfWork(dbContext);
                var service = new CompanyService(unitOfWork);

                var companyToUpdate = (await unitOfWork.Companies.GetCompaniesAsync(1, 1).ConfigureAwait(false))
                                                                 .FirstOrDefault();

                var newCompany = new Company(companyToUpdate.Id, "1", $"Omnium-{DateTime.Now:yyyyMMddHHmmss}", "Street", "City", "zipcode", "testPt", "A");
                service.AddOrUpdate(newCompany);

                var dbCompany = unitOfWork.Companies.GetById(newCompany.Id);
                Assert.Equal(newCompany, dbCompany, new CompanyEqualityComparator());
            }
        }
    }
}
