﻿using Omnium.StaffPerformance.Business.Domain.Entities;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace Omnium.StaffPerformance.Business.Domain.IntegrationTests.Services.Countries
{
    public class CountryEqualityComparator : IEqualityComparer<Country>
    {

        public bool Equals(Country pX, Country pY)
        {
            var isIdEquals = pX.Id.Equals(pY.Id);
            var isISO2Equals = pX.ISO2.Equals(pY.ISO2);
            var isISO3Equals = pX.ISO3.Equals(pY.ISO3);
            var isNameEquals = pX.Name.Equals(pY.Name);         
            var isStatusEquals = pX.Status.Equals(pY.Status);


            return isIdEquals  && isISO2Equals && isISO3Equals && isNameEquals && isStatusEquals;
        }

        public int GetHashCode([DisallowNull] Country pObj)
        {
            return pObj.Id.GetHashCode() + pObj.ISO2.GetHashCode() + pObj.ISO3.GetHashCode() + pObj.Name.GetHashCode() + pObj.Status.GetHashCode();
        }
    }
}
