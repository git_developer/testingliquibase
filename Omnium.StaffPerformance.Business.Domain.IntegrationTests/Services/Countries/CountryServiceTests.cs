using Omnium.StaffPerformance.Business.Domain.Entities;
using Omnium.StaffPerformance.Business.Domain.IntegrationTests.Helpers;
using Omnium.StaffPerformance.Business.Domain.Services.Countries;
using Omnium.StaffPerformance.Data.Postgres.Repositories;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace Omnium.StaffPerformance.Business.Domain.IntegrationTests.Services.Countries
{
    public class CountriesServiceTests
    {

        /// <summary>
        /// Ensures that a country that doesn't exist is created
        /// </summary>
        [Fact]
        public void AddOrUpdate_AddingANewCountry_MustAddTheCountry()
        {
            using (var dbContext = PostgresDataSource.GetPostgresContext())
            {
                var unitOfWork = new UnitOfWork(dbContext);
                var service = new CountryService(unitOfWork);
                var newCountry = new Country(Guid.NewGuid().ToString().ToUpper(), $"Portugal-{DateTime.Now:yyyyMMssHHmmss}", "PT", "PTR", "A");

                service.AddOrUpdate(newCountry);

                var dbCountry = unitOfWork.Countries.GetById(newCountry.Id);

                Assert.Equal(newCountry, dbCountry, new CountryEqualityComparator());
            }
        }

        /// <summary>
        /// Ensures that a country that exist is updated
        /// </summary>
        [Fact]
        public async Task AddOrUpdate_UpdateACountry_MustUpdateTheCountry()
        {
            using (var dbContext = PostgresDataSource.GetPostgresContext())
            {
                var unitOfWork = new UnitOfWork(dbContext);
                var service = new CountryService(unitOfWork);

                var countryToUpdate = (await unitOfWork.Countries.GetCountriesAsync(1, 1).ConfigureAwait(false))
                                                                 .FirstOrDefault();              
                var newCountry = new Country(countryToUpdate.Id, $"Portugal-{DateTime.Now:yyyyMMssHHmmss}", "PT", "PTR", "A");
                service.AddOrUpdate(newCountry);

                var dbCountry = unitOfWork.Countries.GetById(newCountry.Id);
                Assert.Equal(newCountry, dbCountry, new CountryEqualityComparator());
            }
        }
    }
}
