﻿using Microsoft.EntityFrameworkCore;
using Omnium.StaffPerformance.Data.Postgres.Context;

namespace Omnium.StaffPerformance.Business.Domain.IntegrationTests.Helpers
{
    public static class PostgresDataSource
    {
        public static PostgresContext GetPostgresContext()
        {
            var dbContextOptions = new DbContextOptionsBuilder<PostgresContext>();
            //TODO improve this
            dbContextOptions.UseNpgsql("Host=localhost;Port=5432;Database=MSPDB;Username=postgres;Password=admin123#");
            var context = new PostgresContext(dbContextOptions.Options);
            return context;
        }

    }
}
