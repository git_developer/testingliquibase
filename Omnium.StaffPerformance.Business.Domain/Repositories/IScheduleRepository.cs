﻿using Omnium.StaffPerformance.Business.Domain.Entities;
using System;
using System.Collections.Generic;

namespace Omnium.StaffPerformance.Business.Domain.Repositories
{
    public interface IScheduleRepository : IRepository<Schedule>
    {
        /// <summary>
        /// Returns all the schedules of a given employee in a given date
        /// </summary>
        /// <param name="pId"></param>
        /// <returns>all the schedules of a given employee</returns>
        List<Schedule> GetScheduleByEmployeeIdAndDay(string pId, DateTime pDate);
        /// <summary>
        /// Checks if there are any shcedules that intersect the schedule we send as parameter
        /// </summary>
        /// <param name="pSchedule">Schedule we want to check intersections from</param>
        /// <returns>a boolean, true if it intersects, false if it does not</returns>
         bool CheckExistsAnotherSchedule(Schedule pSchedule);
        /// <summary>
        /// Retrieves a schedule from the database by its id
        /// </summary>
        /// <param name="pId">id of the schedule</param>
        /// <returns>the schedule with that id</returns>
        Schedule GetScheduleById(string pId);
    }
}
