﻿using Omnium.StaffPerformance.Business.Domain.Entities;
using System.Collections.Generic;

namespace Omnium.StaffPerformance.Business.Domain.Repositories
{
    public interface IPeriodTypeRepository : IRepository<PeriodType>
    {
        /// <summary>
        /// Gets a period Type instance from the database with the same name parameter
        /// </summary>
        /// <param name="pName">name of the period type we want</param>
        /// <returns>a period type instance with the same name as we sent</returns>
        PeriodType GetPeriodTypeByName(string pName);
        /// <summary>
        /// Gets a period Type instance from the database with the same Id parameter
        /// </summary>
        /// <param name="pId">Id of the period type we want</param>
        /// <returns>a period type instance with the same Id as we sent</returns>
        PeriodType GetPeriodTypeById(string pId);
        /// <summary>
        /// Gets all the existing period types from the database
        /// </summary>
        /// <returns>list with all period types</returns>
        List<PeriodType> GetPeriodTypes();
    }
}
