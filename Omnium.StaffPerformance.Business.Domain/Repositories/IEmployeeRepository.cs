﻿using Omnium.StaffPerformance.Business.Domain.Entities;

namespace Omnium.StaffPerformance.Business.Domain.Repositories
{
    public interface IEmployeeRepository : IRepository<Employee>
    {
        /// <summary>
        /// Retrieves an employee from the database with a given email
        /// </summary>
        /// <param name="pEmail">email of the employee we are searching</param>
        /// <returns>the employee with that email</returns>
        Employee GetEmployeeByEmail(string pEmail);
        /// <summary>
        /// Retrieves an employee from the database with a given ID
        /// </summary>
        /// <param name="pID">ID of the employee we are searching</param>
        /// <returns>the employee with that ID</returns>
        Employee GetEmployeeById(string pID);
        /// <summary>
        /// Checks if the id matches one of an existing employee.Returns false if it does not match
        /// </summary>
        /// <param name="pId">Id of the employee we are looking for</param>
        /// <returns>true if there is a match, false if there is not</returns>
        bool CheckEmployeeExists(string pId);
    }
}
