﻿
namespace Omnium.StaffPerformance.Business.Domain.Repositories
{
    public interface IRepository<TEntity> where TEntity : class
    {
        /// <summary>
        /// Retrieves an Entry from the database with a certain ID
        /// </summary>
        /// <param name="pId">ID to find</param>
        /// <returns>entry with that ID</returns>
        TEntity GetById(object pId);
        /// <summary>
        /// Deletes an entry from the database
        /// </summary>
        /// <param name="pEntityToDelete">entity to delete</param>
        void Delete(TEntity pEntityToDelete);
        /// <summary>
        /// Adds an entry to the database
        /// </summary>
        /// <param name="pEntity">entity to add</param>
        void Add(TEntity pEntity);
    }
}
