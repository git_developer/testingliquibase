﻿using Omnium.StaffPerformance.Business.Domain.Entities;

namespace Omnium.StaffPerformance.Business.Domain.Repositories
{
    public interface ICalendarRepository : IRepository<Calendar>
    {
    }
}
