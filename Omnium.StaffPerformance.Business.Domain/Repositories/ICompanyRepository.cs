﻿using Omnium.StaffPerformance.Business.Domain.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Omnium.StaffPerformance.Business.Domain.Repositories
{
    public interface ICompanyRepository : IRepository<Company>
    {
        /// <summary>
        /// Returns a list of companies
        /// </summary>
        /// <param name="pPage">page</param>
        /// <param name="pRecords">number of records per page</param>
        /// <returns>Returns a list of companies</returns>
        Task<List<Company>> GetCompaniesAsync(int pPage, int pRecords);
    }
}
