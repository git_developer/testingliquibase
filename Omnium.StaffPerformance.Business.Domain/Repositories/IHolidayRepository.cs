﻿using Omnium.StaffPerformance.Business.Domain.Entities;
using System;
using System.Collections.Generic;

namespace Omnium.StaffPerformance.Business.Domain.Repositories
{
    public interface IHolidayRepository : IRepository<Holiday>
    {
        /// <summary>
        /// Finds an holiday with the name, country Id and name secified
        /// </summary>
        /// <param name="pDate">date we want the holiday to be of</param>
        /// <param name="pCountryId">Country Id we want the holiday to be of</param>
        /// <param name="pName">Name we want the holiday to be of</param>
        /// <returns>the holiday found</returns>
        Holiday GetHolidayByNameCountryCodeAndDate(DateTime pDate, string pCountryId, string pName);
        /// <summary>
        /// Finds an holiday with the  country Id and name specified
        /// </summary>
        /// <param name="pDate">date we want the holiday to be of</param>
        /// <param name="pCountryId">Country Id we want the holiday to be of</param>
        /// <returns>the holiday found</returns>
        Holiday GetHolidayByCountryCodeAndDate(DateTime pDate, string pCountryId);
        /// <summary>
        /// Finds an holiday with the id specified
        /// </summary>
        /// <param name="pId">id of the holiday we want</param>
        /// <returns>the holiday with that id</returns>
        Holiday GetHolidayById(string pId);
        /// <summary>
        /// Finds all holidays
        /// </summary>
        /// <returns>allHolidays on the database</returns>
        List<Holiday> GetHolidays();
    }
}
