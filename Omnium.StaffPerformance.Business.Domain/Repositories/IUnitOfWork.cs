﻿
namespace Omnium.StaffPerformance.Business.Domain.Repositories
{
    public interface IUnitOfWork
    {
        IEmployeeRepository Employees { get; }

        IAttendanceRepository Attendances { get; }
        IPeriodTypeRepository PeriodTypes { get; }

        IHolidayRepository Holidays { get; }
        ICountryRepository Countries { get; }
        IStoreRepository Stores { get; }
        IScheduleRepository Schedules { get; }
        ICalendarRepository Calendars { get; }
        ICompanyRepository Companies { get; }
        IPlatformRepository Platforms { get; }

        /// <summary>
        /// Commits the changes to the database from the Repository
        /// </summary>
        void Commit();
    }
}
