﻿using Omnium.StaffPerformance.Business.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Omnium.StaffPerformance.Business.Domain.Repositories
{
    public interface IScheduleIntervalRepository : IRepository<ScheduleInterval>
    {

    }
}
