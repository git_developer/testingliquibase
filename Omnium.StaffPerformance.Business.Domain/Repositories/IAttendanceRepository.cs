﻿using Omnium.StaffPerformance.Business.Domain.Entities;
using System.Collections.Generic;

namespace Omnium.StaffPerformance.Business.Domain.Repositories
{
    public interface IAttendanceRepository : IRepository<Attendance>
    {
        /// <summary>
        /// Returns the last attendance of a specific employee with the given Id
        /// </summary>
        /// <param name="pEmployeeId">Id of the employee we want the last attendance from</param>
        /// <returns>The last attendance found for the given employee</returns>
        Attendance GetLastAttendanceByEmployeeId(string pEmployeeId);
        /// <summary>
        /// Returns all the attendances of a specific employee
        /// </summary>
        /// <param name="pEmployeeId">employee we want the attendances from</param>
        /// <returns>all of its attendances</returns>
        List<Attendance> GetAttendancesByEmployeeId(string pEmployeeId);
    }
}
