﻿using Omnium.StaffPerformance.Business.Domain.Entities;

namespace Omnium.StaffPerformance.Business.Domain.Repositories
{
    public interface IStoreRepository : IRepository<Store>
    {
        /// <summary>
        /// Finds a store with the id specified
        /// </summary>
        /// <param name="pId">id of the store we want</param>
        /// <returns>the store with that id</returns>
        public Store GetStoreById(string pId);
    }
}
