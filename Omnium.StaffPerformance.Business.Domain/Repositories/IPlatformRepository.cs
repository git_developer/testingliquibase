﻿using Omnium.StaffPerformance.Business.Domain.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Omnium.StaffPerformance.Business.Domain.Repositories
{
    public interface IPlatformRepository : IRepository<Platform>
    {
        /// <summary>
        /// Returns a list of platforms
        /// </summary>
        /// <param name="pPage">page</param>
        /// <param name="pRecords">number of records per page</param>
        /// <returns>Returns a list of platforms</returns>
        Task<List<Platform>> GetPlatformsAsync(int pPage, int pRecords);
    }
}
