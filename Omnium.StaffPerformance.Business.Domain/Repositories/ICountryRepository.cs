﻿using Omnium.StaffPerformance.Business.Domain.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Omnium.StaffPerformance.Business.Domain.Repositories
{
    public interface ICountryRepository : IRepository<Country>
    {
        /// <summary>
        /// Returns a country with the specified Name
        /// </summary>
        /// <param name="pCountryName">name of the country we want</param>
        /// <returns>the country with that name</returns>
        Country GetCountryByName(string pCountryName);
        /// <summary>
        /// Returns a list of countries
        /// </summary>
        /// <param name="pPage">page</param>
        /// <param name="pRecords">number of records per page</param>
        /// <returns>Returns a list of countries</returns>
        Task<List<Country>> GetCountriesAsync(int pPage, int pRecords);
        
    }
}
