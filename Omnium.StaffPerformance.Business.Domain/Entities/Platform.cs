﻿using System;

namespace Omnium.StaffPerformance.Business.Domain.Entities
{
    public class Platform : IAggregateRoot
    {
        public string Id { get; private set; }
        public string Name { get; private set; }
        public EPlatformType PlatformType { get; private set; }
        public string IntegrationData { get; private set; }
        public DateTime CreateDate { get; private set; }
        public DateTime UpdateDate { get; private set; }
        public string Status { get; private set; }

        /// <summary>
        /// Creates a Platform instance
        /// </summary>
        /// <param name="pId">platform identification</param>
        /// <param name="pName">Name of the platform</param>
        /// <param name="pPlatformType">Enum of the type of platform code</param>
        /// <param name="pIntegrationData">Necessary information to integrate the data on the external platform</param>
        /// <param name="pStatus">Current Status of the Platform registry</param>
        public Platform(string pId, string pName, EPlatformType pPlatformType, string pIntegrationData, string pStatus)
        {
            Id = string.IsNullOrEmpty(pId) ? throw new ArgumentNullException(nameof(pId)) : pId;
            Name = string.IsNullOrEmpty(pName) ? throw new ArgumentNullException(nameof(pName)) : pName;
            PlatformType = pPlatformType;
            IntegrationData = string.IsNullOrEmpty(pIntegrationData) ? throw new ArgumentNullException(nameof(pIntegrationData)) : pIntegrationData;
            Status = Enum.IsDefined(typeof(EStatus), pStatus) ? pStatus : throw new ArgumentException(nameof(pStatus));
        }

        protected Platform()
        {

        }

        /// <summary>
        /// Updates a Platform
        /// </summary>
        /// <param name="pName">Name of the platform</param>
        /// <param name="pPlatformType">Enum of the type of platform code</param>
        /// <param name="pIntegrationData">Necessary information to integrate the data on the external platform</param>
        /// <param name="pStatus">Status of the Platform</param>
        public void Update(string pName, EPlatformType pPlatformType, string pIntegrationData, string pStatus)
        {
            Name = string.IsNullOrEmpty(pName) ? throw new ArgumentNullException(nameof(pName)) : pName;
            PlatformType = pPlatformType;
            IntegrationData = string.IsNullOrEmpty(pIntegrationData) ? throw new ArgumentNullException(nameof(pIntegrationData)) : pIntegrationData;
            Status = Enum.IsDefined(typeof(EStatus), pStatus) ? pStatus : throw new ArgumentException(nameof(pStatus));
        }

    }
}
