﻿using Omnium.StaffPerformance.Business.Exceptions;
using System;
using System.Collections.Generic;

namespace Omnium.StaffPerformance.Business.Domain.Entities
{
    public class Holiday : IAggregateRoot
    {
        public string Id { get; private set; }
        public DateTime Date { get; private set; }
        public string Name { get; private set; }
        public string CountryId { get; private set; }
        public virtual ICollection<StoreHoliday> StoreHolidays { get; private set; }
        public DateTime CreateDate { get; private set; }
        public DateTime UpdateDate { get; private set; }
        public string Status { get; private set; }

        /// <summary>
        /// Initializes an Holiday
        /// </summary>
        /// <param name="pDate">Date of the holiday</param>
        /// <param name="pName">Name of the Holiday</param>
        /// <param name="pCountryId">GuiD of the country were the holiday is in</param>
        /// <param name="pStatus">Status of the registry</param>
        public Holiday(DateTime pDate, string pName, string pCountryId, string pStatus)
        {
            Id = Guid.NewGuid().ToString().ToUpper();
            Date = pDate;
            Name = string.IsNullOrEmpty(pName) ? throw new ArgumentNullException(nameof(pName)) : pName;
            CountryId = string.IsNullOrEmpty(pCountryId) ? throw new ArgumentNullException(nameof(pCountryId)) : pCountryId;
            Status = Enum.IsDefined(typeof(EStatus), pStatus) ? pStatus : throw new ArgumentException(nameof(pStatus));
            StoreHolidays = new HashSet<StoreHoliday>();
        }

        protected Holiday()
        {

        }

        /// <summary>
        /// Updates the current object's information
        /// </summary>
        /// <param name="pDate">Date of the holiday</param>
        /// <param name="pName">Name of the Holiday</param>
        /// <param name="pCountryId">GuiD of the country were the holiday is in</param>
        /// <param name="pStatus">Status of the registry</param>
        public void Update(DateTime pDate, string pName, string pCountryId, string pStatus)
        {
            Date = pDate;
            Name = string.IsNullOrEmpty(pName) ? throw new ArgumentNullException(nameof(pName)) : pName;
            CountryId = string.IsNullOrEmpty(pCountryId) ? throw new ArgumentNullException(nameof(pCountryId)) : pCountryId;
            Status = Enum.IsDefined(typeof(EStatus), pStatus) ? pStatus : throw new ArgumentException(nameof(pStatus));
        }
        /// <summary>
        /// Adds an object to the storeHoliday collection, creating a connection from holiday to store
        /// </summary>
        /// <param name="pStoreHoliday">StoreHoliday object containing the relationship between store and Holiday</param>
        public void AddStoreHoliday(StoreHoliday pStoreHoliday)
        {
            foreach(var storeHoliday in StoreHolidays)
            {
                if(storeHoliday.Holiday.Id == pStoreHoliday.Holiday.Id && storeHoliday.Store.Id == pStoreHoliday.Store.Id)
                {
                    throw new AssociationXAlreadyExistsCustomException("Store and Holiday");
                }
            }

            StoreHolidays.Add(pStoreHoliday);
        }
    }
}
