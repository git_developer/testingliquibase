﻿using Omnium.StaffPerformance.Common.Validators;
using System;

namespace Omnium.StaffPerformance.Business.Domain.Entities
{
    public class Employee : IAggregateRoot
    {
        public string Id { get; private set; }
        public virtual Company Company { get; private set; }
        public string CompanyId { get; private set; }
        public string NameShort { get; private set; }
        public string NameFull { get; private set; }
        public string Gender { get; private set; }
        public string Email { get; private set; }
        public string MobilePhoneNumber { get; private set; }
        public string PhoneNumber { get; private set; }
        public DateTime BirthDate { get; private set; }
        public DateTime CreateDate { get; private set; }
        public DateTime UpdateDate { get; private set; }
        public string Status { get; private set; }



        /// <summary>
        /// Initializes an Employee
        /// </summary>
        /// <param name="pCompany">company to which the employee belongs</param>
        /// <param name="pShortName">abbreviation of employee name</param>
        /// <param name="pFullName">full name</param>
        /// <param name="pGender">gender</param>
        /// <param name="pEmail">personal email address</param>
        /// <param name="pMobilePhoneNumber">mobile phone number</param>
        /// <param name="pPhoneNumber">phone number</param>
        /// <param name="pBirthDate">birthdate</param>
        /// <param name="pStatus">current status of the employee</param>
        public Employee(Company pCompany, string pShortName, string pFullName, string pGender, string pEmail, string pMobilePhoneNumber, string pPhoneNumber, DateTime pBirthDate, string pStatus)
        {
            Id = Guid.NewGuid().ToString().ToUpper();
            Company = pCompany;
            CompanyId = pCompany.Id;
            NameShort = string.IsNullOrEmpty(pShortName) ? throw new ArgumentNullException(nameof(pShortName)) : pShortName;
            NameFull = string.IsNullOrEmpty(pFullName) ? throw new ArgumentNullException(nameof(pFullName)) : pFullName;
            Gender = Enum.IsDefined(typeof(EGender), pGender) ? pGender : throw new ArgumentException(nameof(pGender));
            Email = string.IsNullOrEmpty(pEmail) ? throw new ArgumentNullException(nameof(pEmail)) : pEmail;
            MobilePhoneNumber = string.IsNullOrEmpty(pMobilePhoneNumber) || ValidatorFactory.IsPhoneOrMobilePhoneValid(pMobilePhoneNumber) ? pMobilePhoneNumber : throw new ArgumentException(nameof(pMobilePhoneNumber));
            PhoneNumber = string.IsNullOrEmpty(pPhoneNumber) || ValidatorFactory.IsPhoneOrMobilePhoneValid(pPhoneNumber) ? pPhoneNumber : throw new ArgumentException(nameof(pPhoneNumber));
            BirthDate = pBirthDate;
            Status = Enum.IsDefined(typeof(EStatus), pStatus) ? pStatus : throw new ArgumentException(nameof(pStatus));
        }

        protected Employee() { }

        /// <summary>
        /// Updates all the information of an employee instance
        /// </summary>
        /// <param name="pCompany">company to which the employee belongs</param>
        /// <param name="pShortName">abbreviation of employee name</param>
        /// <param name="pFullName">full name</param>
        /// <param name="pGender">gender</param>
        /// <param name="pEmail">personal email address</param>
        /// <param name="pMobilePhoneNumber">mobile phone number</param>
        /// <param name="pPhoneNumber">phone number</param>
        /// <param name="pBirthDate">birthdate</param>
        /// <param name="pStatus">current status of the employee</param>
        public void Update(Company pCompany, string pShortName, string pFullName, string pGender, string pEmail, string pMobilePhoneNumber, string pPhoneNumber, DateTime pBirthDate, string pStatus)
        {
            Id = Guid.NewGuid().ToString().ToUpper();
            Company = pCompany;
            CompanyId = pCompany.Id;
            NameShort = string.IsNullOrEmpty(pShortName) ? throw new ArgumentNullException(nameof(pShortName)) : pShortName;
            NameFull = string.IsNullOrEmpty(pFullName) ? throw new ArgumentNullException(nameof(pFullName)) : pFullName;
            Gender = Enum.IsDefined(typeof(EGender), pGender) ? pGender : throw new ArgumentException(nameof(pGender));
            Email = string.IsNullOrEmpty(pEmail) ? throw new ArgumentNullException(nameof(pEmail)) : pEmail;
            MobilePhoneNumber = string.IsNullOrEmpty(pMobilePhoneNumber) || ValidatorFactory.IsPhoneOrMobilePhoneValid(pMobilePhoneNumber) ? pMobilePhoneNumber : throw new ArgumentException(nameof(pMobilePhoneNumber));
            PhoneNumber = string.IsNullOrEmpty(pPhoneNumber) || ValidatorFactory.IsPhoneOrMobilePhoneValid(pPhoneNumber) ? pPhoneNumber : throw new ArgumentException(nameof(pPhoneNumber));
            BirthDate = pBirthDate;
            Status = Enum.IsDefined(typeof(EStatus), pStatus) ? pStatus : throw new ArgumentException(nameof(pStatus));
        }

    }
}
