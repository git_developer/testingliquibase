﻿using System;

namespace Omnium.StaffPerformance.Business.Domain.Entities
    
{
    public interface IAggregateRoot
    {
        DateTime CreateDate { get; }
        DateTime UpdateDate { get; }
    }
}
