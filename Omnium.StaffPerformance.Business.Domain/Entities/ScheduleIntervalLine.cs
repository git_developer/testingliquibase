﻿using System;

namespace Omnium.StaffPerformance.Business.Domain.Entities
{
    public class ScheduleIntervalLine
    {
        public string ScheduleIntervalId { get; private set; }
        public virtual ScheduleInterval ScheduleInterval { get; private set; }
        public int Begin { get; private set; }
        public int End { get; private set; }
        public int Order { get; private set; }

        /// <summary>
        /// Initializes a schedule interval line
        /// </summary>
        /// <param name="pScheduleIntervalId">Id of the schedule interval we want to insert in order</param>
        /// <param name="pBegin">Beggining of the schedule interval "slot"</param>
        /// <param name="pEnd">End of the schedule interval "slot"</param>
        /// <param name="pOrder">Interval order</param>
        public ScheduleIntervalLine(string pScheduleIntervalId, int pBegin, int pEnd, int pOrder)
        {
            ScheduleIntervalId = string.IsNullOrEmpty(pScheduleIntervalId) ? throw new ArgumentNullException(nameof(pScheduleIntervalId)) : pScheduleIntervalId;
            Order = pOrder;
            Begin = pBegin;
            End = pEnd;
        }

        protected ScheduleIntervalLine()
        {

        }
    }
}
