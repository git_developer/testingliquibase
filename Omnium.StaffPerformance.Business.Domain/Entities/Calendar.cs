﻿using System;

namespace Omnium.StaffPerformance.Business.Domain.Entities
{
    public class Calendar :IAggregateRoot
    {
        public string Id { get; private set; }
        public DateTime Date { get; private set; }
        public EWeekDay Weekday { get; private set; }
        public string WeekDayStr { get; private set; }
        public int Week { get; private set; }
        public EMonth Month { get; private set; }
        public string MonthStr { get; private set; }
        public int Year { get; private set; }
        public string Status { get; private set; }
        public DateTime CreateDate { get; private set; }
        public DateTime UpdateDate { get; private set; }

        /// <summary>
        /// Creates a Calendar instance
        /// </summary>
        /// <param name="pDate">Date of the object's day</param>
        /// <param name="pWeekDay">Enum containing the day of the week</param>
        /// <param name="pWeek">Week number(representing which week of the year it is</param>
        /// <param name="pMonth">Enum containing the month</param>
        /// <param name="pYear"> Year of the Date</param>
        /// <param name="pStatus">current status for the registry</param>
        public Calendar(DateTime pDate, EWeekDay pWeekDay, int pWeek, EMonth pMonth, int pYear, string pStatus)
        {
            Id = Guid.NewGuid().ToString().ToUpper();
            Date = pDate;
            WeekDayStr = Enum.IsDefined(typeof(EWeekDay), pWeekDay) ? ((EWeekDay)pWeekDay).ToString() : throw new ArgumentException(nameof(pWeekDay));
            Weekday = pWeekDay;
            Week = pWeek;
            Month = pMonth;
            MonthStr = Enum.IsDefined(typeof(EMonth), pMonth) ? ((EMonth)pMonth).ToString() : throw new ArgumentException(nameof(pMonth));
            Year = pYear;
            Status = Enum.IsDefined(typeof(EStatus), pStatus) ? pStatus : throw new ArgumentException(nameof(pStatus));
        }

        protected Calendar()
        {

        }
    }
}
