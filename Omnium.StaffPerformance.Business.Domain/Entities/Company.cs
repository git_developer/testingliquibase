﻿using System;
using System.Collections.Generic;

namespace Omnium.StaffPerformance.Business.Domain.Entities
{
    public class Company : IAggregateRoot
    {
        public string Id { get; private set; }
        public string CompanyCode { get; private set; }
        public string Name { get; private set; }
        public string Street { get; private set; }
        public string City { get; private set; }
        public string ZipCode { get; private set; }
        public string CountryId { get; private set; }
        public virtual ICollection<CompanyPlatform> CompanyPlatforms { get; private set; }
        public DateTime CreateDate { get; private set; }
        public DateTime UpdateDate { get; private set; }
        public string Status { get; private set; }

        /// <summary>
        /// Creates a Company Instance
        /// </summary>
        /// <param name="pCompanyCode">Company Code</param>
        /// <param name="pName"> Name of the company</param>
        /// <param name="pStreet">Street where the company is situated</param>
        /// <param name="pCity">City where the company is</param>
        /// <param name="pZipCode">Zip Code of the company's adress</param>
        /// <param name="pCountryId">Country identifier from the country where the company is</param>
        /// <param name="pStatus">Current status registry for the company</param>
        public Company(string pId, string pCompanyCode, string pName, string pStreet, string pCity, string pZipCode, string pCountryId, string pStatus, HashSet<CompanyPlatform> pCompanyPlatforms = null)
        {
            Id = string.IsNullOrEmpty(pId) ? throw new ArgumentNullException(nameof(pId)) : pId;
            CompanyCode = string.IsNullOrEmpty(pCompanyCode) ? throw new ArgumentNullException(nameof(pCompanyCode)) : pCompanyCode;
            Name = string.IsNullOrEmpty(pName) ? throw new ArgumentNullException(nameof(pName)) : pName;
            Street = string.IsNullOrEmpty(pStreet) ? throw new ArgumentNullException(nameof(pStreet)) : pStreet;
            City = string.IsNullOrEmpty(pCity) ? throw new ArgumentNullException(nameof(pCity)) : pCity;
            ZipCode = string.IsNullOrEmpty(pZipCode) ? throw new ArgumentNullException(nameof(pZipCode)) : pZipCode;
            CountryId = string.IsNullOrEmpty(pCountryId) ? throw new ArgumentNullException(nameof(pCountryId)) : pCountryId;
            Status = Enum.IsDefined(typeof(EStatus), pStatus) ? pStatus : throw new ArgumentException(nameof(pStatus));
            CompanyPlatforms = pCompanyPlatforms ?? new HashSet<CompanyPlatform>();
        }

        protected Company()
        {

        }

        /// <summary>
        /// Updates a company 
        /// </summary>
        /// <param name="pCompanyCode">Company Code</param>
        /// <param name="pName"> Name of the company</param>
        /// <param name="pStreet">Street where the company is situated</param>
        /// <param name="pCity">City where the company is</param>
        /// <param name="pZipCode">Zip Code of the company's adress</param>
        /// <param name="pCountryId">Country identifier from the country where the company is</param>
        /// <param name="pStatus">Current status registry for the company</param>
        public void Update(string pCompanyCode, string pName, string pStreet, string pCity, string pZipCode, string pCountryId, string pStatus)
        {
            CompanyCode = string.IsNullOrEmpty(pCompanyCode) ? throw new ArgumentNullException(nameof(pCompanyCode)) : pCompanyCode;
            Name = string.IsNullOrEmpty(pName) ? throw new ArgumentNullException(nameof(pName)) : pName;
            Street = string.IsNullOrEmpty(pStreet) ? throw new ArgumentNullException(nameof(pStreet)) : pStreet;
            City = string.IsNullOrEmpty(pCity) ? throw new ArgumentNullException(nameof(pCity)) : pCity;
            ZipCode = string.IsNullOrEmpty(pZipCode) ? throw new ArgumentNullException(nameof(pZipCode)) : pZipCode;
            CountryId = string.IsNullOrEmpty(pCountryId) ? throw new ArgumentNullException(nameof(pCountryId)) : pCountryId;
            Status = Enum.IsDefined(typeof(EStatus), pStatus) ? pStatus : throw new ArgumentException(nameof(pStatus));
        }

    }
}
