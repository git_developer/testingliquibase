﻿
namespace Omnium.StaffPerformance.Business.Domain.Entities
{
    public enum EPlatformType
    {
        POS=0,
        ECommerce=1,
        HR=2
    }
}
