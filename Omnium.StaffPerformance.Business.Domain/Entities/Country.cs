﻿using System;

namespace Omnium.StaffPerformance.Business.Domain.Entities
{
    public class Country : IAggregateRoot
    {

        public string Id { get; private set; }
        public string Name { get; private set; }
        public string ISO2 { get; private set; }
        public string ISO3 { get; private set; }
        public string Status { get; private set; }
        public DateTime CreateDate { get; private set; }
        public DateTime UpdateDate { get; private set; }

        /// <summary>
        /// Initializes a country
        /// </summary>
        /// <param name="pId">identifier</param>
        /// <param name="pName">Name of the country</param>
        /// <param name="pISO2">ISO 3166-1 alpha-2 codes are two-letter country codes defined in ISO 3166-1</param>
        /// <param name="pISO3">ISO 3166-1 alpha-3 codes are three-letter country codes defined in ISO 3166-1</param>
        /// <param name="pStatus">current status for the country</param>
        public Country(string pId, string pName, string pISO2, string pISO3, string pStatus)
        {
            Id = string.IsNullOrEmpty(pId) ? throw new ArgumentNullException(nameof(pId)) : pId;
            Name = string.IsNullOrEmpty(pName) ? throw new ArgumentNullException(nameof(pName)) : pName;
            ISO2 = string.IsNullOrEmpty(pISO2) ? throw new ArgumentNullException(nameof(pISO2)) : pISO2;
            ISO3 = string.IsNullOrEmpty(pISO3) ? throw new ArgumentNullException(nameof(pISO3)) : pISO3;
            Status = Enum.IsDefined(typeof(EStatus), pStatus) ? pStatus : throw new ArgumentException(nameof(pStatus));
        }

        protected Country()
        {

        }


        /// <summary>
        /// Updates a country
        /// </summary>
        /// <param name="pName">Name of the country</param>
        /// <param name="pISO2">ISO 3166-1 alpha-2 codes are two-letter country codes defined in ISO 3166-1</param>
        /// <param name="pISO3">ISO 3166-1 alpha-3 codes are three-letter country codes defined in ISO 3166-1</param>
        /// <param name="pStatus">current status for the country</param>
        public void Update( string pName, string pISO2, string pISO3, string pStatus)
        {
            Name = string.IsNullOrEmpty(pName) ? throw new ArgumentNullException(nameof(pName)) : pName;
            ISO2 = string.IsNullOrEmpty(pISO2) ? throw new ArgumentNullException(nameof(pISO2)) : pISO2;
            ISO3 = string.IsNullOrEmpty(pISO3) ? throw new ArgumentNullException(nameof(pISO3)) : pISO3;
            Status = Enum.IsDefined(typeof(EStatus), pStatus) ? pStatus : throw new ArgumentException(nameof(pStatus));
        }


    }
}
