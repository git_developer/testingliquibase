﻿using System;

namespace Omnium.StaffPerformance.Business.Domain.Entities
{
    public class PeriodType : IAggregateRoot
    {

        public string Id { get; private set; }
        public string Name { get; private set; }
        public string Value { get; private set; }
        public virtual Company Company { get; private set; }
        public string CompanyId { get; private set; }
        public string Status { get; private set; }
        public DateTime CreateDate { get; private set; }
        public DateTime UpdateDate { get; private set; }

        /// <summary>
        /// Inititalizes a PeriodType object
        /// </summary>
        /// <param name="pName">Name of the period type</param>
        /// <param name="pValue">Visual representation of what appears in the schedule spot</param>
        /// <param name="pStatus">status of the registry (I, A or D)</param>
        public PeriodType(Company pCompany,string pName, string pValue, string pStatus)
        {
            Id = Guid.NewGuid().ToString().ToUpper();
            Company = pCompany;
            CompanyId = pCompany.Id;
            Name = string.IsNullOrEmpty(pName) ? throw new ArgumentNullException(nameof(pName)) : pName;
            Value = string.IsNullOrEmpty(pValue) ? throw new ArgumentNullException(nameof(pValue)) : pValue;
            Status = Enum.IsDefined(typeof(EStatus), pStatus) ? pStatus : throw new ArgumentException(nameof(pStatus));
        }

        protected PeriodType()
        {

        }

        public void Update(Company pCompany,string pName, string pValue, string pStatus)
        {
            Company = pCompany;
            CompanyId = pCompany.Id;
            Name = string.IsNullOrEmpty(pName) ? throw new ArgumentNullException(nameof(pName)) : pName;
            Value = string.IsNullOrEmpty(pValue) ? throw new ArgumentNullException(nameof(pValue)) : pValue;
            Status = Enum.IsDefined(typeof(EStatus), pStatus) ? pStatus : throw new ArgumentException(nameof(pStatus));
        }
    }
}
