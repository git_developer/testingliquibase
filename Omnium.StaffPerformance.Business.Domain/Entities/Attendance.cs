﻿using Omnium.StaffPerformance.Business.Domain.Services.Attendances;
using System;

namespace Omnium.StaffPerformance.Business.Domain.Entities
{
    public class Attendance : IAggregateRoot
    {

        public string Id { get; private set; }
        public string EmployeeId { get; private set; }
        public DateTime Date { get; private set; }
        public EAttendanceType Type { get; private set; }
        public virtual Company Company { get; private set; }
        public string CompanyId { get; private set; }
        public string Status { get; private set; }
        public DateTime CreateDate { get; private set; }
        public DateTime UpdateDate { get; private set; }

        /// <summary>
        /// Initializes and Attendance
        /// </summary>
        /// <param name="pEmployeeId">id of the employee registering the attendance</param>
        /// <param name="pDate">Date of the entry/exit</param>
        /// <param name="pType">type of attendance, 1 for exit, 0 for entry </param>
        /// <param name="pStatus">status of the employee</param>
        public Attendance(Company pCompany,string pEmployeeId, DateTime pDate, EAttendanceType pType, string pStatus)
        {
            Id = Guid.NewGuid().ToString().ToUpper();
            Company = pCompany;
            CompanyId = pCompany.Id;
            EmployeeId = string.IsNullOrEmpty(pEmployeeId) ? throw new ArgumentNullException(nameof(pEmployeeId)) : pEmployeeId;
            Date = pDate;
            Type = pType;
            Status = Enum.IsDefined(typeof(EStatus), pStatus) ? pStatus : throw new ArgumentException(nameof(pStatus));
        }

        protected Attendance()
        {

        }
    }
}
