﻿using System;

namespace Omnium.StaffPerformance.Business.Domain.Entities
{
    public class Schedule : IAggregateRoot
    {
        public string Id { get; private set; }
        public string EmployeeId { get; private set; }
        public virtual Store Store { get; private set; }
        public string StoreId { get; private set; }
        public virtual Calendar Calendar { get; private set; }
        public string CalendarId { get; private set; }
        public virtual Company Company { get; private set; }
        public string CompanyId { get; private set; }
        public virtual PeriodType PeriodType { get; private set; }
        public string PeriodTypeId { get; private set; }
        public DateTime Begin { get; private set; }
        public DateTime End { get; private set; }
        public DateTime CreateDate { get; private set; }
        public DateTime UpdateDate { get; private set; }
        public string Status { get; private set; }
        /// <summary>
        /// Creates a Schedule Instance
        /// </summary>
        /// <param name="pEmployeeId">Employee Identifier for this schedule</param>
        /// <param name="pStore">Store for this schedule</param>
        /// <param name="pCalendarId">Calendar Identifier for this schedule</param>
        /// <param name="pCompany">Company where this schedule belongs</param>
        /// <param name="pPeriodTypeId">Period Type Identifier for this schedule</param>
        /// <param name="pBegin">beggining date of the schedule</param>
        /// <param name="pEnd">end date of the schedule</param>
        /// <param name="pStatus">current status registry for this schedule</param>
        public Schedule(string pEmployeeId, Store pStore, string pCalendarId, Company pCompany, string pPeriodTypeId, DateTime pBegin, DateTime pEnd, string pStatus)
        {
            Id = Guid.NewGuid().ToString().ToUpper();
            EmployeeId = string.IsNullOrEmpty(pEmployeeId) ? throw new ArgumentNullException(nameof(pEmployeeId)) : pEmployeeId;
            Store = pStore;
            StoreId = pStore.Id;
            CalendarId = string.IsNullOrEmpty(pCalendarId) ? throw new ArgumentNullException(nameof(pCalendarId)) : pCalendarId;
            Company = pCompany;
            CompanyId = pCompany.Id;
            PeriodTypeId = string.IsNullOrEmpty(pPeriodTypeId) ? throw new ArgumentNullException(nameof(pPeriodTypeId)) : pPeriodTypeId;
            Begin = pBegin;
            End = pEnd;
            Status = Enum.IsDefined(typeof(EStatus), pStatus) ? pStatus : throw new ArgumentException(nameof(pStatus));
        }

        protected Schedule() { }

        public void Update(string pEmployeeId, Store pStore, string pCalendarId, Company pCompany, string pPeriodTypeId, DateTime pBegin, DateTime pEnd, string pStatus)
        {
            EmployeeId = string.IsNullOrEmpty(pEmployeeId) ? throw new ArgumentNullException(nameof(pEmployeeId)) : pEmployeeId;
            Store = pStore;
            StoreId = pStore.Id;
            CalendarId = string.IsNullOrEmpty(pCalendarId) ? throw new ArgumentNullException(nameof(pCalendarId)) : pCalendarId;
            Company = pCompany;
            CompanyId = pCompany.Id;
            PeriodTypeId = string.IsNullOrEmpty(pPeriodTypeId) ? throw new ArgumentNullException(nameof(pPeriodTypeId)) : pPeriodTypeId;
            Begin = pBegin;
            End = pEnd;
            Status = Enum.IsDefined(typeof(EStatus), pStatus) ? pStatus : throw new ArgumentException(nameof(pStatus));
        }

        /// <summary>
        /// Sets the Calendar with a new instance
        /// </summary>
        /// <param name="pCalendar"></param>
        public void SetCalendar(Calendar pCalendar)
        {
            Calendar = pCalendar;
            CalendarId = pCalendar.Id;
        }
        /// <summary>
        /// Sets the period type with a new instance
        /// </summary>
        /// <param name="pPeriodType"></param>
        public void SetPeriodType(PeriodType pPeriodType)
        {
            PeriodType = pPeriodType;
            PeriodTypeId = pPeriodType.Id;
        }

    }
}
