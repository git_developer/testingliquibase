﻿using System;

namespace Omnium.StaffPerformance.Business.Domain.Entities
{
    public class CompanyPlatform
    {
        public virtual Company Company { get; private set; }
        public string CompanyId { get; private set; }
        public virtual Platform Platform { get; private set; }
        public string PlatformId { get; private set; }
        public string Status { get; private set; }

        public CompanyPlatform(Company pCompany, Platform pPlatform, string pStatus)
        {
            Company = pCompany ?? throw new ArgumentNullException(nameof(pCompany));
            CompanyId = pCompany.Id;
            Platform = pPlatform ?? throw new ArgumentNullException(nameof(pPlatform));
            PlatformId = pPlatform.Id;
            Status = Enum.IsDefined(typeof(EStatus), pStatus) ? pStatus : throw new ArgumentException(nameof(pStatus));
        }

        public CompanyPlatform(string pCompanyId, string pPlatformId, string pStatus)
        {
            CompanyId = string.IsNullOrEmpty(pCompanyId) ? throw new ArgumentNullException(nameof(pCompanyId)) : pCompanyId;
            PlatformId = string.IsNullOrEmpty(pPlatformId) ? throw new ArgumentNullException(nameof(pPlatformId)) : pPlatformId;
            Status = Enum.IsDefined(typeof(EStatus), pStatus) ? pStatus : throw new ArgumentException(nameof(pStatus));
        }

        protected CompanyPlatform()
        {

        }
    }
}
