﻿using System;
using System.Collections.Generic;

namespace Omnium.StaffPerformance.Business.Domain.Entities
{
    public class ScheduleInterval : IAggregateRoot
    {
        public string Id { get; private set; }
        public string Name { get; private set; }
        public int Interval { get; private set; }
        public int Begin { get; private set; }
        public int End { get; private set; }

        public virtual ICollection<ScheduleIntervalLine> ScheduleIntervalLines { get; private set; }
        public DateTime CreateDate { get; private set; }
        public DateTime UpdateDate { get; private set; }
        public string Status { get; private set; }

        /// <summary>
        /// Initializes a schedule interval
        /// </summary>
        /// <param name="pName">Name of the interval</param>
        /// <param name="pInterval">Duration of the intervak</param>
        /// <param name="pBegin">Beggining hour of the interval</param>
        /// <param name="pEnd">End hour of the interval</param>
        /// <param name="pStatus">status of the registry</param>
        public ScheduleInterval(string pName, int pInterval, int pBegin, int pEnd, string pStatus)
        {
            Id = Guid.NewGuid().ToString().ToUpper();
            Name = string.IsNullOrEmpty(pName) ? throw new ArgumentNullException(nameof(pName)) : pName;
            Interval = pInterval;
            Begin = pBegin;
            End = pEnd;
            Status = Enum.IsDefined(typeof(EStatus), pStatus) ? pStatus : throw new ArgumentException(nameof(pStatus));
            ScheduleIntervalLines = new HashSet<ScheduleIntervalLine>();
        }

        protected ScheduleInterval()
        {

        }
    }
}
