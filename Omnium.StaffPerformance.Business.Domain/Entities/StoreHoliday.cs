﻿using System;

namespace Omnium.StaffPerformance.Business.Domain.Entities
{
    public class StoreHoliday
    {
        public virtual Store Store { get; private set; }
        public string StoreId { get; private set; }
        public virtual Holiday Holiday { get; private set; }
        public string HolidayId { get; private set; }
        /// <summary>
        /// Initializes a StoreHoliday
        /// </summary>
        /// <param name="pStore">object of the store we want to connect to the holiday object</param>
        /// <param name="pHoliday">object of the holiday we want to connect to the store object</param>
        public StoreHoliday(Store pStore, Holiday pHoliday)
        {
            Store = pStore;
            StoreId = pStore.Id;
            Holiday = pHoliday;
            HolidayId = pHoliday.Id;
        }

        protected StoreHoliday()
        {

        }
    }
}
