﻿using Omnium.StaffPerformance.Business.Exceptions;
using System;
using System.Collections.Generic;

namespace Omnium.StaffPerformance.Business.Domain.Entities
{
    public class Store : IAggregateRoot
    {
        public string Id { get; private set; }
        public string Status { get; private set; }

        public virtual ICollection<StoreHoliday> StoreHolidays { get; private set; }
        public DateTime CreateDate { get; private set; }
        public DateTime UpdateDate { get; private set; }

        /// <summary>
        /// Initializes a store
        /// </summary>
        /// <param name="pStatus">current status for the store registry</param>
        public Store(string pStatus)
        {
            Id = Guid.NewGuid().ToString().ToUpper();
            Status = Enum.IsDefined(typeof(EStatus), pStatus) ? pStatus : throw new ArgumentException(nameof(pStatus));
            StoreHolidays = new HashSet<StoreHoliday>();
        }

        protected Store()
        {

        }
        /// <summary>
        /// Adds an object to the storeHoliday collection, creating a connection from store to holiday
        /// </summary>
        /// <param name="pStoreHoliday">StoreHoliday object containing the relationship between store and Holiday</param>
        public void AddStoreHoliday(StoreHoliday pStoreHoliday)
        {
            foreach (var storeHoliday in StoreHolidays)
            {
                if (storeHoliday.HolidayId.Equals(pStoreHoliday.HolidayId) && storeHoliday.StoreId.Equals(pStoreHoliday.StoreId))
                {
                    throw new AssociationXAlreadyExistsCustomException("Store and Holiday");
                }
            }
            StoreHolidays.Add(pStoreHoliday);
        }
    }
}
