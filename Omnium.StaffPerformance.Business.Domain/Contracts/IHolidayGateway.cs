﻿using Omnium.StaffPerformance.Business.Domain.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Omnium.StaffPerformance.Business.Domain.Contracts
{
    public interface IHolidayGateway
    {
        /// <summary>
        /// Gets the existing holidays from a country in a certain year and returns the list of them
        /// </summary>
        /// <param name="pYear">year we want the holidays to be in</param>
        /// <param name="pCountry">Country of the holidays we want to return</param>
        /// <returns>List with the holiday information</returns>
        Task<List<Holiday>> GetHolidaysAsync(int pYear, string pCountry);
    }
}
