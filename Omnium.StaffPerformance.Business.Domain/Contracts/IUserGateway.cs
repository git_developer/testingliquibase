﻿using Omnium.StaffPerformance.Business.Domain.Entities;
using System.Threading.Tasks;

namespace Omnium.StaffPerformance.Business.Domain.Contracts

{
    public interface IUserGateway
    {
        Task<string> CreateUserAsync(Employee pEmployee);

    }
}
