﻿using Omnium.StaffPerformance.Business.Domain.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Omnium.StaffPerformance.Business.Domain.Services.Holidays
{
    public interface IHolidayService
    {
        /// <summary>
        /// Receives the holidays from nager's holiday api and adds them to the db
        /// </summary>
        /// <param name="pYear">year we want to receive the holidays from</param>
        /// <param name="pCountryName">country we want to receive the holidays from</param>
        /// <returns>the number of holidays added</returns>
        Task<int> ImportHolidaysAsync(int pYear, string pCountryName);
        /// <summary>
        /// Adds an Holiday to the database if it doesnt exist there already
        /// </summary>
        /// <param name="pHoliday">Holiday we want to add</param>
        void AddHoliday(Holiday pHoliday);
        /// <summary>
        /// Gets an holiday from the database given its id
        /// </summary>
        /// <param name="pId">id of the holiday we want</param>
        /// <returns>the holiday with that id</returns>
        Holiday GetHolidayById(string pId);
        /// <summary>
        /// Gets all the holidays from the database
        /// </summary>
        /// <returns>all the holidays found</returns>
        List<Holiday> GetHolidays();
        /// <summary>
        /// Updates an holiday database entry
        /// </summary>
        /// <param name="pId">id of the holiday we want to update</param>
        /// <param name="pHoliday">Holiday object with the new information</param>
        /// <returns>the updated holiday object</returns>
        Holiday UpdateHoliday(string pId, Holiday pHoliday);
        /// <summary>
        /// Creates the association between holiday and store
        /// </summary>
        /// <param name="pStoreId">Id of the store to associate</param>
        /// <param name="pHolidayId">Id of the holiday to associate</param>
        /// <returns>The object representing the association</returns>
        StoreHoliday AssociateHolidayToStore(string pStoreId, string pHolidayId);


    }
}
