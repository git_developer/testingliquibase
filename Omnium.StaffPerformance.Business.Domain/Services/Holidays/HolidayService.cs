﻿using Omnium.StaffPerformance.Business.Domain.Contracts;
using Omnium.StaffPerformance.Business.Domain.Entities;
using Omnium.StaffPerformance.Business.Domain.Repositories;
using Omnium.StaffPerformance.Business.Exceptions;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Omnium.StaffPerformance.Business.Domain.Services.Holidays
{
    public class HolidayService : IHolidayService
    {
        private readonly IUnitOfWork myUnitOfWork;
        private readonly IHolidayRepository myHolidayRepository;
        private readonly IHolidayGateway myHolidayGateway;
        /// <summary>
        /// Initializes the holiday service
        /// </summary>
        /// <param name="pUnitOfWork">unit of work of current transaction</param>
        /// <param name="pGateway">gateway to Nager's holiday API</param>
        public HolidayService(IUnitOfWork pUnitOfWork, IHolidayGateway pGateway)
        {
            myUnitOfWork = pUnitOfWork;
            myHolidayRepository = myUnitOfWork.Holidays;
            myHolidayGateway = pGateway;
        }
        /// <summary>
        /// Receives the holidays from nager's holiday api and adds them to the db
        /// </summary>
        /// <param name="pYear">year we want to receive the holidays from</param>
        /// <param name="pCountryName">country we want to receive the holidays from</param>
        /// <returns>the number of holidays added</returns>
        public async Task<int> ImportHolidaysAsync(int pYear, string pCountryName)
        {

            var country = myUnitOfWork.Countries.GetCountryByName(pCountryName);

            if (country == null)
            {
                throw new EntityXNotFoundCustomException("Country","Name");
            }

            var holidayList= await  myHolidayGateway.GetHolidaysAsync(pYear, country.ISO2);


            int contAux = 0;

            foreach(var holiday in holidayList)
            {
                if (myHolidayRepository.GetHolidayByNameCountryCodeAndDate(holiday.Date, country.Id, holiday.Name) == null)
                {
                    Holiday holidayWithCountryId = new Holiday(holiday.Date, holiday.Name, country.Id, "A");
                    myHolidayRepository.Add(holidayWithCountryId);
                    contAux++;
                }
            }
            
            myUnitOfWork.Commit();
            return contAux;
        }
        /// <summary>
        /// Adds an Holiday to the database if it doesnt exist there already
        /// </summary>
        /// <param name="pHoliday">Holiday we want to add</param>
        public void AddHoliday(Holiday pHoliday)
        {
            if (myHolidayRepository.GetHolidayByCountryCodeAndDate(pHoliday.Date, pHoliday.CountryId) != null)
            {
                throw new EntityXAlreadyExistsCustomException("Holiday");
            }

            if (myUnitOfWork.Countries.GetById(pHoliday.CountryId)==null)
            {
                throw new EntityXNotFoundCustomException("Country","Id");
            }

            myHolidayRepository.Add(pHoliday);
            myUnitOfWork.Commit();
        }
        /// <summary>
        /// Gets an holiday from the database given its id
        /// </summary>
        /// <param name="pId">id of the holiday we want</param>
        /// <returns>the holiday with that id</returns>
        public Holiday GetHolidayById(string pId)
        {
            var holiday = myHolidayRepository.GetHolidayById(pId);

            if(holiday== null)
            {
                throw new EntityXNotFoundCustomException("Holiday","Id");
            }

            return holiday;
        }
        /// <summary>
        /// Gets all the holidays from the database
        /// </summary>
        /// <returns>all the holidays found</returns>
        public List<Holiday> GetHolidays()
        {
            return myHolidayRepository.GetHolidays();
        }
        /// <summary>
        /// Updates an holiday database entry
        /// </summary>
        /// <param name="pId">id of the holiday we want to update</param>
        /// <param name="pHoliday">Holiday object with the new information</param>
        /// <returns>the updated holiday object</returns>
        public Holiday UpdateHoliday(string pId, Holiday pHoliday)
        {
            var holiday = myHolidayRepository.GetHolidayById(pId);

            if (holiday == null)
            {
                throw new EntityXNotFoundCustomException("Holiday", "Id");
            }

            if (myUnitOfWork.Countries.GetById(pHoliday.CountryId) == null)
            {
                throw new EntityXNotFoundCustomException("Country", "Id");
            }

            holiday.Update(pHoliday.Date, pHoliday.Name, pHoliday.CountryId, pHoliday.Status);

            myUnitOfWork.Commit();

            return holiday;
        }
        /// <summary>
        /// Creates the association between holiday and store
        /// </summary>
        /// <param name="pStoreId">Id of the store to associate</param>
        /// <param name="pHolidayId">Id of the holiday to associate</param>
        /// <returns>The object representing the association</returns>
        public StoreHoliday AssociateHolidayToStore(string pStoreId, string pHolidayId)
        {
            var holiday = myHolidayRepository.GetHolidayById(pHolidayId);

            if (holiday == null)
            {
                throw new EntityXNotFoundCustomException("Holiday", "Id");
            }
            
            var store = myUnitOfWork.Stores.GetStoreById(pStoreId);

            if (store == null)
            {
                throw new EntityXNotFoundCustomException("Store", "Id");
            }

            var storeHoliday = new StoreHoliday(store, holiday);

            holiday.AddStoreHoliday(storeHoliday);

            myUnitOfWork.Commit();

            return storeHoliday;
        }
    }
}
