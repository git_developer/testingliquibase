﻿using Omnium.StaffPerformance.Business.Domain.Entities;
using Omnium.StaffPerformance.Business.Domain.Repositories;
using Omnium.StaffPerformance.Business.Exceptions;
using System;

namespace Omnium.StaffPerformance.Business.Domain.Services.Employees
{
    public class EmployeeService: IEmployeeService
    {
        private readonly IUnitOfWork myUnitOfWork;
        private readonly IEmployeeRepository myEmployeeRepository;

        /// <summary>
        /// Initializes a service
        /// </summary>
        /// <param name="pUnitOfWork">Unit of work of current transaction</param>
        public EmployeeService(IUnitOfWork pUnitOfWork)
        {
            myUnitOfWork = pUnitOfWork ?? throw new ArgumentNullException(nameof(pUnitOfWork));
            myEmployeeRepository = pUnitOfWork.Employees;
        }

        /// <summary>
        /// Adds an Employee to the database
        /// </summary>
        /// <param name="pEmployee">Employee instance to be added</param>
        public void AddEmployee(Employee pEmployee)
        {
            var existingEmployee=myEmployeeRepository.GetEmployeeByEmail(pEmployee.Email);

            if(existingEmployee == null)
            {
                myEmployeeRepository.Add(pEmployee);
            }
            else
            {
                throw new EntityXAlreadyExistsCustomException("Employee");
            }
            myUnitOfWork.Commit();
        }

        /// <summary>
        /// Gets an employee from the database with a specified ID
        /// </summary>
        /// <param name="pEmployeeID">ID of the employee we are looking for</param>
        /// <returns>Employee with the specified ID</returns>
        public Employee GetEmployeeById(string pEmployeeID)
        {
            var employeeWithID = myEmployeeRepository.GetEmployeeById(pEmployeeID);

            if(employeeWithID == null)
            {
                throw new EntityXNotFoundCustomException("Employee","Id");
            }

            return employeeWithID;
        }
        /// <summary>
        /// Updates the information of an employee
        /// </summary>
        /// <param name="pId">the id of the employee to update</param>
        /// <param name="pEmployee">Employee object with info to replace the database information</param>
        /// <returns>the updated employee</returns>
        public Employee UpdateEmployee(string pId,Employee pEmployee)
        {
            var dbEmployee = myEmployeeRepository.GetEmployeeById(pId);

            if (dbEmployee == null)
            {
                throw new EntityXNotFoundCustomException("Employee", "Id");
            }

            dbEmployee.Update(pEmployee.Company, pEmployee.NameShort, pEmployee.NameFull, pEmployee.Gender, pEmployee.Email,
                                pEmployee.MobilePhoneNumber, pEmployee.PhoneNumber, pEmployee.BirthDate, pEmployee.Status);

            myUnitOfWork.Commit();

            return dbEmployee;

            

        }
        /// <summary>
        /// Retrieves the company with the specified Id and returns it
        /// </summary>
        /// <param name="pId">id of the company</param>
        /// <returns>The company with the specified Id</returns>
        public Company GetCompanyById(string pId)
        {
            var comp = myUnitOfWork.Companies.GetById(pId);

            if(comp== null)
            {
                throw new EntityXNotFoundCustomException("Company", "Id");
            }
            return comp;
        }

    }
}
