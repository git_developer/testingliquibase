﻿using Omnium.StaffPerformance.Business.Domain.Entities;

namespace Omnium.StaffPerformance.Business.Domain.Services.Employees
{
    public interface IEmployeeService
    {
        /// <summary>
        /// Adds an Employee to the database
        /// </summary>
        /// <param name="pEmployee">Employee instance to be added</param>
        void AddEmployee(Employee pEmployee);
        /// <summary>
        /// Gets an employee from the database with a specified ID
        /// </summary>
        /// <param name="pEmployeeID">ID to search for</param>
        /// <returns>Employee instance</returns>
        Employee GetEmployeeById(string pEmployeeID);
        /// <summary>
        /// Updates the information of an employee
        /// </summary>
        /// <param name="pId">the id of the employee to update</param>
        /// <param name="pEmployee">Employee object with info to replace the database information</param>
        /// <returns>the updated employee</returns>
        Employee UpdateEmployee(string pId,Employee pEmployee);
        /// <summary>
        /// Retrieves the company with the specified Id and returns it
        /// </summary>
        /// <param name="pId">id of the company</param>
        /// <returns>The company with the specified Id</returns>
        Company GetCompanyById(string pId);
    }
}
