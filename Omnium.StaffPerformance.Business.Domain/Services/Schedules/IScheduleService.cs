﻿using Omnium.StaffPerformance.Business.Domain.Entities;
using System;
using System.Collections.Generic;

namespace Omnium.StaffPerformance.Business.Domain.Services.Schedules
{
    public interface IScheduleService
    {
        /// <summary>
        /// Gets the schedules for all the employees on a list
        /// </summary>
        /// <param name="pEmployeeIds">list of employee ids we want the schedules from</param>
        /// <param name="pDay">Day we want the schedules from</param>
        /// <returns>A list containing the Id of the employee and his schedule</returns>
        Dictionary<string, List<Schedule>> GetSchedules(List<string> pEmployeeIds, DateTime pDay);
        /// <summary>
        /// Adds a list of schedules to one employee
        /// </summary>
        /// <param name="pSchedules">List of schedules</param>
        /// <param name="pEmployeeId">Id of the employee of whom the schedules are from</param>
        void CreateSchedules(List<Schedule> pSchedules, string pEmployeeId);
        /// <summary>
        /// Updates a list of schedules
        /// </summary>
        /// <param name="pIdAndSchedules">key value pair list with key being the id of an existing schedule and value being the schedule with the information we want to replace with</param>
        /// <returns>List with the changed schedules</returns>
        List<Schedule> UpdateSchedules(Dictionary<string, Schedule> pIdAndSchedules);
        /// <summary>
        /// Retrieves the company with the specified Id and returns it
        /// </summary>
        /// <param name="pId">id of the company</param>
        /// <returns>The company with the specified Id</returns>
        Company GetCompanyById(string pId);
            /// <summary>
            /// Retrieves the store with the specified Id and returns it
            /// </summary>
            /// <param name="pId">id of the store</param>
            /// <returns>The store with the specified Id</returns>
        Store GetStoreyById(string pId);

    }
}
