﻿using Omnium.StaffPerformance.Business.Domain.Entities;
using Omnium.StaffPerformance.Business.Domain.Repositories;
using Omnium.StaffPerformance.Business.Exceptions;
using System;
using System.Collections.Generic;

namespace Omnium.StaffPerformance.Business.Domain.Services.Schedules
{
    public class ScheduleService : IScheduleService
    {
        private readonly IUnitOfWork myUnitOfWork;
        private readonly IScheduleRepository myScheduleRepository;

        /// <summary>
        /// Initializes a service
        /// </summary>
        /// <param name="pUnitOfWork">Unit of work of current transaction</param>
        public ScheduleService(IUnitOfWork pUnitOfWork)
        {
            myUnitOfWork = pUnitOfWork ?? throw new ArgumentNullException(nameof(pUnitOfWork));
            myScheduleRepository = myUnitOfWork.Schedules;
        }

        /// <summary>
        /// Gets the schedules for all the employees on a list
        /// </summary>
        /// <param name="pEmployeeIds">list of employee ids we want the schedules from</param>
        /// <param name="pDay">Day we want the schedules from</param>
        /// <returns>A list containing the Id of the employee and his schedule</returns>
        public Dictionary<string, List<Schedule>> GetSchedules(List<string> pEmployeeIds, DateTime pDay)
        {
            Dictionary<string, List<Schedule>> returnList = new Dictionary<string, List<Schedule>>();

            foreach(var employee in pEmployeeIds)
            {
                if (!myUnitOfWork.Employees.CheckEmployeeExists(employee))
                {
                    throw new EntityXNotFoundCustomException("Employee", "Id");
                }

                var allEmployeeSchedule = myScheduleRepository.GetScheduleByEmployeeIdAndDay(employee, pDay);

                returnList.Add(employee, allEmployeeSchedule);
            }
            
            return returnList;
        }

        /// <summary>
        /// Adds a list of schedules to one employee
        /// </summary>
        /// <param name="pSchedules">List of schedules</param>
        /// <param name="pEmployeeId">Id of the employee of whom the schedules are from</param>
        public void CreateSchedules(List<Schedule> pSchedules, string pEmployeeId)
        {

            var employee = myUnitOfWork.Employees.GetEmployeeById(pEmployeeId);

            if (employee==null)
            {
                throw new EntityXNotFoundCustomException("Employee", "Id");
            }

            foreach(var schedule in pSchedules)
            {
                if (myScheduleRepository.CheckExistsAnotherSchedule(schedule))
                {
                    throw new ScheduleIntersectionCustomException();
                }

                schedule.SetCalendar(myUnitOfWork.Calendars.GetById(schedule.CalendarId));
                schedule.SetPeriodType(myUnitOfWork.PeriodTypes.GetPeriodTypeById(schedule.PeriodTypeId));

                myScheduleRepository.Add(schedule);
            }
            myUnitOfWork.Commit();
        }

        /// <summary>
        /// Updates a list of schedules
        /// </summary>
        /// <param name="pIdAndSchedules">key value pair list with key being the id of an existing schedule and value being the schedule with the information we want to replace with</param>
        /// <returns>List with the changed schedules</returns>
        public List<Schedule> UpdateSchedules(Dictionary<string, Schedule> pIdAndSchedules )
        {
            List<Schedule> listReturn = new List<Schedule>();

            foreach (var schedule in pIdAndSchedules)
            {
                if (myScheduleRepository.CheckExistsAnotherSchedule(schedule.Value))
                {
                    throw new ScheduleIntersectionCustomException();
                }

                var dbSchedule=myScheduleRepository.GetScheduleById(schedule.Key);
                var tempSchedule = schedule.Value;
                dbSchedule.SetCalendar(myUnitOfWork.Calendars.GetById(tempSchedule.CalendarId));
                dbSchedule.SetPeriodType(myUnitOfWork.PeriodTypes.GetPeriodTypeById(tempSchedule.PeriodTypeId));
                dbSchedule.Update(tempSchedule.EmployeeId, tempSchedule.Store, tempSchedule.CalendarId, tempSchedule.Company, tempSchedule.PeriodTypeId, tempSchedule.Begin, tempSchedule.End, tempSchedule.Status);

                listReturn.Add(dbSchedule);
            }
            myUnitOfWork.Commit();

            return listReturn;
        }

        /// <summary>
        /// Retrieves the company with the specified Id and returns it
        /// </summary>
        /// <param name="pId">id of the company</param>
        /// <returns>The company with the specified Id</returns>
        public Company GetCompanyById(string pId)
        {
            var comp = myUnitOfWork.Companies.GetById(pId);

            if (comp == null)
            {
                throw new EntityXNotFoundCustomException("Company", "Id");
            }
            return comp;
        }
        /// <summary>
        /// Retrieves the store with the specified Id and returns it
        /// </summary>
        /// <param name="pId">id of the store</param>
        /// <returns>The store with the specified Id</returns>
        public Store GetStoreyById(string pId)
        {
            var comp = myUnitOfWork.Stores.GetStoreById(pId);

            if (comp == null)
            {
                throw new EntityXNotFoundCustomException("Store", "Id");
            }
            return comp;
        }
    }
}
