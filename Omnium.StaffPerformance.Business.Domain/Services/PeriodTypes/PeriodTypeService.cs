﻿using Omnium.StaffPerformance.Business.Domain.Entities;
using Omnium.StaffPerformance.Business.Domain.Repositories;
using Omnium.StaffPerformance.Business.Exceptions;
using System;
using System.Collections.Generic;

namespace Omnium.StaffPerformance.Business.Domain.Services.PeriodTypes
{
    public class PeriodTypeService : IPeriodTypeService
    {
        private readonly IUnitOfWork myUnitOfWork;
        private readonly IPeriodTypeRepository myPeriodTypeRepository;

        /// <summary>
        /// Initializes a service
        /// </summary>
        /// <param name="pUnitOfWork">Unit of work of current transaction</param>
        public PeriodTypeService(IUnitOfWork pUnitOfWork)
        {
            myUnitOfWork = pUnitOfWork ?? throw new ArgumentNullException(nameof(pUnitOfWork));
            myPeriodTypeRepository = myUnitOfWork.PeriodTypes;
        }
        /// <summary>
        /// Creates a period type in the database
        /// </summary>
        /// <param name="pPeriodType">period type object we want to add to the database</param>
        public void AddPeriodType(PeriodType pPeriodType)
        {
            var existingPeriodType = myPeriodTypeRepository.GetPeriodTypeByName(pPeriodType.Name);

            if (existingPeriodType != null)
            {
                throw new EntityXAlreadyExistsCustomException("Period Type");
            }

            myPeriodTypeRepository.Add(pPeriodType);
            myUnitOfWork.Commit();
            
        }
        /// <summary>
        /// Gets a periodType with the specified Id
        /// </summary>
        /// <param name="pId">id of the period type we want to fetch</param>
        /// <returns>the requested period type</returns>
        public PeriodType GetPeriodTypeById(string pId)
        {
            var periodType = myPeriodTypeRepository.GetPeriodTypeById(pId);

            if (periodType == null)
            {
                throw new EntityXNotFoundCustomException("Period Type", "Id");
            }

            return periodType;
        }
        /// <summary>
        /// Gets all the existing period types from the repository
        /// </summary>
        /// <returns>List with period types</returns>
        public List<PeriodType> GetPeriodTypes()
        {
            return  myPeriodTypeRepository.GetPeriodTypes();
        }

        /// <summary>
        /// Updates a period type received from the database with the selected Id
        /// </summary>
        /// <param name="pId">Id of the period type</param>
        /// <param name="pPeriodType">object of the period type with the parameters we want</param>
        /// <returns>the updated periodType</returns>
        public PeriodType UpdatePeriodType(string pId, PeriodType pPeriodType)
        {
            var periodType = myPeriodTypeRepository.GetPeriodTypeById(pId);

            if (periodType == null)
            {
                throw new EntityXNotFoundCustomException("Period Type", "Id");
            }

            periodType.Update(pPeriodType.Company,pPeriodType.Name, pPeriodType.Value, pPeriodType.Status);

            myUnitOfWork.Commit();

            return periodType;
        }
        /// <summary>
        /// Retrieves the company with the specified Id and returns it
        /// </summary>
        /// <param name="pId">id of the company</param>
        /// <returns>The company with the specified Id</returns>
        public Company GetCompanyById(string pId)
        {
            var comp = myUnitOfWork.Companies.GetById(pId);

            if (comp == null)
            {
                throw new EntityXNotFoundCustomException("Company", "Id");
            }
            return comp;
        }
    }
}
