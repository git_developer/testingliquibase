﻿using Omnium.StaffPerformance.Business.Domain.Entities;
using System.Collections.Generic;

namespace Omnium.StaffPerformance.Business.Domain.Services.PeriodTypes
{
    public interface IPeriodTypeService
    {
        /// <summary>
        /// Creates a period type in the database
        /// </summary>
        /// <param name="pPeriodType">period type object we want to add to the database</param>
        void AddPeriodType(PeriodType pPeriodType);
        /// <summary>
        /// Gets a periodType with the specified Id
        /// </summary>
        /// <param name="pId">id of the period type we want to fetch</param>
        /// <returns>the requested period type</returns>
        PeriodType GetPeriodTypeById(string pId);
        /// <summary>
        /// Gets all the existing period types from the repository
        /// </summary>
        /// <returns>List with period types</returns>
        List<PeriodType> GetPeriodTypes();
        /// <summary>
        /// Updates a period type received from the database with the selected Id
        /// </summary>
        /// <param name="pId">Id of the period type</param>
        /// <param name="pPeriodType">object of the period type with the parameters we want</param>
        /// <returns>the updated periodType</returns>
        PeriodType UpdatePeriodType(string pId, PeriodType pPeriodType);
        /// <summary>
        /// Retrieves the company with the specified Id and returns it
        /// </summary>
        /// <param name="pId">id of the company</param>
        /// <returns>The company with the specified Id</returns>
        Company GetCompanyById(string pId);
    }
}
