﻿using Omnium.StaffPerformance.Business.Domain.Entities;
using System;
using System.Collections.Generic;

namespace Omnium.StaffPerformance.Business.Domain.Services.Attendances
{
    public interface IAttendanceService
    {
        /// <summary>
        /// Adds an Attendance to the database
        /// </summary>
        /// <param name="pAttendance">Attendance instance we want to add</param>
        void AddAttendance(Attendance pAttendance);
        /// <summary>
        /// Gets attendances from one employee from the database and returns only those within the time period specified
        /// </summary>
        /// <param name="pStart">Start of the interval</param>
        /// <param name="pFinish">end of the interval</param>
        /// <param name="pEmployeeId">Id of the employee we want the attendances from</param>
        /// <returns>List of attendances by an employee within the specified period</returns>
        List<Attendance> GetAttendancesByInterval(DateTime pStart, DateTime pFinish, string pEmployeeId);
        /// <summary>
        /// Retrieves the company with the specified Id and returns it
        /// </summary>
        /// <param name="pId">id of the company</param>
        /// <returns>The company with the specified Id</returns>
        Company GetCompanyById(string pId);
    }
}
