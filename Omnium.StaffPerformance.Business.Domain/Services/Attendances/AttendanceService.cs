﻿using Omnium.StaffPerformance.Business.Domain.Entities;
using Omnium.StaffPerformance.Business.Domain.Repositories;
using Omnium.StaffPerformance.Business.Exceptions;
using System;
using System.Collections.Generic;

namespace Omnium.StaffPerformance.Business.Domain.Services.Attendances
{
    public class AttendanceService : IAttendanceService
    {
        private readonly IUnitOfWork myUnitOfWork;
        private readonly IAttendanceRepository myAttendanceRepository;
        private readonly IEmployeeRepository myEmployeeRepository;

        public AttendanceService(IUnitOfWork pUnitOfWork)
        {
            myUnitOfWork = pUnitOfWork;
            myAttendanceRepository = myUnitOfWork.Attendances;
            myEmployeeRepository = myUnitOfWork.Employees;
        }

        /// <summary>
        /// Adds an Attendance to the database
        /// </summary>
        /// <param name="pAttendance">Attendance instance we want to add</param>
        public void AddAttendance(Attendance pAttendance)
        {

            if (!Enum.IsDefined(typeof(EAttendanceType),pAttendance.Type))
            {
                throw new InvalidAttendanceTypeCustomException();
            }

            var employee = myEmployeeRepository.GetEmployeeById(pAttendance.EmployeeId);

            if (employee == null)
            {
                throw new EntityXNotFoundCustomException("Employee", "Id");
            }

            var lastAttendance = myAttendanceRepository.GetLastAttendanceByEmployeeId(pAttendance.EmployeeId);

            if (lastAttendance!=null && pAttendance.Type == lastAttendance.Type)
            {
                throw new SameAttendanceTypeCustomException();
            }
            else
            {
                myAttendanceRepository.Add(pAttendance);
                myUnitOfWork.Commit();
            }

        }
        /// <summary>
        /// Gets attendances from one employee from the database and returns only those within the time period specified
        /// </summary>
        /// <param name="pStart">Start of the interval</param>
        /// <param name="pFinish">end of the interval</param>
        /// <param name="pEmployeeId">Id of the employee we want the attendances from</param>
        /// <returns>List of attendances by an employee within the specified period</returns>
        public List<Attendance> GetAttendancesByInterval(DateTime pStart, DateTime pFinish, string pEmployeeId)
        {
            var employeeWithID = myEmployeeRepository.GetEmployeeById(pEmployeeId);

            if (employeeWithID == null)
            {
                throw new EntityXNotFoundCustomException("Employee", "Id");
            }

            var attendancesList = myAttendanceRepository.GetAttendancesByEmployeeId(pEmployeeId);
            List<Attendance> returnValues = new List<Attendance>();

            foreach(var attendance in attendancesList)
            {
                if(IsBetween(attendance.Date, pStart, pFinish))
                {
                    returnValues.Add(attendance);
                }
            }

            return returnValues;
        }
        /// <summary>
        /// Checks if a datetime is between two other datetimes
        /// </summary>
        /// <param name="check">datetime to check</param>
        /// <param name="start">start of the interval</param>
        /// <param name="finish">end of interval</param>
        /// <returns>boolean representing if it is in between or not</returns>
        public bool IsBetween(DateTime check, DateTime start, DateTime finish)
        {
            return check > start && check < finish;
        }

        /// <summary>
        /// Retrieves the company with the specified Id and returns it
        /// </summary>
        /// <param name="pId">id of the company</param>
        /// <returns>The company with the specified Id</returns>
        public Company GetCompanyById(string pId)
        {
            var comp = myUnitOfWork.Companies.GetById(pId);

            if (comp == null)
            {
                throw new EntityXNotFoundCustomException("Company", "Id");
            }
            return comp;
        }
    }
}
