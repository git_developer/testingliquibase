﻿using Omnium.StaffPerformance.Business.Domain.Entities;

namespace Omnium.StaffPerformance.Business.Domain.Services.Companies
{
    public interface ICompanyService
    {
        /// <summary>
        /// Checks if the <paramref name="pNewCompany"/> already exists, if exists updates the company with the new values otherwise adds the company.
        /// </summary>
        /// <param name="pNewCompany">company to create or update</param>
        void AddOrUpdate(Company pNewCompany);
    }
}
