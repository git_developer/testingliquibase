﻿using Omnium.StaffPerformance.Business.Domain.Entities;
using Omnium.StaffPerformance.Business.Domain.Repositories;
using System;

namespace Omnium.StaffPerformance.Business.Domain.Services.Companies
{

    public class CompanyService : ICompanyService
    {
        private readonly IUnitOfWork myUnitOfWork;

        public CompanyService(IUnitOfWork pUnitOfWork)
        {
            myUnitOfWork = pUnitOfWork ?? throw new ArgumentNullException(nameof(pUnitOfWork));
        }


        public void AddOrUpdate(Company pNewCompany)
        {
            var dbCompany = myUnitOfWork.Companies.GetById(pNewCompany.Id);

            if (dbCompany == null)
            {
                myUnitOfWork.Companies.Add(pNewCompany);
            }
            else
            {
                dbCompany.Update(pNewCompany.CompanyCode, pNewCompany.Name, pNewCompany.Street, pNewCompany.City, pNewCompany.ZipCode, pNewCompany.CountryId, pNewCompany.Status);
            }

            myUnitOfWork.Commit();
        }

    }
}
