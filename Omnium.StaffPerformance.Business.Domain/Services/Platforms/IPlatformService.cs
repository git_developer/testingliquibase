﻿using Omnium.StaffPerformance.Business.Domain.Entities;

namespace Omnium.StaffPerformance.Business.Domain.Services.Platforms
{
    public interface IPlatformService
    {
        /// <summary>
        /// Checks if the <paramref name="pNewPlatform"/> already exists, if exists updates the platform with the new values otherwise adds the platform.
        /// </summary>
        /// <param name="pNewPlatform">platform to create or update</param>
        void AddOrUpdate(Platform pNewPlatform);
    }
}