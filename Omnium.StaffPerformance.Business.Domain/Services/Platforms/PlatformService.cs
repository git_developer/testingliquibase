﻿using Omnium.StaffPerformance.Business.Domain.Entities;
using Omnium.StaffPerformance.Business.Domain.Repositories;
using System;

namespace Omnium.StaffPerformance.Business.Domain.Services.Platforms
{
    public class PlatformService : IPlatformService
    {
        private readonly IUnitOfWork myUnitOfWork;

        public PlatformService(IUnitOfWork pUnitOfWork)
        {
            myUnitOfWork = pUnitOfWork ?? throw new ArgumentNullException(nameof(pUnitOfWork));
        }

        public void AddOrUpdate(Platform pNewPlatform)
        {
            var dbPlatform = myUnitOfWork.Platforms.GetById(pNewPlatform.Id);

            if (dbPlatform == null)
            {
                myUnitOfWork.Platforms.Add(pNewPlatform);
            }
            else
            {
                dbPlatform.Update(pNewPlatform.Name, pNewPlatform.PlatformType, pNewPlatform.IntegrationData, pNewPlatform.Status);
            }

            myUnitOfWork.Commit();
        }
    }
}
