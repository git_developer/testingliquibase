﻿using Omnium.StaffPerformance.Business.Domain.Entities;

namespace Omnium.StaffPerformance.Business.Domain.Services.Countries
{
    public interface ICountryService
    {
        /// <summary>
        /// Checks if the <paramref name="pNewCountry"/> already exists, if exists updates the country with the new values otherwise adds the country.
        /// </summary>
        /// <param name="pNewCountry">country to create or update</param>
        void AddOrUpdate(Country pNewCountry);
    }
}
