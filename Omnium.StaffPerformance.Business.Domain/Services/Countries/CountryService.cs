﻿using Omnium.StaffPerformance.Business.Domain.Entities;
using Omnium.StaffPerformance.Business.Domain.Repositories;
using System;

namespace Omnium.StaffPerformance.Business.Domain.Services.Countries
{

    public class CountryService : ICountryService
    {

        private readonly IUnitOfWork myUnitOfWork;

        public CountryService(IUnitOfWork pUnitOfWork)
        {
            myUnitOfWork = pUnitOfWork ?? throw new ArgumentNullException(nameof(pUnitOfWork));
        }

        public void AddOrUpdate(Country pNewCountry)
        {
            var dbCountry = myUnitOfWork.Countries.GetById(pNewCountry.Id);

            if (dbCountry == null)
            {
                myUnitOfWork.Countries.Add(pNewCountry);
            }
            else
            {
                dbCountry.Update(pNewCountry.Name, pNewCountry.ISO2, pNewCountry.ISO3, pNewCountry.Status);
            }

            myUnitOfWork.Commit();
        }

    }
}
