﻿using Omnium.StaffPerformance.Business.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Omnium.StaffPerformance.Data.Nager.IntegrationTests.Helpers
{
    public class HolidayEqualityComparator : IEqualityComparer<Holiday>
    {

        public bool Equals(Holiday x, Holiday y)
        {
            bool dateIsEqual = false;
            if(DateTime.Compare(x.Date, y.Date)==0)
            {
                dateIsEqual = true;
            }

            bool nameIsEqual = x.Name.Equals(y.Name);
            bool countryIdIsEqual = x.CountryId.Equals(y.CountryId);

            return dateIsEqual && nameIsEqual && countryIdIsEqual;

        }

        public int GetHashCode([DisallowNull] Holiday obj)
        {
            return obj.Date.GetHashCode() + obj.Name.GetHashCode() + obj.CountryId.GetHashCode();
        }
    }
}
