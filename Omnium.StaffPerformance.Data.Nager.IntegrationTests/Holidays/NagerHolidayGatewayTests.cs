﻿using Omnium.StaffPerformance.Business.Domain.Entities;
using Omnium.StaffPerformance.Business.Exceptions;
using Omnium.StaffPerformance.Data.Nager.Holidays;
using Omnium.StaffPerformance.Data.Nager.IntegrationTests.Helpers;
using Omnium.StaffPerformance.Data.Nager.Models;
using System;
using System.Collections.Generic;
using Xunit;

namespace Omnium.StaffPerformance.Data.Nager.IntegrationTests.Holidays
{
    public class NagerHolidayGatewayTests
    {
        /// <summary>
        /// Ensures that invalid ISO Codes for countries return an exception
        /// </summary>
        [Fact]
        public void GetHolidaysAsync_GetHolidaysWithInvalidCountry_ThrowsException()
        {
            var holidayGateway = new NagerHolidayGateway("https://date.nager.at/api/");

            Assert.ThrowsAsync<POSCustomException>(() => holidayGateway.GetHolidaysAsync(2021, "TEST"));

        }
        /// <summary>
        /// Ensures that invalid years (year 1000000000 for example) return an exception
        /// </summary>
        [Fact]
        public void GetHolidaysAsync_GetHolidaysWithInvalidYear_ThrowsException()
        {
            var holidayGateway = new NagerHolidayGateway("https://date.nager.at/api/");

            Assert.ThrowsAsync<POSCustomException>(() => holidayGateway.GetHolidaysAsync(1000000000, "PT"));

        }
        /// <summary>
        /// Ensures that valid parameters return an existing list with holidays. Requires the existence of Andorra in country Database
        /// </summary>
        [Fact]
        public async void GetHolidaysAsync_GetHolidaysWithValidInputs_ReturnsHolidayList()
        {
            Holiday h1 = new Holiday(new DateTime(2021, 01, 01), "New Year's Day","AD","A");
            Holiday h2 = new Holiday(new DateTime(2021, 03, 14), "Constitution Day", "AD", "A");
            Holiday h3 = new Holiday(new DateTime(2021, 03, 14), "National Holiday", "AD", "A");
            Holiday h4 = new Holiday(new DateTime(2021, 12, 25), "Christmas Day", "AD", "A");
            List<Holiday> listExpected = new List<Holiday>();
            listExpected.Add(h1);
            listExpected.Add(h2);
            listExpected.Add(h3);
            listExpected.Add(h4);

            var holidayGateway = new NagerHolidayGateway("https://date.nager.at/api/");

            var response= await holidayGateway.GetHolidaysAsync(2021, "AD");

            Assert.NotNull(response);
            Assert.True(response.Count > 0);

            int cont = 0;

            foreach(Holiday h in response)
            {
                Assert.Equal(listExpected[cont], h,new HolidayEqualityComparator() );
                cont++;
            }

        }

    }
}
