﻿using Amazon;
using Amazon.SecretsManager;
using Amazon.SecretsManager.Extensions.Caching;
using System;
using System.Text.Json;
using System.Threading.Tasks;

namespace Omnium.StaffPerformance.Data.AWS.SecretManagers
{
    /// <summary>
    /// Must be use as Singleton
    /// </summary>
    public class AWSSecretManagerCacheGateway : ISecretManagerGateway
    {
        private const uint AWS_REFRESH_INTERVAL = 86400000;

        private readonly string mySecretName;
        private readonly IAmazonSecretsManager mySecretsManager;
        private readonly SecretsManagerCache mySecretsManagerCache;

        /// <summary>
        /// Initializes a AWSSecretManagerGateway
        /// </summary>
        /// <param name="pSecretName">secret manager name from which secrets will be retrieve</param>
        public AWSSecretManagerCacheGateway(string pSecretName)
        {
            mySecretName = string.IsNullOrEmpty(pSecretName) ? throw new ArgumentNullException(nameof(pSecretName)) : pSecretName;
            mySecretsManager = new AmazonSecretsManagerClient(RegionEndpoint.EUWest1);
            mySecretsManagerCache = new SecretsManagerCache(mySecretsManager, new SecretCacheConfiguration { CacheItemTTL = AWS_REFRESH_INTERVAL });
        }

        public Task<string> GetSecretAsync(string pSecretKey = null)
        {
            var secret = GetSecretAmazonSecretsManagerAsync(pSecretKey);
            return secret;
        }

        public async Task<T> GetSecretAsync<T>()
        {
            var secret = await GetSecretAmazonSecretsManagerAsync(null);
            var secrets = JsonSerializer.Deserialize<T>(secret);
            return secrets;
        }

        private async Task<string> GetSecretAmazonSecretsManagerAsync(string pSecretKey = null)
        {
            var response = await mySecretsManagerCache.GetSecretString(mySecretName);
            var secret = GetSecretValue(response, pSecretKey);
            return secret;
        }


        private string GetSecretValue(string pSecretResponse, string pSecretKey)
        {
            var secret = string.Empty;

            if (string.IsNullOrEmpty(pSecretKey))
            {
                secret = pSecretResponse;
            }
            else
            {
                var secretJson = JsonDocument.Parse(pSecretResponse).RootElement;
                secret = secretJson.GetProperty(pSecretKey).GetString();
            }

            return secret;
        }

        public void Dispose()
        {
            mySecretsManager?.Dispose();
            mySecretsManagerCache?.Dispose();
        }

    }
}
