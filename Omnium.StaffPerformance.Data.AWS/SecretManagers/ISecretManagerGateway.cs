﻿using System;
using System.Threading.Tasks;

namespace Omnium.StaffPerformance.Data.AWS.SecretManagers
{
    public interface ISecretManagerGateway : IDisposable
    {
        /// <summary>
        /// Gets the secret manager content. When the <paramref name="pSecretkey"/> isn't null or empty returns only the value for that key, otherwise
        /// returns all the values in the secrets manager
        /// </summary>
        /// <param name="pSecretKey">secret key which you want to retrieve</param>
        /// <returns>Returns the secret</returns>
        Task<string> GetSecretAsync(string pSecretKey = null);
        /// <summary>
        /// Returns the secret manager content
        /// </summary>
        /// <returns>Returns the secret</returns>
        Task<T> GetSecretAsync<T>();
    }
}
