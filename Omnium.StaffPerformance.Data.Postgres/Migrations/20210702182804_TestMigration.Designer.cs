﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;
using Omnium.StaffPerformance.Data.Postgres.Context;

namespace Omnium.StaffPerformance.Data.Postgres.Migrations
{
    [DbContext(typeof(PostgresContext))]
    [Migration("20210702182804_TestMigration")]
    partial class TestMigration
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("Relational:MaxIdentifierLength", 63)
                .HasAnnotation("ProductVersion", "5.0.7")
                .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

            modelBuilder.Entity("Omnium.StaffPerformance.Business.Domain.Entities.Attendance", b =>
                {
                    b.Property<string>("Id")
                        .HasColumnType("text")
                        .HasColumnName("ID");

                    b.Property<string>("CompanyId")
                        .HasColumnType("text")
                        .HasColumnName("COMPANY_ID");

                    b.Property<DateTime>("CreateDate")
                        .HasColumnType("timestamp without time zone")
                        .HasColumnName("CREATE_DATE");

                    b.Property<DateTime>("Date")
                        .HasColumnType("timestamp without time zone")
                        .HasColumnName("DATE");

                    b.Property<string>("EmployeeId")
                        .HasColumnType("text")
                        .HasColumnName("EMPLOYEE_ID");

                    b.Property<string>("Status")
                        .HasColumnType("text")
                        .HasColumnName("STATUS");

                    b.Property<int>("Type")
                        .HasColumnType("integer")
                        .HasColumnName("TYPE");

                    b.Property<DateTime>("UpdateDate")
                        .HasColumnType("timestamp without time zone")
                        .HasColumnName("UPDATE_DATE");

                    b.HasKey("Id");

                    b.HasIndex("CompanyId");

                    b.ToTable("MSPF_ATTENDANCES");
                });

            modelBuilder.Entity("Omnium.StaffPerformance.Business.Domain.Entities.Calendar", b =>
                {
                    b.Property<string>("Id")
                        .HasColumnType("text")
                        .HasColumnName("ID");

                    b.Property<DateTime>("CreateDate")
                        .HasColumnType("timestamp without time zone")
                        .HasColumnName("CREATE_DATE");

                    b.Property<DateTime>("Date")
                        .HasColumnType("timestamp without time zone")
                        .HasColumnName("DATE");

                    b.Property<int>("Month")
                        .HasColumnType("integer")
                        .HasColumnName("MONTH");

                    b.Property<string>("MonthStr")
                        .HasColumnType("text")
                        .HasColumnName("MONTH_STR");

                    b.Property<string>("Status")
                        .HasColumnType("text")
                        .HasColumnName("STATUS");

                    b.Property<DateTime>("UpdateDate")
                        .HasColumnType("timestamp without time zone")
                        .HasColumnName("UPDATE_DATE");

                    b.Property<int>("Week")
                        .HasColumnType("integer")
                        .HasColumnName("WEEK");

                    b.Property<string>("WeekDayStr")
                        .HasColumnType("text")
                        .HasColumnName("WEEK_DAY_STR");

                    b.Property<int>("Weekday")
                        .HasColumnType("integer")
                        .HasColumnName("WEEKDAY");

                    b.Property<int>("Year")
                        .HasColumnType("integer")
                        .HasColumnName("YEAR");

                    b.HasKey("Id");

                    b.ToTable("REF_CALENDARS");
                });

            modelBuilder.Entity("Omnium.StaffPerformance.Business.Domain.Entities.Company", b =>
                {
                    b.Property<string>("Id")
                        .HasColumnType("text")
                        .HasColumnName("ID");

                    b.Property<string>("City")
                        .HasColumnType("text")
                        .HasColumnName("CITY");

                    b.Property<string>("CompanyCode")
                        .HasColumnType("text")
                        .HasColumnName("COMPANY_CD");

                    b.Property<string>("CountryId")
                        .HasColumnType("text")
                        .HasColumnName("COUNTRY_ID");

                    b.Property<DateTime>("CreateDate")
                        .HasColumnType("timestamp without time zone")
                        .HasColumnName("CREATE_DATE");

                    b.Property<string>("Name")
                        .HasColumnType("text")
                        .HasColumnName("NAME");

                    b.Property<string>("Status")
                        .HasColumnType("text")
                        .HasColumnName("STATUS");

                    b.Property<string>("Street")
                        .HasColumnType("text")
                        .HasColumnName("STREET");

                    b.Property<DateTime>("UpdateDate")
                        .HasColumnType("timestamp without time zone")
                        .HasColumnName("UPDATE_DATE");

                    b.Property<string>("ZipCode")
                        .HasColumnType("text")
                        .HasColumnName("ZIP_CD");

                    b.HasKey("Id");

                    b.ToTable("REF_COMPANIES");
                });

            modelBuilder.Entity("Omnium.StaffPerformance.Business.Domain.Entities.CompanyPlatform", b =>
                {
                    b.Property<string>("CompanyId")
                        .HasColumnType("text")
                        .HasColumnName("COMPANY_ID");

                    b.Property<string>("PlatformId")
                        .HasColumnType("text")
                        .HasColumnName("PLATFORM_ID");

                    b.Property<string>("Status")
                        .HasColumnType("text")
                        .HasColumnName("STATUS");

                    b.HasKey("CompanyId", "PlatformId");

                    b.HasIndex("PlatformId");

                    b.ToTable("CompanyPlatform");
                });

            modelBuilder.Entity("Omnium.StaffPerformance.Business.Domain.Entities.Country", b =>
                {
                    b.Property<string>("Id")
                        .HasColumnType("text")
                        .HasColumnName("ID");

                    b.Property<DateTime>("CreateDate")
                        .HasColumnType("timestamp without time zone")
                        .HasColumnName("CREATE_DATE");

                    b.Property<string>("ISO2")
                        .HasColumnType("text")
                        .HasColumnName("ISO2");

                    b.Property<string>("ISO3")
                        .HasColumnType("text")
                        .HasColumnName("ISO3");

                    b.Property<string>("Name")
                        .HasColumnType("text")
                        .HasColumnName("NAME");

                    b.Property<string>("Status")
                        .HasColumnType("text")
                        .HasColumnName("STATUS");

                    b.Property<DateTime>("UpdateDate")
                        .HasColumnType("timestamp without time zone")
                        .HasColumnName("UPDATE_DATE");

                    b.HasKey("Id");

                    b.ToTable("REF_COUNTRIES");
                });

            modelBuilder.Entity("Omnium.StaffPerformance.Business.Domain.Entities.Employee", b =>
                {
                    b.Property<string>("Id")
                        .HasColumnType("text")
                        .HasColumnName("ID");

                    b.Property<DateTime>("BirthDate")
                        .HasColumnType("timestamp without time zone")
                        .HasColumnName("BIRTH_DATE");

                    b.Property<string>("CompanyId")
                        .HasColumnType("text")
                        .HasColumnName("COMPANY_ID");

                    b.Property<DateTime>("CreateDate")
                        .HasColumnType("timestamp without time zone")
                        .HasColumnName("CREATE_DATE");

                    b.Property<string>("Email")
                        .HasColumnType("text")
                        .HasColumnName("EMAIL");

                    b.Property<string>("Gender")
                        .HasColumnType("text")
                        .HasColumnName("GENDER");

                    b.Property<string>("MobilePhoneNumber")
                        .HasColumnType("text")
                        .HasColumnName("MOBILE_PHONE_NUMBER");

                    b.Property<string>("NameFull")
                        .HasColumnType("text")
                        .HasColumnName("NAME_FULL");

                    b.Property<string>("NameShort")
                        .HasColumnType("text")
                        .HasColumnName("NAME_SHORT");

                    b.Property<string>("PhoneNumber")
                        .HasColumnType("text")
                        .HasColumnName("PHONE_NUMBER");

                    b.Property<string>("Status")
                        .HasColumnType("text")
                        .HasColumnName("STATUS");

                    b.Property<DateTime>("UpdateDate")
                        .HasColumnType("timestamp without time zone")
                        .HasColumnName("UPDATE_DATE");

                    b.HasKey("Id");

                    b.HasIndex("CompanyId");

                    b.ToTable("REF_EMPLOYEES");
                });

            modelBuilder.Entity("Omnium.StaffPerformance.Business.Domain.Entities.Holiday", b =>
                {
                    b.Property<string>("Id")
                        .HasColumnType("text")
                        .HasColumnName("ID");

                    b.Property<string>("CountryId")
                        .HasColumnType("text")
                        .HasColumnName("COUNTRY_ID");

                    b.Property<DateTime>("CreateDate")
                        .HasColumnType("timestamp without time zone")
                        .HasColumnName("CREATE_DATE");

                    b.Property<DateTime>("Date")
                        .HasColumnType("timestamp without time zone")
                        .HasColumnName("DATE");

                    b.Property<string>("Name")
                        .HasColumnType("text")
                        .HasColumnName("NAME");

                    b.Property<string>("Status")
                        .HasColumnType("text")
                        .HasColumnName("STATUS");

                    b.Property<DateTime>("UpdateDate")
                        .HasColumnType("timestamp without time zone")
                        .HasColumnName("UPDATE_DATE");

                    b.HasKey("Id");

                    b.ToTable("REF_HOLIDAYS");
                });

            modelBuilder.Entity("Omnium.StaffPerformance.Business.Domain.Entities.PeriodType", b =>
                {
                    b.Property<string>("Id")
                        .HasColumnType("text")
                        .HasColumnName("ID");

                    b.Property<string>("CompanyId")
                        .HasColumnType("text")
                        .HasColumnName("COMPANY_ID");

                    b.Property<DateTime>("CreateDate")
                        .HasColumnType("timestamp without time zone")
                        .HasColumnName("CREATE_DATE");

                    b.Property<string>("Name")
                        .HasColumnType("text")
                        .HasColumnName("NAME");

                    b.Property<string>("Status")
                        .HasColumnType("text")
                        .HasColumnName("STATUS");

                    b.Property<DateTime>("UpdateDate")
                        .HasColumnType("timestamp without time zone")
                        .HasColumnName("UPDATE_DATE");

                    b.Property<string>("Value")
                        .HasColumnType("text")
                        .HasColumnName("VAL");

                    b.HasKey("Id");

                    b.HasIndex("CompanyId");

                    b.ToTable("MSPF_PERIOD_TYPES");
                });

            modelBuilder.Entity("Omnium.StaffPerformance.Business.Domain.Entities.Platform", b =>
                {
                    b.Property<string>("Id")
                        .HasColumnType("text")
                        .HasColumnName("ID");

                    b.Property<DateTime>("CreateDate")
                        .HasColumnType("timestamp without time zone")
                        .HasColumnName("CREATE_DATE");

                    b.Property<string>("IntegrationData")
                        .HasColumnType("jsonb")
                        .HasColumnName("INTEGRATION_DATA");

                    b.Property<string>("Name")
                        .HasColumnType("text")
                        .HasColumnName("NAME");

                    b.Property<int>("PlatformType")
                        .HasColumnType("integer")
                        .HasColumnName("PLATFORM_TYPE");

                    b.Property<string>("Status")
                        .HasColumnType("text")
                        .HasColumnName("STATUS");

                    b.Property<DateTime>("UpdateDate")
                        .HasColumnType("timestamp without time zone")
                        .HasColumnName("UPDATE_DATE");

                    b.HasKey("Id");

                    b.ToTable("REF_PLATFORM");
                });

            modelBuilder.Entity("Omnium.StaffPerformance.Business.Domain.Entities.Schedule", b =>
                {
                    b.Property<string>("Id")
                        .HasColumnType("text")
                        .HasColumnName("ID");

                    b.Property<DateTime>("Begin")
                        .HasColumnType("timestamp without time zone")
                        .HasColumnName("BEGIN");

                    b.Property<string>("CalendarId")
                        .HasColumnType("text")
                        .HasColumnName("CALENDAR_ID");

                    b.Property<string>("CompanyId")
                        .HasColumnType("text")
                        .HasColumnName("COMPANY_ID");

                    b.Property<DateTime>("CreateDate")
                        .HasColumnType("timestamp without time zone")
                        .HasColumnName("CREATE_DATE");

                    b.Property<string>("EmployeeId")
                        .HasColumnType("text")
                        .HasColumnName("EMPLOYEE_ID");

                    b.Property<DateTime>("End")
                        .HasColumnType("timestamp without time zone")
                        .HasColumnName("END");

                    b.Property<string>("PeriodTypeId")
                        .HasColumnType("text")
                        .HasColumnName("PERIOD_TYPE_ID");

                    b.Property<string>("Status")
                        .HasColumnType("text")
                        .HasColumnName("STATUS");

                    b.Property<string>("StoreId")
                        .HasColumnType("text")
                        .HasColumnName("STORE_ID");

                    b.Property<DateTime>("UpdateDate")
                        .HasColumnType("timestamp without time zone")
                        .HasColumnName("UPDATE_DATE");

                    b.HasKey("Id");

                    b.HasIndex("CalendarId");

                    b.HasIndex("CompanyId");

                    b.HasIndex("PeriodTypeId");

                    b.HasIndex("StoreId");

                    b.ToTable("MSPF_SCHEDULES");
                });

            modelBuilder.Entity("Omnium.StaffPerformance.Business.Domain.Entities.Store", b =>
                {
                    b.Property<string>("Id")
                        .HasColumnType("text")
                        .HasColumnName("ID");

                    b.Property<DateTime>("CreateDate")
                        .HasColumnType("timestamp without time zone")
                        .HasColumnName("CREATE_DATE");

                    b.Property<string>("Status")
                        .HasColumnType("text")
                        .HasColumnName("STATUS");

                    b.Property<DateTime>("UpdateDate")
                        .HasColumnType("timestamp without time zone")
                        .HasColumnName("UPDATE_DATE");

                    b.HasKey("Id");

                    b.ToTable("REF_STORES");
                });

            modelBuilder.Entity("Omnium.StaffPerformance.Business.Domain.Entities.StoreHoliday", b =>
                {
                    b.Property<string>("HolidayId")
                        .HasColumnType("text")
                        .HasColumnName("HOLIDAY_ID");

                    b.Property<string>("StoreId")
                        .HasColumnType("text")
                        .HasColumnName("STORE_ID");

                    b.HasKey("HolidayId", "StoreId");

                    b.HasIndex("StoreId");

                    b.ToTable("StoreHoliday");
                });

            modelBuilder.Entity("Omnium.StaffPerformance.Business.Domain.Entities.Attendance", b =>
                {
                    b.HasOne("Omnium.StaffPerformance.Business.Domain.Entities.Company", "Company")
                        .WithMany()
                        .HasForeignKey("CompanyId");

                    b.Navigation("Company");
                });

            modelBuilder.Entity("Omnium.StaffPerformance.Business.Domain.Entities.CompanyPlatform", b =>
                {
                    b.HasOne("Omnium.StaffPerformance.Business.Domain.Entities.Company", "Company")
                        .WithMany("CompanyPlatforms")
                        .HasForeignKey("CompanyId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("Omnium.StaffPerformance.Business.Domain.Entities.Platform", "Platform")
                        .WithMany("CompanyPlatforms")
                        .HasForeignKey("PlatformId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Company");

                    b.Navigation("Platform");
                });

            modelBuilder.Entity("Omnium.StaffPerformance.Business.Domain.Entities.Employee", b =>
                {
                    b.HasOne("Omnium.StaffPerformance.Business.Domain.Entities.Company", "Company")
                        .WithMany()
                        .HasForeignKey("CompanyId");

                    b.Navigation("Company");
                });

            modelBuilder.Entity("Omnium.StaffPerformance.Business.Domain.Entities.PeriodType", b =>
                {
                    b.HasOne("Omnium.StaffPerformance.Business.Domain.Entities.Company", "Company")
                        .WithMany()
                        .HasForeignKey("CompanyId");

                    b.Navigation("Company");
                });

            modelBuilder.Entity("Omnium.StaffPerformance.Business.Domain.Entities.Schedule", b =>
                {
                    b.HasOne("Omnium.StaffPerformance.Business.Domain.Entities.Calendar", "Calendar")
                        .WithMany()
                        .HasForeignKey("CalendarId");

                    b.HasOne("Omnium.StaffPerformance.Business.Domain.Entities.Company", "Company")
                        .WithMany()
                        .HasForeignKey("CompanyId");

                    b.HasOne("Omnium.StaffPerformance.Business.Domain.Entities.PeriodType", "PeriodType")
                        .WithMany()
                        .HasForeignKey("PeriodTypeId");

                    b.HasOne("Omnium.StaffPerformance.Business.Domain.Entities.Store", "Store")
                        .WithMany()
                        .HasForeignKey("StoreId");

                    b.Navigation("Calendar");

                    b.Navigation("Company");

                    b.Navigation("PeriodType");

                    b.Navigation("Store");
                });

            modelBuilder.Entity("Omnium.StaffPerformance.Business.Domain.Entities.StoreHoliday", b =>
                {
                    b.HasOne("Omnium.StaffPerformance.Business.Domain.Entities.Holiday", "Holiday")
                        .WithMany("StoreHolidays")
                        .HasForeignKey("HolidayId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("Omnium.StaffPerformance.Business.Domain.Entities.Store", "Store")
                        .WithMany("StoreHolidays")
                        .HasForeignKey("StoreId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Holiday");

                    b.Navigation("Store");
                });

            modelBuilder.Entity("Omnium.StaffPerformance.Business.Domain.Entities.Company", b =>
                {
                    b.Navigation("CompanyPlatforms");
                });

            modelBuilder.Entity("Omnium.StaffPerformance.Business.Domain.Entities.Holiday", b =>
                {
                    b.Navigation("StoreHolidays");
                });

            modelBuilder.Entity("Omnium.StaffPerformance.Business.Domain.Entities.Platform", b =>
                {
                    b.Navigation("CompanyPlatforms");
                });

            modelBuilder.Entity("Omnium.StaffPerformance.Business.Domain.Entities.Store", b =>
                {
                    b.Navigation("StoreHolidays");
                });
#pragma warning restore 612, 618
        }
    }
}
