﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Omnium.StaffPerformance.Data.Postgres.Migrations
{
    public partial class Correction : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "SCHEDULE_INTERVAL_LINE",
                columns: table => new
                {
                    ID = table.Column<string>(type: "text", nullable: false),
                    SCHEDULE_INTERVAL_ID = table.Column<string>(type: "text", nullable: true),
                    BEGIN = table.Column<int>(type: "integer", nullable: false),
                    END = table.Column<int>(type: "integer", nullable: false),
                    ORDER = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SCHEDULE_INTERVAL_LINE", x => x.ID);
                    table.ForeignKey(
                        name: "FK_SCHEDULE_INTERVAL_LINE_MSPF_SCHEDULE_INTERVALS_SCHEDULE_INT~",
                        column: x => x.SCHEDULE_INTERVAL_ID,
                        principalTable: "MSPF_SCHEDULE_INTERVALS",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_SCHEDULE_INTERVAL_LINE_SCHEDULE_INTERVAL_ID",
                table: "SCHEDULE_INTERVAL_LINE",
                column: "SCHEDULE_INTERVAL_ID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SCHEDULE_INTERVAL_LINE");
        }
    }
}
