﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Omnium.StaffPerformance.Data.Postgres.Migrations
{
    public partial class TestMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_MSPF_SCHEDULES_CALENDAR_ID",
                table: "MSPF_SCHEDULES",
                column: "CALENDAR_ID");

            migrationBuilder.CreateIndex(
                name: "IX_MSPF_SCHEDULES_PERIOD_TYPE_ID",
                table: "MSPF_SCHEDULES",
                column: "PERIOD_TYPE_ID");

            migrationBuilder.AddForeignKey(
                name: "FK_MSPF_SCHEDULES_MSPF_PERIOD_TYPES_PERIOD_TYPE_ID",
                table: "MSPF_SCHEDULES",
                column: "PERIOD_TYPE_ID",
                principalTable: "MSPF_PERIOD_TYPES",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_MSPF_SCHEDULES_REF_CALENDARS_CALENDAR_ID",
                table: "MSPF_SCHEDULES",
                column: "CALENDAR_ID",
                principalTable: "REF_CALENDARS",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MSPF_SCHEDULES_MSPF_PERIOD_TYPES_PERIOD_TYPE_ID",
                table: "MSPF_SCHEDULES");

            migrationBuilder.DropForeignKey(
                name: "FK_MSPF_SCHEDULES_REF_CALENDARS_CALENDAR_ID",
                table: "MSPF_SCHEDULES");

            migrationBuilder.DropIndex(
                name: "IX_MSPF_SCHEDULES_CALENDAR_ID",
                table: "MSPF_SCHEDULES");

            migrationBuilder.DropIndex(
                name: "IX_MSPF_SCHEDULES_PERIOD_TYPE_ID",
                table: "MSPF_SCHEDULES");
        }
    }
}
