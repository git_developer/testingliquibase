﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Omnium.StaffPerformance.Data.Postgres.Migrations
{
    public partial class ScheduleIntervals : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "MSPF_SCHEDULE_INTERVALS",
                columns: table => new
                {
                    ID = table.Column<string>(type: "text", nullable: false),
                    NAME = table.Column<string>(type: "text", nullable: true),
                    INTERVAL = table.Column<int>(type: "integer", nullable: false),
                    BEGIN = table.Column<int>(type: "integer", nullable: false),
                    END = table.Column<int>(type: "integer", nullable: false),
                    CREATE_DATE = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UPDATE_DATE = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    STATUS = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MSPF_SCHEDULE_INTERVALS", x => x.ID);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MSPF_SCHEDULE_INTERVALS");
        }
    }
}
