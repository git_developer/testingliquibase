﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Omnium.StaffPerformance.Data.Postgres.Migrations
{
    public partial class Correction2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SCHEDULE_INTERVAL_LINE_MSPF_SCHEDULE_INTERVALS_SCHEDULE_INT~",
                table: "SCHEDULE_INTERVAL_LINE");

            migrationBuilder.DropPrimaryKey(
                name: "PK_SCHEDULE_INTERVAL_LINE",
                table: "SCHEDULE_INTERVAL_LINE");

            migrationBuilder.DropIndex(
                name: "IX_SCHEDULE_INTERVAL_LINE_SCHEDULE_INTERVAL_ID",
                table: "SCHEDULE_INTERVAL_LINE");

            migrationBuilder.DropColumn(
                name: "ID",
                table: "SCHEDULE_INTERVAL_LINE");

            migrationBuilder.RenameTable(
                name: "SCHEDULE_INTERVAL_LINE",
                newName: "ScheduleIntervalLine");

            migrationBuilder.AlterColumn<string>(
                name: "SCHEDULE_INTERVAL_ID",
                table: "ScheduleIntervalLine",
                type: "text",
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "text",
                oldNullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_ScheduleIntervalLine",
                table: "ScheduleIntervalLine",
                columns: new[] { "SCHEDULE_INTERVAL_ID", "ORDER" });

            migrationBuilder.AddForeignKey(
                name: "FK_ScheduleIntervalLine_MSPF_SCHEDULE_INTERVALS_SCHEDULE_INTER~",
                table: "ScheduleIntervalLine",
                column: "SCHEDULE_INTERVAL_ID",
                principalTable: "MSPF_SCHEDULE_INTERVALS",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ScheduleIntervalLine_MSPF_SCHEDULE_INTERVALS_SCHEDULE_INTER~",
                table: "ScheduleIntervalLine");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ScheduleIntervalLine",
                table: "ScheduleIntervalLine");

            migrationBuilder.RenameTable(
                name: "ScheduleIntervalLine",
                newName: "SCHEDULE_INTERVAL_LINE");

            migrationBuilder.AlterColumn<string>(
                name: "SCHEDULE_INTERVAL_ID",
                table: "SCHEDULE_INTERVAL_LINE",
                type: "text",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "text");

            migrationBuilder.AddColumn<string>(
                name: "ID",
                table: "SCHEDULE_INTERVAL_LINE",
                type: "text",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddPrimaryKey(
                name: "PK_SCHEDULE_INTERVAL_LINE",
                table: "SCHEDULE_INTERVAL_LINE",
                column: "ID");

            migrationBuilder.CreateIndex(
                name: "IX_SCHEDULE_INTERVAL_LINE_SCHEDULE_INTERVAL_ID",
                table: "SCHEDULE_INTERVAL_LINE",
                column: "SCHEDULE_INTERVAL_ID");

            migrationBuilder.AddForeignKey(
                name: "FK_SCHEDULE_INTERVAL_LINE_MSPF_SCHEDULE_INTERVALS_SCHEDULE_INT~",
                table: "SCHEDULE_INTERVAL_LINE",
                column: "SCHEDULE_INTERVAL_ID",
                principalTable: "MSPF_SCHEDULE_INTERVALS",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
