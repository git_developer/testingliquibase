﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Omnium.StaffPerformance.Data.Postgres.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "REF_CALENDARS",
                columns: table => new
                {
                    ID = table.Column<string>(type: "text", nullable: false),
                    DATE = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    WEEKDAY = table.Column<int>(type: "integer", nullable: false),
                    WEEK_DAY_STR = table.Column<string>(type: "text", nullable: true),
                    WEEK = table.Column<int>(type: "integer", nullable: false),
                    MONTH = table.Column<int>(type: "integer", nullable: false),
                    MONTH_STR = table.Column<string>(type: "text", nullable: true),
                    YEAR = table.Column<int>(type: "integer", nullable: false),
                    STATUS = table.Column<string>(type: "text", nullable: true),
                    CREATE_DATE = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UPDATE_DATE = table.Column<DateTime>(type: "timestamp without time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_REF_CALENDARS", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "REF_COMPANIES",
                columns: table => new
                {
                    ID = table.Column<string>(type: "text", nullable: false),
                    COMPANY_CD = table.Column<string>(type: "text", nullable: true),
                    NAME = table.Column<string>(type: "text", nullable: true),
                    STREET = table.Column<string>(type: "text", nullable: true),
                    CITY = table.Column<string>(type: "text", nullable: true),
                    ZIP_CD = table.Column<string>(type: "text", nullable: true),
                    COUNTRY_ID = table.Column<string>(type: "text", nullable: true),
                    CREATE_DATE = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UPDATE_DATE = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    STATUS = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_REF_COMPANIES", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "REF_COUNTRIES",
                columns: table => new
                {
                    ID = table.Column<string>(type: "text", nullable: false),
                    NAME = table.Column<string>(type: "text", nullable: true),
                    ISO2 = table.Column<string>(type: "text", nullable: true),
                    ISO3 = table.Column<string>(type: "text", nullable: true),
                    STATUS = table.Column<string>(type: "text", nullable: true),
                    CREATE_DATE = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UPDATE_DATE = table.Column<DateTime>(type: "timestamp without time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_REF_COUNTRIES", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "REF_HOLIDAYS",
                columns: table => new
                {
                    ID = table.Column<string>(type: "text", nullable: false),
                    DATE = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    NAME = table.Column<string>(type: "text", nullable: true),
                    COUNTRY_ID = table.Column<string>(type: "text", nullable: true),
                    CREATE_DATE = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UPDATE_DATE = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    STATUS = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_REF_HOLIDAYS", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "REF_PLATFORM",
                columns: table => new
                {
                    ID = table.Column<string>(type: "text", nullable: false),
                    NAME = table.Column<string>(type: "text", nullable: true),
                    PLATFORM_TYPE = table.Column<int>(type: "integer", nullable: false),
                    INTEGRATION_DATA = table.Column<string>(type: "jsonb", nullable: true),
                    CREATE_DATE = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UPDATE_DATE = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    STATUS = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_REF_PLATFORM", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "REF_STORES",
                columns: table => new
                {
                    ID = table.Column<string>(type: "text", nullable: false),
                    STATUS = table.Column<string>(type: "text", nullable: true),
                    CREATE_DATE = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UPDATE_DATE = table.Column<DateTime>(type: "timestamp without time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_REF_STORES", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "MSPF_ATTENDANCES",
                columns: table => new
                {
                    ID = table.Column<string>(type: "text", nullable: false),
                    EMPLOYEE_ID = table.Column<string>(type: "text", nullable: true),
                    DATE = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    TYPE = table.Column<int>(type: "integer", nullable: false),
                    COMPANY_ID = table.Column<string>(type: "text", nullable: true),
                    STATUS = table.Column<string>(type: "text", nullable: true),
                    CREATE_DATE = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UPDATE_DATE = table.Column<DateTime>(type: "timestamp without time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MSPF_ATTENDANCES", x => x.ID);
                    table.ForeignKey(
                        name: "FK_MSPF_ATTENDANCES_REF_COMPANIES_COMPANY_ID",
                        column: x => x.COMPANY_ID,
                        principalTable: "REF_COMPANIES",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "MSPF_PERIOD_TYPES",
                columns: table => new
                {
                    ID = table.Column<string>(type: "text", nullable: false),
                    NAME = table.Column<string>(type: "text", nullable: true),
                    VAL = table.Column<string>(type: "text", nullable: true),
                    COMPANY_ID = table.Column<string>(type: "text", nullable: true),
                    STATUS = table.Column<string>(type: "text", nullable: true),
                    CREATE_DATE = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UPDATE_DATE = table.Column<DateTime>(type: "timestamp without time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MSPF_PERIOD_TYPES", x => x.ID);
                    table.ForeignKey(
                        name: "FK_MSPF_PERIOD_TYPES_REF_COMPANIES_COMPANY_ID",
                        column: x => x.COMPANY_ID,
                        principalTable: "REF_COMPANIES",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "REF_EMPLOYEES",
                columns: table => new
                {
                    ID = table.Column<string>(type: "text", nullable: false),
                    COMPANY_ID = table.Column<string>(type: "text", nullable: true),
                    NAME_SHORT = table.Column<string>(type: "text", nullable: true),
                    NAME_FULL = table.Column<string>(type: "text", nullable: true),
                    GENDER = table.Column<string>(type: "text", nullable: true),
                    EMAIL = table.Column<string>(type: "text", nullable: true),
                    MOBILE_PHONE_NUMBER = table.Column<string>(type: "text", nullable: true),
                    PHONE_NUMBER = table.Column<string>(type: "text", nullable: true),
                    BIRTH_DATE = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    CREATE_DATE = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UPDATE_DATE = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    STATUS = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_REF_EMPLOYEES", x => x.ID);
                    table.ForeignKey(
                        name: "FK_REF_EMPLOYEES_REF_COMPANIES_COMPANY_ID",
                        column: x => x.COMPANY_ID,
                        principalTable: "REF_COMPANIES",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CompanyPlatform",
                columns: table => new
                {
                    COMPANY_ID = table.Column<string>(type: "text", nullable: false),
                    PLATFORM_ID = table.Column<string>(type: "text", nullable: false),
                    STATUS = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CompanyPlatform", x => new { x.COMPANY_ID, x.PLATFORM_ID });
                    table.ForeignKey(
                        name: "FK_CompanyPlatform_REF_COMPANIES_COMPANY_ID",
                        column: x => x.COMPANY_ID,
                        principalTable: "REF_COMPANIES",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CompanyPlatform_REF_PLATFORM_PLATFORM_ID",
                        column: x => x.PLATFORM_ID,
                        principalTable: "REF_PLATFORM",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "MSPF_SCHEDULES",
                columns: table => new
                {
                    ID = table.Column<string>(type: "text", nullable: false),
                    EMPLOYEE_ID = table.Column<string>(type: "text", nullable: true),
                    STORE_ID = table.Column<string>(type: "text", nullable: true),
                    CALENDAR_ID = table.Column<string>(type: "text", nullable: true),
                    COMPANY_ID = table.Column<string>(type: "text", nullable: true),
                    PERIOD_TYPE_ID = table.Column<string>(type: "text", nullable: true),
                    BEGIN = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    END = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    STATUS = table.Column<string>(type: "text", nullable: true),
                    CREATE_DATE = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UPDATE_DATE = table.Column<DateTime>(type: "timestamp without time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MSPF_SCHEDULES", x => x.ID);
                    table.ForeignKey(
                        name: "FK_MSPF_SCHEDULES_REF_COMPANIES_COMPANY_ID",
                        column: x => x.COMPANY_ID,
                        principalTable: "REF_COMPANIES",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_MSPF_SCHEDULES_REF_STORES_STORE_ID",
                        column: x => x.STORE_ID,
                        principalTable: "REF_STORES",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "StoreHoliday",
                columns: table => new
                {
                    STORE_ID = table.Column<string>(type: "text", nullable: false),
                    HOLIDAY_ID = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StoreHoliday", x => new { x.HOLIDAY_ID, x.STORE_ID });
                    table.ForeignKey(
                        name: "FK_StoreHoliday_REF_HOLIDAYS_HOLIDAY_ID",
                        column: x => x.HOLIDAY_ID,
                        principalTable: "REF_HOLIDAYS",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_StoreHoliday_REF_STORES_STORE_ID",
                        column: x => x.STORE_ID,
                        principalTable: "REF_STORES",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CompanyPlatform_PLATFORM_ID",
                table: "CompanyPlatform",
                column: "PLATFORM_ID");

            migrationBuilder.CreateIndex(
                name: "IX_MSPF_ATTENDANCES_COMPANY_ID",
                table: "MSPF_ATTENDANCES",
                column: "COMPANY_ID");

            migrationBuilder.CreateIndex(
                name: "IX_MSPF_PERIOD_TYPES_COMPANY_ID",
                table: "MSPF_PERIOD_TYPES",
                column: "COMPANY_ID");

            migrationBuilder.CreateIndex(
                name: "IX_MSPF_SCHEDULES_COMPANY_ID",
                table: "MSPF_SCHEDULES",
                column: "COMPANY_ID");

            migrationBuilder.CreateIndex(
                name: "IX_MSPF_SCHEDULES_STORE_ID",
                table: "MSPF_SCHEDULES",
                column: "STORE_ID");

            migrationBuilder.CreateIndex(
                name: "IX_REF_EMPLOYEES_COMPANY_ID",
                table: "REF_EMPLOYEES",
                column: "COMPANY_ID");

            migrationBuilder.CreateIndex(
                name: "IX_StoreHoliday_STORE_ID",
                table: "StoreHoliday",
                column: "STORE_ID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CompanyPlatform");

            migrationBuilder.DropTable(
                name: "MSPF_ATTENDANCES");

            migrationBuilder.DropTable(
                name: "MSPF_PERIOD_TYPES");

            migrationBuilder.DropTable(
                name: "MSPF_SCHEDULES");

            migrationBuilder.DropTable(
                name: "REF_CALENDARS");

            migrationBuilder.DropTable(
                name: "REF_COUNTRIES");

            migrationBuilder.DropTable(
                name: "REF_EMPLOYEES");

            migrationBuilder.DropTable(
                name: "StoreHoliday");

            migrationBuilder.DropTable(
                name: "REF_PLATFORM");

            migrationBuilder.DropTable(
                name: "REF_COMPANIES");

            migrationBuilder.DropTable(
                name: "REF_HOLIDAYS");

            migrationBuilder.DropTable(
                name: "REF_STORES");
        }
    }
}
