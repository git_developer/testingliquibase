﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Omnium.StaffPerformance.Business.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Omnium.StaffPerformance.Data.Postgres.Mapping
{
    public class PlatformEntityTypeConfiguration : IEntityTypeConfiguration<Platform>
    {
        /// <summary>
        /// Configures the connection from entity to column
        /// </summary>
        /// <param name="pBuilder">Database builder</param>
        public void Configure(EntityTypeBuilder<Platform> pBuilder)
        {
            pBuilder.Property(b => b.IntegrationData).HasColumnType("jsonb");
        }
    }
}
