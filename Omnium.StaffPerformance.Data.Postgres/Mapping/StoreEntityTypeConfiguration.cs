﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Omnium.StaffPerformance.Business.Domain.Entities;

namespace Omnium.StaffPerformance.Data.Postgres.Mapping
{
    public class StoreEntityTypeConfiguration : IEntityTypeConfiguration<Store>
    {

        /// <summary>
        /// Configures the connection from entity to column
        /// </summary>
        /// <param name="pBuilder">Database builder</param>
        public void Configure(EntityTypeBuilder<Store> pBuilder)
        {
            pBuilder.HasMany(h => h.StoreHolidays).WithOne(s => s.Store);
        }
    }
}
