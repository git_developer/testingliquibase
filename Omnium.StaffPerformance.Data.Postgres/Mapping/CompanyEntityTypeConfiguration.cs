﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Omnium.StaffPerformance.Business.Domain.Entities;


namespace Omnium.StaffPerformance.Data.Postgres.Mapping
{
    public class CompanyEntityTypeConfiguration : IEntityTypeConfiguration<Company>
    {
        /// <summary>
        /// Configures the connection from entity to column
        /// </summary>
        /// <param name="pBuilder">Database builder</param>
        public void Configure(EntityTypeBuilder<Company> pBuilder)
        {
            pBuilder.HasMany(p => p.CompanyPlatforms).WithOne(c => c.Company);
        }
    }
}
