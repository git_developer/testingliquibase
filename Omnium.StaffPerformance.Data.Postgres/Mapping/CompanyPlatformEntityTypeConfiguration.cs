﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Omnium.StaffPerformance.Business.Domain.Entities;


namespace Omnium.StaffPerformance.Data.Postgres.Mapping
{
    public class CompanyPlatformEntityTypeConfiguration : IEntityTypeConfiguration<CompanyPlatform>
    {
        /// <summary>
        /// Configures the connection from entity to column
        /// </summary>
        /// <param name="pBuilder">Database builder</param>
        public void Configure(EntityTypeBuilder<CompanyPlatform> pBuilder)
        {
            pBuilder.HasKey(a => new { a.CompanyId, a.PlatformId });
        }
    }
}
