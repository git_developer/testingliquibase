﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Omnium.StaffPerformance.Business.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Omnium.StaffPerformance.Data.Postgres.Mapping
{
    public class ScheduleIntervalLineEntityConfiguration : IEntityTypeConfiguration<ScheduleIntervalLine>
    {

        /// <summary>
        /// Configures the connection from entity to column
        /// </summary>
        /// <param name="pBuilder">Database builder</param>
        public void Configure(EntityTypeBuilder<ScheduleIntervalLine> pBuilder)
        {
            pBuilder.HasKey(a => new { a.ScheduleIntervalId, a.Order });
        }
    }
}
