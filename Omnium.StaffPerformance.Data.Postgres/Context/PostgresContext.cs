﻿using Microsoft.EntityFrameworkCore;
using Omnium.StaffPerformance.Business.Domain.Entities;
using Omnium.StaffPerformance.Data.Postgres.Conventions;
using System.Linq;
using System;
using System.Reflection;
using Omnium.StaffPerformance.Data.Postgres.Mapping;

namespace Omnium.StaffPerformance.Data.Postgres.Context
{
    public class PostgresContext : DbContext
    {
        public PostgresContext(DbContextOptions<PostgresContext> options) : base(options) { }

        public virtual DbSet<Employee> Employees { get; private set; }
        public virtual DbSet<Attendance> Attendances { get; private set; }
        public virtual DbSet<Holiday> Holidays { get; private set; }
        public virtual DbSet<Country> Countries { get; private set; }
        public virtual DbSet<PeriodType> PeriodTypes { get; private set; }
        public virtual DbSet<Store> Stores { get; private set; }
        public virtual DbSet<Schedule> Schedules { get; private set; }
        public virtual DbSet<Calendar> Calendars { get; private set; }
        public virtual DbSet<ScheduleInterval> ScheduleIntervals{get; private set;}
        public virtual DbSet<Company> Companies { get; private set; }
        public virtual DbSet<Platform> Platform { get; private set; }

        /// <summary>
        /// Configures the entities to add to the database to its respective columns
        /// </summary>
        /// <param name="pModelBuilder">Database builder</param>
        protected override void OnModelCreating(ModelBuilder pModelBuilder)
        {
            pModelBuilder.SetOmniumConventions(new string[] { "Employee", "Holiday", "Country","Store","Calendar", "Company", "Platform" }, new string[] { "Attendance", "PeriodType", "Schedule", "ScheduleInterval" });

            pModelBuilder.ApplyConfiguration(new HolidayEntityTypeConfiguration());
            pModelBuilder.ApplyConfiguration(new StoreEntityTypeConfiguration());
            pModelBuilder.ApplyConfiguration(new StoreHolidayEntityTypeConfiguration());
            pModelBuilder.ApplyConfiguration(new ScheduleIntervalLineEntityConfiguration());

            pModelBuilder.ApplyConfiguration(new CompanyEntityTypeConfiguration());
            pModelBuilder.ApplyConfiguration(new PlatformEntityTypeConfiguration());
            pModelBuilder.ApplyConfiguration(new CompanyPlatformEntityTypeConfiguration());
        }
        /// <summary>
        /// Saves changes to the database
        /// </summary>
        /// <returns>integer signaling the success of the operation</returns>
        public override int SaveChanges()
        {
            BeforeSaveChanges();
            return base.SaveChanges();
        }
        /// <summary>
        /// Creates or updates the existing system dates
        /// </summary>
        private void BeforeSaveChanges()
        {
            var aggregateRoots = ChangeTracker.Entries().Where(v => v.Entity is IAggregateRoot);
            
            foreach (var ag in aggregateRoots)
            {
                var cast = (IAggregateRoot)ag.Entity;
                if (ag.State == EntityState.Added)
                {
                    var time = DateTime.Now;

                    Type typeInQuestion = ag.Entity.GetType();
                    PropertyInfo fieldCreate = typeInQuestion.GetProperty("CreateDate");
                    fieldCreate.SetValue(ag.Entity, time);


                    PropertyInfo fieldUpdate = typeInQuestion.GetProperty("UpdateDate");
                    fieldUpdate.SetValue(ag.Entity, time);
                }
                else if (ag.State == EntityState.Modified)
                {
                    Type typeInQuestion = ag.Entity.GetType();
                    PropertyInfo fieldCreate = typeInQuestion.GetProperty("UpdateDate");
                    fieldCreate.SetValue(ag.Entity, DateTime.Now);
                }
            }
        }
       
    }
}
