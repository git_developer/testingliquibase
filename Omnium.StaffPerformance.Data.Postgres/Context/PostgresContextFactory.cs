﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace Omnium.StaffPerformance.Data.Postgres.Context
{
   public class PostgresContextFactory: IDesignTimeDbContextFactory<PostgresContext>
    {
        /// <summary>
        /// Used for creating the Context in Design-Time
        /// </summary>
        /// <param name="args"></param>
        /// <returns>A database context</returns>
        public PostgresContext CreateDbContext(string [] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<PostgresContext>();
            optionsBuilder.UseNpgsql("Host=localhost;Port=5432;Database=postgres;Username=postgres;Password=Omnium2021!");

            return new PostgresContext(optionsBuilder.Options);
        }
    }
}
