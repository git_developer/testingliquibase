﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Omnium.StaffPerformance.Data.Postgres.Conventions
{
    internal class TableFactConvention : IConvention
    {
        private readonly IEnumerable<string> myEntities;
        private readonly string myFactPrefix;

        internal TableFactConvention(string pFactPrefix, IEnumerable<string> pEntities)
        {
            myEntities = pEntities ?? throw new ArgumentNullException(nameof(pEntities));
            myFactPrefix = string.IsNullOrEmpty(pFactPrefix) ? throw new ArgumentNullException(nameof(pFactPrefix)) : pFactPrefix;
        }

        public bool TryConfigure(IMutableEntityType pMutableEntityType)
        {
            var conventionApplied = false;
            var name = pMutableEntityType.DisplayName();

            if (myEntities.Any(n => n.Equals(name)))
            {
                var builder = new StringBuilder();

                builder.Append(myFactPrefix);
                builder.Append(pMutableEntityType.GetTableName());
                pMutableEntityType.SetTableName(builder.ToString());

                conventionApplied = true;
            }

            return conventionApplied;
        }
    }

}



