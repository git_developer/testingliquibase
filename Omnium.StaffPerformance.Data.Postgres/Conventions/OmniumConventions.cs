﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using System.Collections.Generic;

namespace Omnium.StaffPerformance.Data.Postgres.Conventions
{
    internal static class OmniumConventions
    {
        /// <summary>
        /// Setups the Omnium database conventions
        /// </summary>
        /// <param name="pReferenceEntities">entities that must be mark as reference</param>
        public static void SetOmniumConventions(this ModelBuilder pBuilder, IEnumerable<string> pReferenceEntities, IEnumerable<string> pFactEntities)
        {
            var conventions = new IConvention[] {
                new TableFactConvention("Mspf", pFactEntities), new TableReferenceConvention(pReferenceEntities),new TableUpperSnakeCaseConvention(), 
                new ColumnAbreviationUpperSnakeCaseConvention(),
            };

            var entities = EntityTypes(pBuilder);

            foreach (var entity in entities)
            {
                if (entity.FindPrimaryKey() != null)
                {
                    foreach (var convention in conventions)
                    {
                        convention.TryConfigure(entity);
                    }
                }
                else
                {
                    //only for the complex type conventions
                    new ColumnAbreviationUpperSnakeCaseConvention().TryConfigure(entity);
                }
            }
        }

        private static IEnumerable<IMutableEntityType> EntityTypes(ModelBuilder pBuilder)
        {
            return pBuilder.Model.GetEntityTypes();
        }

    }

}
