﻿using Microsoft.EntityFrameworkCore.Metadata;

namespace Omnium.StaffPerformance.Data.Postgres.Conventions
{
    internal interface IConvention
    {
        /// <summary>
        /// Trys to applied a configuration. If the configuration is applied with sucess return true otherwise returns false
        /// </summary>
        /// <param name="pMutableEntityType">Entity to be configured</param>
        /// <returns></returns>
        public bool TryConfigure(IMutableEntityType pMutableEntityType);
    }

}
