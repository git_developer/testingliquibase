﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Omnium.StaffPerformance.Data.Postgres.Extensions;

namespace Omnium.StaffPerformance.Data.Postgres.Conventions
{
    internal class ColumnAbreviationUpperSnakeCaseConvention : IConvention
    {
        public bool TryConfigure(IMutableEntityType pMutableEntityType)
        {
            var properties = pMutableEntityType.GetProperties();

            foreach (var property in properties)
            {
                var columnName = property.Name.Replace("Code", "Cd")
                                              .Replace("Value", "Val")
                                              .Replace("Description", "Desc")
                                              .Replace("Quantity", "Qty")
                                              .FromPascalCaseToUpperSnakeCase();
                property.SetColumnName(columnName);
            }

            return true;
        }

    }

}
