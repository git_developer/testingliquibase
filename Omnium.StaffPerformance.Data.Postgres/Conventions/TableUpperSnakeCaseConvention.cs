﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Omnium.StaffPerformance.Data.Postgres.Extensions;

namespace Omnium.StaffPerformance.Data.Postgres.Conventions
{
    internal class TableUpperSnakeCaseConvention : IConvention
    {
        public bool TryConfigure(IMutableEntityType pMutableEntityType)
        {
            var name = pMutableEntityType.GetTableName()
                                         .FromPascalCaseToUpperSnakeCase();
            pMutableEntityType.SetTableName(name);

            return true;
        }
    }

}
