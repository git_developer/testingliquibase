﻿using Omnium.StaffPerformance.Business.Domain.Entities;
using Omnium.StaffPerformance.Business.Domain.Repositories;
using Omnium.StaffPerformance.Data.Postgres.Context;

namespace Omnium.StaffPerformance.Data.Postgres.Repositories
{
    public class CalendarRepository : BaseRepository<Calendar>, ICalendarRepository
    {
        /// <summary>
        /// Creates an instance of the Calendar repository
        /// </summary>
        /// <param name="pContext">Database context</param>
        public CalendarRepository(PostgresContext pContext) : base(pContext)
        {
            myContext = pContext;
        }
    }
}
