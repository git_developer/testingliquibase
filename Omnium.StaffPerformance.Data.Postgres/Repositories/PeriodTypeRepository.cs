﻿using Omnium.StaffPerformance.Business.Domain.Entities;
using Omnium.StaffPerformance.Business.Domain.Repositories;
using Omnium.StaffPerformance.Data.Postgres.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Omnium.StaffPerformance.Data.Postgres.Repositories
{
    public class PeriodTypeRepository : BaseRepository<PeriodType>, IPeriodTypeRepository
    {
        /// <summary>
        /// Creates an instance of the period type repository
        /// </summary>
        /// <param name="pContext">Database context</param>
        public PeriodTypeRepository(PostgresContext pContext) : base(pContext)
        {
            myContext = pContext;
        }
        /// <summary>
        /// Gets a period Type instance from the database with the same name parameter
        /// </summary>
        /// <param name="pName">name of the period type we want</param>
        /// <returns>a period type instance with the same name as we sent</returns>
        public PeriodType GetPeriodTypeByName(string pName)
        {
            if (string.IsNullOrEmpty(nameof(pName)))
            {
                throw new ArgumentNullException(nameof(pName));
            }
            return myContext.PeriodTypes.FirstOrDefault(o=>o.Name.Equals(pName));
        }
        /// <summary>
        /// Gets a period Type instance from the database with the same Id parameter
        /// </summary>
        /// <param name="pId">Id of the period type we want</param>
        /// <returns>a period type instance with the same Id as we sent</returns>
        public PeriodType GetPeriodTypeById(string pId)
        {
            if (string.IsNullOrEmpty(nameof(pId)))
            {
                throw new ArgumentNullException(nameof(pId));
            }
            return myContext.PeriodTypes.FirstOrDefault(o => o.Id.Equals(pId));
        }
        /// <summary>
        /// Gets all the existing period types from the database
        /// </summary>
        /// <returns>list with all period types</returns>
        public List<PeriodType> GetPeriodTypes()
        {
            return myContext.PeriodTypes.ToList();
        }
    }
}
