﻿using Omnium.StaffPerformance.Business.Domain.Entities;
using Omnium.StaffPerformance.Business.Domain.Repositories;
using Omnium.StaffPerformance.Data.Postgres.Context;
using System;
using System.Linq;

namespace Omnium.StaffPerformance.Data.Postgres.Repositories
{
    public class EmployeeRepository : BaseRepository<Employee>, IEmployeeRepository
    {

        /// <summary>
        /// Creates an instance of the employees repository
        /// </summary>
        /// <param name="pContext">Database context</param>
        public EmployeeRepository(PostgresContext pContext) : base(pContext)
        {
            myContext = pContext;
        }

        /// <summary>
        /// Finds the employee with a certain email on the db and returns an instance of said employee
        /// </summary>
        /// <param name="pEmail">email of the employee we want to find</param>
        /// <returns>instance of the employee</returns>
        public Employee GetEmployeeByEmail(string pEmail)
        {
            if (string.IsNullOrEmpty(nameof(pEmail)))
            {
                throw new ArgumentNullException(nameof(pEmail));
            }
            return myContext.Employees.FirstOrDefault(o => o.Email.Equals(pEmail));
        }
        /// <summary>
        /// Finds the employee with a certain ID on the db and returns an instance of said employee
        /// </summary>
        /// <param name="pID">ID of the employee we want to find</param>
        /// <returns>instance of the employee</returns>
        public Employee GetEmployeeById(string pID)
        {
            if (string.IsNullOrEmpty(nameof(pID)))
            {
                throw new ArgumentNullException(nameof(pID));
            }
            return myContext.Employees.FirstOrDefault(o => o.Id.Equals(pID));
        }
        /// <summary>
        /// Checks if the id matches one of an existing employee.Returns false if it does not match
        /// </summary>
        /// <param name="pId">Id of the employee we are looking for</param>
        /// <returns>true if there is a match, false if there is not</returns>
        public bool CheckEmployeeExists(string pId)
        {
            if (string.IsNullOrEmpty(nameof(pId)))
            {
                throw new ArgumentNullException(nameof(pId));
            }
            if( myContext.Employees.FirstOrDefault(o => o.Id.Equals(pId)) == null)
            {
                return false;
            }
            return true;
        }
    }
}
