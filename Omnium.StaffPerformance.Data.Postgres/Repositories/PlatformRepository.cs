﻿using Microsoft.EntityFrameworkCore;
using Omnium.StaffPerformance.Business.Domain.Entities;
using Omnium.StaffPerformance.Business.Domain.Repositories;
using Omnium.StaffPerformance.Business.Exceptions;
using Omnium.StaffPerformance.Data.Postgres.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Omnium.StaffPerformance.Data.Postgres.Repositories
{
    public class PlatformRepository : BaseRepository<Platform>, IPlatformRepository
    {
        /// <summary>
        /// Creates an instance of the Platform repository
        /// </summary>
        /// <param name="pContext">Database context</param>
        public PlatformRepository(PostgresContext pContext) : base(pContext)
        {
            myContext = pContext;
        }

        public Task<List<Platform>> GetPlatformsAsync(int pPage, int pRecords)
        {
            if (pPage <= 0)
            {
                throw new ArgumentException(MessageBuilder.GetMessage(ECode.InvalidField, "pPage"), paramName: nameof(pPage));
            }

            if (pRecords <= 0)
            {
                throw new ArgumentException(MessageBuilder.GetMessage(ECode.InvalidField, "pRecords"), paramName: nameof(pRecords));
            }

            pPage--;

            return myContext.Platform.Skip(pPage * pRecords)
                                     .Take(pRecords)
                                     .ToListAsync();
        }
    }
}
