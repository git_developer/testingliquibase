﻿using Omnium.StaffPerformance.Business.Domain.Entities;
using Omnium.StaffPerformance.Business.Domain.Repositories;
using Omnium.StaffPerformance.Data.Postgres.Context;
using System;
using System.Linq;

namespace Omnium.StaffPerformance.Data.Postgres.Repositories
{
    public class StoreRepository : BaseRepository<Store>, IStoreRepository
    {
        /// <summary>
        /// Creates an instance of the Store repository
        /// </summary>
        /// <param name="pContext">Database context</param>
        public StoreRepository(PostgresContext pContext) : base(pContext)
        {
            myContext = pContext;
        }

        /// <summary>
        /// Finds a store with the id specified
        /// </summary>
        /// <param name="pId">id of the store we want</param>
        /// <returns>the store with that id</returns>
        public Store GetStoreById(string pId)
        {
            if (string.IsNullOrEmpty(nameof(pId)))
            {
                throw new ArgumentNullException(nameof(pId));
            }

            return myContext.Stores.FirstOrDefault(o => o.Id.Equals(pId));
        }
    }
}
