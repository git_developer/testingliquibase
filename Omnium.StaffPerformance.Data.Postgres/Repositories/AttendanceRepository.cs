﻿using Omnium.StaffPerformance.Business.Domain.Entities;
using Omnium.StaffPerformance.Business.Domain.Repositories;
using Omnium.StaffPerformance.Data.Postgres.Context;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Omnium.StaffPerformance.Data.Postgres.Repositories
{
    public class AttendanceRepository : BaseRepository<Attendance>, IAttendanceRepository
    {

        /// <summary>
        /// Creates an instance of the attendance repository
        /// </summary>
        /// <param name="pContext">Database context</param>
        public AttendanceRepository(PostgresContext pContext) : base(pContext)
        {
            myContext = pContext;
        }
        
        /// <summary>
        /// Returns the last attendance of a specific employee with the given Id
        /// </summary>
        /// <param name="pEmployeeId">Id of the employee we want the last attendance from</param>
        /// <returns>His last attendance</returns>
        public Attendance GetLastAttendanceByEmployeeId(string pEmployeeId)
        {
            if (string.IsNullOrEmpty(nameof(pEmployeeId)))
            {
                throw new ArgumentNullException(nameof(pEmployeeId));
            }
            return myContext.Attendances.Where(o=>o.EmployeeId.Equals(pEmployeeId)).OrderByDescending(a => a.Date).FirstOrDefault();

        }
        /// <summary>
        /// Returns all the attendances of a specific employee
        /// </summary>
        /// <param name="pEmployeeId">employee we want the attendances from</param>
        /// <returns>all of its attendances</returns>
        public List<Attendance> GetAttendancesByEmployeeId(string pEmployeeId)
        {
            if (string.IsNullOrEmpty(nameof(pEmployeeId)))
            {
                throw new ArgumentNullException(nameof(pEmployeeId));
            }

            return myContext.Attendances.Where(o => o.EmployeeId.Equals(pEmployeeId)).ToList();

        }
    }
}
