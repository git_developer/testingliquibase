﻿using Omnium.StaffPerformance.Business.Domain.Entities;
using Omnium.StaffPerformance.Business.Domain.Repositories;
using Omnium.StaffPerformance.Data.Postgres.Context;

namespace Omnium.StaffPerformance.Data.Postgres.Repositories
{
    public class ScheduleIntervalRepository: BaseRepository<ScheduleInterval>, IScheduleIntervalRepository
    {
        /// <summary>
        /// Creates an instance of the schedule interval repository
        /// </summary>
        /// <param name="pContext">Database context</param>
        public ScheduleIntervalRepository(PostgresContext pContext) : base(pContext)
        {
            myContext = pContext;
        }
    }
}
