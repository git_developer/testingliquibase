﻿using Omnium.StaffPerformance.Business.Domain.Repositories;
using Omnium.StaffPerformance.Data.Postgres.Context;

namespace Omnium.StaffPerformance.Data.Postgres.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {

        private readonly PostgresContext myContext;
        private IEmployeeRepository myEmployees;
        private IAttendanceRepository myAttendances;
        private IHolidayRepository myHolidays;
        private ICountryRepository myCountries;
        private IPeriodTypeRepository myPeriodTypes;
        private IStoreRepository myStores;
        private IScheduleRepository mySchedules;
        private ICalendarRepository myCalendars;
        private IScheduleIntervalRepository myScheduleIntervals;
        private ICompanyRepository myCompanies;
        private IPlatformRepository myPlatforms;

        public UnitOfWork(PostgresContext Context)
        {
            myContext = Context;
        }

        public IEmployeeRepository Employees
        {
            get
            {
                return myEmployees ??= new EmployeeRepository(myContext);
            }
        }

        public IAttendanceRepository Attendances
        {
            get
            {
                return myAttendances ??= new AttendanceRepository(myContext);
            }
        }
        public IHolidayRepository Holidays
        {
            get
            {
                return myHolidays ??= new HolidayRepository(myContext);
            }
        }
        public ICountryRepository Countries
        {
            get
            {
                return myCountries ??= new CountryRepository(myContext);
            }
        }
        public IPeriodTypeRepository PeriodTypes
        {
            get
            {
                return myPeriodTypes ??= new PeriodTypeRepository(myContext);
            }
        }
        public IStoreRepository Stores
        {
            get
            {
                return myStores ??= new StoreRepository(myContext);
            }
        }
        public IScheduleRepository Schedules
        {
            get
            {
                return mySchedules ??= new ScheduleRepository(myContext);
            }
        }
        public ICalendarRepository Calendars
        {
            get
            {
                return myCalendars ??= new CalendarRepository(myContext);
            }
        }

        public IScheduleIntervalRepository ScheduleIntervals
        {
            get
            {
                return myScheduleIntervals ??= new ScheduleIntervalRepository(myContext);
            }
        }
        public ICompanyRepository Companies
        {
            get
            {
                return myCompanies ??= new CompanyRepository(myContext);
            }
        }
        public IPlatformRepository Platforms
        {
            get
            {
                return myPlatforms ??= new PlatformRepository(myContext);
            }
        }

        public void Commit()
        {
            myContext.SaveChanges();
        }
    }
}
