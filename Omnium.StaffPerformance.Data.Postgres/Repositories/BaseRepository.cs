﻿using Microsoft.EntityFrameworkCore;
using Omnium.StaffPerformance.Business.Domain.Repositories;
using Omnium.StaffPerformance.Data.Postgres.Context;

namespace Omnium.StaffPerformance.Data.Postgres.Repositories
{
    public class BaseRepository<TEntity> : IRepository<TEntity> where TEntity : class
    {

        protected PostgresContext myContext;
        internal DbSet<TEntity> myDbSet;

        public BaseRepository(PostgresContext pContext)
        {
            this.myContext = pContext;
            this.myDbSet = myContext.Set<TEntity>();
        }

        public virtual TEntity GetById(object pId)
        {
            return myDbSet.Find(pId);
        }

        public virtual void Add(TEntity pEntity)
        {
            myDbSet.Add(pEntity);
        }

        public virtual void Delete(TEntity pEntityToDelete)
        {
            if (myContext.Entry(pEntityToDelete).State == EntityState.Detached)
            {
                myDbSet.Attach(pEntityToDelete);
            }
            myDbSet.Remove(pEntityToDelete);
        }
    }
}
