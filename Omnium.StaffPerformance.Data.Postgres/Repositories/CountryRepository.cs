﻿using Microsoft.EntityFrameworkCore;
using Omnium.StaffPerformance.Business.Domain.Entities;
using Omnium.StaffPerformance.Business.Domain.Repositories;
using Omnium.StaffPerformance.Business.Exceptions;
using Omnium.StaffPerformance.Data.Postgres.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Omnium.StaffPerformance.Data.Postgres.Repositories
{
    public class CountryRepository : BaseRepository<Country>, ICountryRepository
    {

        /// <summary>
        /// Creates an instance of the country repository
        /// </summary>
        /// <param name="pContext">Database context</param>
        public CountryRepository(PostgresContext pContext) : base(pContext)
        {
            myContext = pContext;
        }

        public Task<List<Country>> GetCountriesAsync(int pPage, int pRecords)
        {
            if (pPage <= 0)
            {
                throw new ArgumentException(MessageBuilder.GetMessage(ECode.InvalidField, "pPage"), paramName: nameof(pPage));
            }

            if (pRecords <= 0)
            {
                throw new ArgumentException(MessageBuilder.GetMessage(ECode.InvalidField, "pRecords"), paramName: nameof(pRecords));
            }

            pPage--;

            return myContext.Countries.Skip(pPage * pRecords)
                                      .Take(pRecords)
                                      .ToListAsync();
        }

        /// <summary>
        /// Returns a country with the specified Name
        /// </summary>
        /// <param name="pCountryName">name of the country we want</param>
        /// <returns>the country with that name</returns>
        public Country GetCountryByName(string pCountryName)
        {
            if (string.IsNullOrEmpty(nameof(pCountryName)))
            {
                throw new ArgumentNullException(nameof(pCountryName));
            }
            return myContext.Countries.FirstOrDefault(o => o.Name.Equals(pCountryName));

        }

    }
}
