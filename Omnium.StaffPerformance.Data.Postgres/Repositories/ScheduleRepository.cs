﻿using Microsoft.EntityFrameworkCore;
using Omnium.StaffPerformance.Business.Domain.Entities;
using Omnium.StaffPerformance.Business.Domain.Repositories;
using Omnium.StaffPerformance.Data.Postgres.Context;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Omnium.StaffPerformance.Data.Postgres.Repositories
{
    public class ScheduleRepository : BaseRepository<Schedule>, IScheduleRepository
    {
        /// <summary>
        /// Creates an instance of the Schedule repository
        /// </summary>
        /// <param name="pContext">Database context</param>
        public ScheduleRepository(PostgresContext pContext) : base(pContext)
        {
            myContext = pContext;
        }
        /// <summary>
        /// Returns all the schedules of a given employee in a given date
        /// </summary>
        /// <param name="pId"></param>
        /// <returns>all the schedules of a given employee</returns>
        public List<Schedule> GetScheduleByEmployeeIdAndDay(string pId, DateTime pDate)
        {
            if (string.IsNullOrEmpty(nameof(pId)))
            {
                throw new ArgumentNullException(nameof(pId));
            }
            return myContext.Schedules.Include(calendar=>calendar.Calendar).Include(periodtype => periodtype.PeriodType).Where(o =>o.Begin >= pDate && o.End<pDate.AddDays(1) && o.EmployeeId.Equals(pId)).OrderBy(a => a.Begin).ToList();
        }

        /// <summary>
        /// Checks if there are any shcedules that intersect the schedule we send as parameter
        /// </summary>
        /// <param name="pSchedule">Schedule we want to check intersections from</param>
        /// <returns>a boolean, true if it intersects, false if it does not</returns>
        public bool CheckExistsAnotherSchedule(Schedule pSchedule)
        {
            return myContext.Schedules.Where(s => s.EmployeeId == pSchedule.EmployeeId && s.CalendarId==pSchedule.CalendarId).Any(s => (s.Begin > pSchedule.Begin && s.Begin<pSchedule.End) ||
                                                                                                                                       (s.End > pSchedule.Begin && s.End<pSchedule.End) ||
                                                                                                                                       (s.Begin < pSchedule.Begin && s.End > pSchedule.End));
        }

        /// <summary>
        /// Retrieves a schedule from the database by its id
        /// </summary>
        /// <param name="pId">id of the schedule</param>
        /// <returns>the schedule with that id</returns>
        public Schedule GetScheduleById(string pId)
        {
            if (string.IsNullOrEmpty(nameof(pId)))
            {
                throw new ArgumentNullException(nameof(pId));
            }
            return myContext.Schedules.Include(calendar => calendar.Calendar).Include(periodtype => periodtype.PeriodType).
                                        Where(o => o.Id.Equals(pId)).FirstOrDefault();
        }
    }
}
