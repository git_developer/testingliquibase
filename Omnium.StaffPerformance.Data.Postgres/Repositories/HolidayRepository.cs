﻿using Omnium.StaffPerformance.Business.Domain.Entities;
using Omnium.StaffPerformance.Business.Domain.Repositories;
using Omnium.StaffPerformance.Data.Postgres.Context;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Omnium.StaffPerformance.Data.Postgres.Repositories
{
    public class HolidayRepository : BaseRepository<Holiday>, IHolidayRepository
    {

        /// <summary>
        /// Creates an instance of the Holiday repository
        /// </summary>
        /// <param name="pContext">Database context</param>
        public HolidayRepository(PostgresContext pContext) : base(pContext)
        {
            myContext = pContext;
        }
        /// <summary>
        /// Finds an holiday with the name, country Id and name secified
        /// </summary>
        /// <param name="pDate">date we want the holiday to be of</param>
        /// <param name="pCountryId">Country Id we want the holiday to be of</param>
        /// <param name="pName">Name we want the holiday to be of</param>
        /// <returns>the holiday found</returns>
        public Holiday GetHolidayByNameCountryCodeAndDate(DateTime pDate, string pCountryId, string pName)
        {
            if (string.IsNullOrEmpty(nameof(pCountryId)))
            {
                throw new ArgumentNullException(nameof(pCountryId));
            }
            if (string.IsNullOrEmpty(nameof(pName)))
            {
                throw new ArgumentNullException(nameof(pName));
            }

            return myContext.Holidays.Where(o => o.CountryId.Equals(pCountryId)).Where(o => o.Name.Equals(pName)).Where(o => o.Date.Equals(pDate)).FirstOrDefault();
        }

        /// <summary>
        /// Finds an holiday with the  country Id and name specified
        /// </summary>
        /// <param name="pDate">date we want the holiday to be of</param>
        /// <param name="pCountryId">Country Id we want the holiday to be of</param>
        /// <returns>the holiday found</returns>
        public Holiday GetHolidayByCountryCodeAndDate(DateTime pDate, string pCountryId)
        {
            if (string.IsNullOrEmpty(nameof(pCountryId)))
            {
                throw new ArgumentNullException(nameof(pCountryId));
            }

            return myContext.Holidays.Where(o => o.CountryId.Equals(pCountryId)).Where(o => o.Date.Equals(pDate)).FirstOrDefault();
        }
        /// <summary>
        /// Finds an holiday with the id specified
        /// </summary>
        /// <param name="pId">id of the holiday we want</param>
        /// <returns>the holiday with that id</returns>
        public Holiday GetHolidayById(string pId)
        {
            if (string.IsNullOrEmpty(nameof(pId)))
            {
                throw new ArgumentNullException(nameof(pId));
            }

            return myContext.Holidays.FirstOrDefault(o => o.Id.Equals(pId));
        }
        /// <summary>
        /// Finds all holidays
        /// </summary>
        /// <returns>allHolidays on the database</returns>
        public List<Holiday> GetHolidays()
        {
            return myContext.Holidays.ToList();
        }
    }
}
