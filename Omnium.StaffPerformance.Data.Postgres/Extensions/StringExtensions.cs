﻿using System.Linq;
using System.Text;


namespace Omnium.StaffPerformance.Data.Postgres.Extensions
{
    internal static class StringExtensions
    {
        /// <summary>
        /// Converts a string from a pascal case to upper snake case
        /// </summary>
        /// <returns></returns>
        internal static string FromPascalCaseToUpperSnakeCase(this string pString)
        {
            var builder = new StringBuilder();

            if (pString.Any(c => char.IsLower(c)))
            {
                builder.Append(char.ToUpper(pString.ElementAt(0)));
                for (var i = 1; i < pString.Length; i++)
                {
                    var c = pString.ElementAt(i);

                    if (char.IsUpper(c))
                    {
                        builder.AppendFormat("_{0}", c);
                    }
                    else
                    {
                        builder.Append(char.ToUpper(c));
                    }
                }
            }
            else
            {
                builder.Append(pString);
            }

            return builder.ToString();
        }
    }
}
