﻿using Omnium.StaffPerformance.Business.Domain.Entities;
using Omnium.StaffPerformance.Services.API.Models;
using System.Collections.Generic;

namespace Omnium.StaffPerformance.Services.API.Adapters
{
    internal class PeriodTypeDtoAdapter
    {
        internal PeriodType Adapt(PeriodTypeDto pDto, Company pCompany)
        {
            return new PeriodType(pCompany, pDto.Name, pDto.Value, "A");
        }

        internal PeriodTypeResponseDto AdaptResponse(PeriodType pObj)
        {
            return new PeriodTypeResponseDto()
            {
                Id = pObj.Id,
                CompanyId = pObj.CompanyId,
                Name=pObj.Name,
                Value=pObj.Value
            };
        }

        internal List<PeriodTypeResponseDto> AdaptListResponse(List<PeriodType> list)
        {
            List<PeriodTypeResponseDto> returnList = new List<PeriodTypeResponseDto>();

            foreach(var pt in list)
            {
                var ptResponse= new PeriodTypeResponseDto()
                {
                    Id = pt.Id,
                    CompanyId=pt.CompanyId,
                    Name = pt.Name,
                    Value = pt.Value
                };
                returnList.Add(ptResponse);
            }
            return returnList;
        }

    }
}
