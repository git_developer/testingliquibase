﻿using Omnium.StaffPerformance.Business.Domain.Entities;
using Omnium.StaffPerformance.Services.API.Extensions;
using Omnium.StaffPerformance.Services.API.Models;
using System;
using System.Collections.Generic;
using System.Globalization;

namespace Omnium.StaffPerformance.Services.API.Adapters
{
    internal class HolidayDtoAdapter
    {

        internal Holiday Adapt(HolidayDto pDto)
        {
            DateTime date = DateTime.ParseExact(pDto.Date, AvailableDateFormatsExtension.GetDateFormat(), CultureInfo.InvariantCulture);

            return new Holiday(date, pDto.Name, pDto.CountryId, "A");
        }

        internal HolidayResponseDto AdaptResponse(Holiday pObj)
        {
            return new HolidayResponseDto
            {
                Id=pObj.Id,
                Date=pObj.Date.ToString(AvailableDateFormatsExtension.GetDateFormat()),
                Name=pObj.Name,
                CountryId=pObj.CountryId
            };
        }

        internal List<HolidayResponseDto> AdaptListResponse(List<Holiday> pListObj)
        {
            List<HolidayResponseDto> returnList = new List<HolidayResponseDto>();

            foreach(var hol in pListObj)
            {
                var holDto=new HolidayResponseDto
                {
                    Id = hol.Id,
                    Date = hol.Date.ToString(AvailableDateFormatsExtension.GetDateFormat()),
                    Name = hol.Name,
                    CountryId = hol.CountryId
                };
                returnList.Add(holDto);

            }

            return returnList;
        }

        internal StoreHolidayDto AdaptStoreHoliday(StoreHoliday pObj)
        {
            return new StoreHolidayDto
            {
                StoreId=pObj.Store.Id,
                HolidayId=pObj.Holiday.Id
            };
        }
    }
}
