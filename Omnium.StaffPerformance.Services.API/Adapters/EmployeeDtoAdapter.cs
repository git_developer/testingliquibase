﻿using Omnium.StaffPerformance.Business.Domain.Entities;
using Omnium.StaffPerformance.Services.API.Extensions;
using Omnium.StaffPerformance.Services.API.Models;
using System;
using System.Globalization;

namespace Omnium.StaffPerformance.Services.API.Adapters
{
    internal class EmployeeDtoAdapter
    {

        /// <summary>
        /// Adapts the employee data transfer object to an employee object
        /// </summary>
        /// <param name="pDto">employee data transfer object</param>
        /// <returns> an employee object</returns>
        internal Employee Adapt(EmployeeDto pDto, Company pCompany)
        {

            Employee dbEmployee = null;

            string ddMMyyyyFormat = AvailableDateFormatsExtension.GetDateFormat();

            DateTime birthDate = DateTime.ParseExact(pDto.BirthDate, ddMMyyyyFormat, CultureInfo.InvariantCulture);



            dbEmployee = new Employee(pCompany, pDto.ShortName, pDto.FullName,
                                        pDto.Gender, pDto.Email, pDto.MobilePhoneNumber, pDto.PhoneNumber,
                                        birthDate, "A");

            return dbEmployee;
        }

        /// <summary>
        /// Adapts an employee object into the response structure
        /// </summary>
        /// <param name="e">employee object</param>
        /// <returns>the response structure filled with employees parameters</returns>
        internal EmployeeResponseDto AdaptResponse(Employee e)
        {

            EmployeeResponseDto dto = new EmployeeResponseDto
            {
                Id = e.Id,
                CompanyId=e.CompanyId,
                ShortName=e.NameShort,
                FullName=e.NameFull,
                Gender=e.Gender,
                Email=e.Email,
                MobilePhoneNumber=e.MobilePhoneNumber,
                PhoneNumber=e.PhoneNumber,
                BirthDate=e.BirthDate.ToString(AvailableDateFormatsExtension.GetDateFormat()),
            };

            return dto;
        }

    }
}
