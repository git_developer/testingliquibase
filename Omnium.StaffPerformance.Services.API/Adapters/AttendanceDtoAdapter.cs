﻿using Omnium.StaffPerformance.Business.Domain.Entities;
using Omnium.StaffPerformance.Business.Domain.Services.Attendances;
using Omnium.StaffPerformance.Services.API.Extensions;
using Omnium.StaffPerformance.Services.API.Models;
using System;
using System.Collections.Generic;
using System.Globalization;

namespace Omnium.StaffPerformance.Services.API.Adapters
{
    internal class AttendanceDtoAdapter
    {
        /// <summary>
        /// Adapts the attendance data transfer object to an attendance object
        /// </summary>
        /// <param name="pDto">attendance data transfer object</param>
        /// <returns>an attendance object</returns>
        internal Attendance Adapt(AttendanceDto pDto, Company pCompany)
        {
            DateTime date = DateTime.ParseExact(pDto.Date, AvailableDateFormatsExtension.GetDateTimeFormat(), CultureInfo.InvariantCulture);

            return new Attendance(pCompany, pDto.EmployeeId, date, (EAttendanceType)pDto.Type, "A");

        }
        /// <summary>
        /// Adapts an attendance object into the response structure
        /// </summary>
        /// <param name="attendance">attendance object</param>
        /// <returns>response structure with the attendance parameters</returns>
        internal AttendanceResponseDto AdaptResponse(Attendance attendance)
        {
            return new AttendanceResponseDto
            {
                CompanyId = attendance.CompanyId,
                Id = attendance.Id,
                EmployeeId = attendance.EmployeeId,
                Date = attendance.Date.ToString(AvailableDateFormatsExtension.GetDateTimeFormat()),
                Type = (int)attendance.Type,
                Status = attendance.Status
            };
        }
        /// <summary>
        /// Adapts a list of attendance objects into the response structure
        /// </summary>
        /// <param name="pList">list of attendance objects</param>
        /// <returns>list of response structures with the attendance parameters</returns>
        internal List<AttendanceResponseDto> AdaptListResponse(List<Attendance> pList)
        {
            List<AttendanceResponseDto> returnList = new List<AttendanceResponseDto>();

            foreach(var att in pList)
            {
                returnList.Add(new AttendanceResponseDto
                            {
                                Id = att.Id,
                                EmployeeId = att.EmployeeId,
                                Date = att.Date.ToString(AvailableDateFormatsExtension.GetDateTimeFormat()),
                                Type = (int)att.Type,
                                Status = att.Status
                            });
            }

            return returnList;

        }
        /// <summary>
        /// Converts a date in string format to our global dateTime format
        /// </summary>
        /// <param name="pDateTime">string datetime to be converted</param>
        /// <returns>datetime respective to the given string in the correct format</returns>
        internal DateTime AdaptToDateTimeFormat(string pDateTime)
        {
            return DateTime.ParseExact(pDateTime, AvailableDateFormatsExtension.GetDateTimeFormat(), CultureInfo.InvariantCulture);
        }
    }
}
