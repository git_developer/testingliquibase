﻿using Omnium.StaffPerformance.Business.Domain.Entities;
using Omnium.StaffPerformance.Services.API.Extensions;
using Omnium.StaffPerformance.Services.API.Models;
using System;
using System.Collections.Generic;
using System.Globalization;

namespace Omnium.StaffPerformance.Services.API.Adapters
{
    public class ScheduleDtoAdapter
    {
        /// <summary>
        /// Adapts a List of ScheduleDto into Schedule objects and returns the list with all the objects created
        /// </summary>
        /// <param name="pList">List of schedule dtos</param>
        /// <param name="pComp">Company for the schedules to be in</param>
        /// <param name="pStore">Store for the schedules to be in</param>
        /// <param name="pEmployeeId">Employee Id to whom these schedules belong to</param>
        /// <returns>A list with the created schedules</returns>
        public List<Schedule> AdaptScheduleDto(List<ScheduleEntryEmployeeDto> pList, Company pComp, Store pStore, string pEmployeeId)
        {
            List<Schedule> listReturn = new List<Schedule>();

            foreach(var schedule in pList)
            {
                DateTime begin= DateTime.ParseExact(schedule.Begin, AvailableDateFormatsExtension.GetDateTimeFormat(), CultureInfo.InvariantCulture);
                DateTime end = DateTime.ParseExact(schedule.End, AvailableDateFormatsExtension.GetDateTimeFormat(), CultureInfo.InvariantCulture);

                Schedule s = new Schedule(pEmployeeId, pStore, schedule.CalendarId, pComp, schedule.PeriodTypeId, begin, end, schedule.Status);

                listReturn.Add(s);
            }
            return listReturn;
        }

        /// <summary>
        /// Adapts a List of UpdateScheduleDto into Schedule objects and returns the list with all the objects created as value and the id of the schedule we want to update as key
        /// </summary>
        /// <param name="pList">List of update schedule dtos</param>
        /// <param name="pComp">Company for the schedules to be in</param>
        /// <param name="pStore">Store for the schedules to be in</param>
        /// <param name="pEmployeeId">Employee Id to whom these schedules belong to</param>
        /// <returns>the list with all the objects created as value and the id of the schedule we want to update as key</returns>
        public Dictionary<string, Schedule> AdaptUpdateScheduleDto(List<ScheduleEntryEmployeeUpdateDto> pList, Company pComp, Store pStore, string pEmployeeId)
        {
            Dictionary<string, Schedule> listReturn = new Dictionary<string, Schedule>();

            foreach (var schedule in pList)
            {
                DateTime begin = DateTime.ParseExact(schedule.Begin, AvailableDateFormatsExtension.GetDateTimeFormat(), CultureInfo.InvariantCulture);
                DateTime end = DateTime.ParseExact(schedule.End, AvailableDateFormatsExtension.GetDateTimeFormat(), CultureInfo.InvariantCulture);

                Schedule s = new Schedule(pEmployeeId, pStore, schedule.CalendarId, pComp, schedule.PeriodTypeId, begin, end, schedule.Status);

                listReturn.Add(schedule.Id, s);
            }
            return listReturn;
        }

        /// <summary>
        /// Adapts a List with the employee Id and the schedules related to that employeeId to the appropriate response
        /// </summary>
        /// <param name="pList">List with the employee Id and all the schedules related to it</param>
        /// <returns>Response dto with a comprehensible structure for the schedules</returns>
        public List<ScheduleResponseDto> AdaptListResponse(Dictionary<string, List<Schedule>> pList)
        {
            List<ScheduleResponseDto> listReturn = new List<ScheduleResponseDto>();

            foreach(var employeeSchedule in pList)
            {
                List<ScheduleEntryEmployeeResponseDto> schedules = new List<ScheduleEntryEmployeeResponseDto>();
                foreach(var schedule in employeeSchedule.Value)
                {
                    var scheduleDto = new ScheduleEntryEmployeeResponseDto
                    {
                        Id = schedule.Id,
                        StoreId=schedule.StoreId,
                        Calendar=schedule.Calendar.Date.ToString(AvailableDateFormatsExtension.GetDateFormat()),
                        CompanyId=schedule.CompanyId,
                        PeriodType=schedule.PeriodType.Name,
                        Begin=schedule.Begin.ToString(AvailableDateFormatsExtension.GetDateTimeFormat()),
                        End= schedule.End.ToString(AvailableDateFormatsExtension.GetDateTimeFormat())

                    };
                    schedules.Add(scheduleDto);
                }

                var dto = new ScheduleResponseDto
                {
                    EmployeeId = employeeSchedule.Key,
                    EmployeeSchedule = schedules
                };
                listReturn.Add(dto);
            }

            return listReturn;
        }
        /// <summary>
        /// Adapts a string into Date format
        /// </summary>
        /// <param name="pDate">Date </param>
        /// <returns>The adapted datetime</returns>
        public DateTime AdaptDate(string pDate)
        {
            return DateTime.ParseExact(pDate, AvailableDateFormatsExtension.GetDateFormat(), CultureInfo.InvariantCulture);
        }
    }
}
