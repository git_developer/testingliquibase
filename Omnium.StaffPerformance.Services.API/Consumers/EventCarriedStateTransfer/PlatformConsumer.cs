﻿using MassTransit;
using Omnium.Business.Contracts;
using Omnium.StaffPerformance.Business.Domain.Entities;
using Omnium.StaffPerformance.Business.Domain.Services.Platforms;
using System;
using System.Threading.Tasks;

namespace Omnium.StaffPerformance.Services.API.Consumers.EventCarriedStateTransfer
{
    public class PlatformConsumer : IConsumer<PlatformCreatedEvent>, IConsumer<PlatformUpdatedEvent>
    {
        private readonly IPlatformService myPlatformService;

        public PlatformConsumer(IPlatformService pPlatformService)
        {
            myPlatformService = pPlatformService ?? throw new ArgumentNullException(nameof(pPlatformService));
        }

        public Task Consume(ConsumeContext<PlatformCreatedEvent> context)
        {
            var platform = new Platform(context.Message.Id, context.Message.Name, (EPlatformType)context.Message.PlatformType, context.Message.IntegrationData, context.Message.Status);
            myPlatformService.AddOrUpdate(platform);

            return Task.CompletedTask;
        }

        public Task Consume(ConsumeContext<PlatformUpdatedEvent> context)
        {
            var platform = new Platform(context.Message.Id, context.Message.Name, (EPlatformType)context.Message.PlatformType, context.Message.IntegrationData, context.Message.Status);
            myPlatformService.AddOrUpdate(platform);

            return Task.CompletedTask;
        }


    }
}
