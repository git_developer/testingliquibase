﻿using MassTransit;
using Omnium.Business.Contracts;
using Omnium.StaffPerformance.Business.Domain.Entities;
using Omnium.StaffPerformance.Business.Domain.Services.Countries;
using System;
using System.Threading.Tasks;

namespace Omnium.StaffPerformance.Services.API.Consumers.EventCarriedStateTransfer
{
    public class CountryConsumer : IConsumer<CountryCreatedEvent>, IConsumer<CountryUpdatedEvent>
    {

        private readonly ICountryService myCountryService;

        public CountryConsumer(ICountryService pCountryService)
        {
            myCountryService = pCountryService ?? throw new ArgumentNullException(nameof(pCountryService));
        }

        public Task Consume(ConsumeContext<CountryCreatedEvent> pContext)
        {
            var company = new Country(pContext.Message.Id, pContext.Message.Name, pContext.Message.ISO2, pContext.Message.ISO3, pContext.Message.Status);
            myCountryService.AddOrUpdate(company);

            return Task.CompletedTask;
        }

        public Task Consume(ConsumeContext<CountryUpdatedEvent> pContext)
        {
            var company = new Country(pContext.Message.Id, pContext.Message.Name, pContext.Message.ISO2, pContext.Message.ISO3, pContext.Message.Status);
            myCountryService.AddOrUpdate(company);

            return Task.CompletedTask;
        }
    }
}
