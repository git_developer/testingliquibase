﻿using MassTransit;
using Omnium.Business.Contracts;
using Omnium.StaffPerformance.Business.Domain.Entities;
using Omnium.StaffPerformance.Business.Domain.Services.Companies;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Omnium.StaffPerformance.Services.API.Consumers.EventCarriedStateTransfer
{
    public class CompanyConsumer : IConsumer<CompanyCreatedEvent>, IConsumer<CompanyUpdatedEvent>
    {
        private readonly ICompanyService myCompanyService;

        public CompanyConsumer(ICompanyService pCompanyService)
        {
            myCompanyService = pCompanyService ?? throw new ArgumentNullException(nameof(pCompanyService));
        }

        public Task Consume(ConsumeContext<CompanyCreatedEvent> pContext)
        {
            var companyPlatforms = pContext.Message.Platforms?.Select(c => new CompanyPlatform(c.EntityId, c.PlatformId, c.Status))
                                                              .ToHashSet();
            var company = new Company(pContext.Message.Id, pContext.Message.CompanyCode, pContext.Message.Name, pContext.Message.Street, pContext.Message.City, pContext.Message.ZipCode, pContext.Message.CountryId, pContext.Message.Status, companyPlatforms);
            myCompanyService.AddOrUpdate(company);

            return Task.CompletedTask;
        }

        public Task Consume(ConsumeContext<CompanyUpdatedEvent> pContext)
        {
            var companyPlatforms = pContext.Message.Platforms?.Select(c => new CompanyPlatform(c.EntityId, c.PlatformId, c.Status))
                                                              .ToHashSet();
            var company = new Company(pContext.Message.Id, pContext.Message.CompanyCode, pContext.Message.Name, pContext.Message.Street, pContext.Message.City, pContext.Message.ZipCode, pContext.Message.CountryId, pContext.Message.Status, companyPlatforms);
            myCompanyService.AddOrUpdate(company);

            return Task.CompletedTask;
        }

    }
}
