﻿using Omnium.StaffPerformance.Services.API.Extensions;

namespace Omnium.StaffPerformance.Services.API.Models
{
    public struct AttendanceDto
    {
        public string CompanyId { get; set; }
        public string EmployeeId { get; set; }
        [DateFormatValidation(false, "dd/MM/yyyy-HH:mm:ss")]
        public string Date { get; set; }
        public int Type { get; set; }
    }
}
