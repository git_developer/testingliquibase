﻿using Omnium.StaffPerformance.Services.API.Extensions;

namespace Omnium.StaffPerformance.Services.API.Models
{
    public struct HolidayDto
    {
        [DateFormatValidation(false, "dd/MM/yyyy")]
        public string Date { get; set; }
        public string Name { get; set; }
        public string CountryId { get; set; }

    }
}
