﻿
namespace Omnium.StaffPerformance.Services.API.Models
{
    public struct EmployeeResponseDto
    {
        public string Id { get; set; }  
        public string CompanyId { get; set; }
        public string ShortName { get; set; }
        public string FullName { get; set; }
        public string Gender { get; set; }
        public string Email { get; set; }
        public string MobilePhoneNumber { get; set; }
        public string PhoneNumber { get; set; }
        public string BirthDate { get; set; }
    }
}

