﻿namespace Omnium.StaffPerformance.Services.API.Models
{
    public struct AttendanceResponseDto
    {
        public string CompanyId { get; set; }
        public string Id { get; set; }
        public string EmployeeId { get; set; }
        public string Date { get; set; }
        public int Type { get; set; }
        public string Status { get; set; }
    }
}
