﻿
namespace Omnium.StaffPerformance.Services.API.Models
{
    public struct PeriodTypeResponseDto
    {
        public string CompanyId { get; set; }
        public string Id { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
    }
}
