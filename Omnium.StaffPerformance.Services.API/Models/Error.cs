﻿
namespace Omnium.StaffPerformance.Services.API.Models
{
    public struct Error
    {
        public int Code { get; set; }
        public string Message { get; set; }

    }
}
