﻿using System.Collections.Generic;

namespace Omnium.StaffPerformance.Services.API.Models
{
    public struct ScheduleResponseDto
    {
        public string EmployeeId { get; set; }
        public List<ScheduleEntryEmployeeResponseDto> EmployeeSchedule {get; set;}
    }

    public struct ScheduleEntryEmployeeResponseDto
    {
        public string Id { get; set; }
        public string StoreId { get; set; }
        public string Calendar { get; set; }
        public string CompanyId { get; set; }
        public string PeriodType { get; set; }
        public string Begin { get; set; }
        public string End { get; set; }
    }
}
