﻿
namespace Omnium.StaffPerformance.Services.API.Models
{
    public class StoreHolidayDto
    {
        public string StoreId { get; set; }
        public string HolidayId { get; set; }
    }
}
