﻿using Omnium.StaffPerformance.Services.API.Extensions;
using System.ComponentModel.DataAnnotations;


namespace Omnium.StaffPerformance.Services.API.Models
{
    public struct EmployeeDto
    {
        public string CompanyId { get; set; }
        public string ShortName { get; set; }
        public string FullName { get; set; }
        public string Gender { get; set; }
        [EmailAddress]
        public string Email { get; set; }
        public string MobilePhoneNumber { get; set; }
        public string PhoneNumber { get; set; }
        [DateFormatValidation(false, "dd/MM/yyyy")]
        public string BirthDate { get; set; }
    }
}
