﻿
namespace Omnium.StaffPerformance.Services.API.Models
{
    public struct HolidayResponseDto
    {
        public string Id { get; set; }
        public string Date { get; set; }
        public string Name { get; set; }
        public string CountryId { get; set; }
    }
}
