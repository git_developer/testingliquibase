﻿using Omnium.StaffPerformance.Services.API.Extensions;
using System.Collections.Generic;

namespace Omnium.StaffPerformance.Services.API.Models
{
    public struct ScheduleDto
    {
        public string EmployeeId { get; set; }
        public List<ScheduleEntryEmployeeDto> EmployeeSchedule { get; set; }
    }

    public struct ScheduleEntryEmployeeDto
    {
        public string CalendarId { get; set; }
        public string PeriodTypeId { get; set; }
        [DateFormatValidation(false, "dd/MM/yyyy-HH:mm:ss")]
        [TimespanValidation(15)]
        public string Begin { get; set; }
        [DateFormatValidation(false, "dd/MM/yyyy-HH:mm:ss")]
        [TimespanValidation(15)]
        public string End { get; set; }
        public string Status { get; set; }
    }
}
