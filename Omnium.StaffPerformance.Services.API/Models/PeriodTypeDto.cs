﻿
namespace Omnium.StaffPerformance.Services.API.Models
{
    public struct PeriodTypeDto
    {
        public string CompanyId { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
    }
}
