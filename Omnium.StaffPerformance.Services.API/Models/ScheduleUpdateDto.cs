﻿using Omnium.StaffPerformance.Services.API.Extensions;
using System.Collections.Generic;

namespace Omnium.StaffPerformance.Services.API.Models
{
    public struct ScheduleUpdateDto
    {
        public string EmployeeId { get; set; }
        public List<ScheduleEntryEmployeeUpdateDto> EmployeeSchedule { get; set; }
    }

    public struct ScheduleEntryEmployeeUpdateDto
    {
        public string Id { get; set; }
        public string CalendarId { get; set; }
        public string PeriodTypeId { get; set; }
        [DateFormatValidation(false, "dd/MM/yyyy-HH:mm:ss")]
        [TimespanValidation(15)]
        public string Begin { get; set; }
        [DateFormatValidation(false, "dd/MM/yyyy-HH:mm:ss")]
        [TimespanValidation(15)]
        public string End { get; set; }
        public string Status { get; set; }
    }
}
