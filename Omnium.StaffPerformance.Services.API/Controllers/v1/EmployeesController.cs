﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Omnium.StaffPerformance.Business.Domain.Entities;
using Omnium.StaffPerformance.Business.Domain.Services.Employees;
using Omnium.StaffPerformance.Services.API.Adapters;
using Omnium.StaffPerformance.Services.API.Models;

namespace Omnium.StaffPerformance.Services.API.Controllers.v1
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class EmployeesController : ControllerBase
    {

        private readonly IEmployeeService myEmployeeService;

        public EmployeesController(IEmployeeService pService)
        {
            myEmployeeService = pService;
        }
        /// <summary>
        /// Post method to create an employee
        /// </summary>
        /// <param name="pEmployeeDto"> employee to be added</param>
        /// <returns code= 200> returns the created employee</returns>
        /// <returns code= 400> returns an error explaining the invalid part of the request</returns>
        /// <returns code= 500> returns an error explaining why the server did not process the request</returns>
        [HttpPost]
        [ProducesResponseType(typeof(EmployeeResponseDto), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(Error), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(Error), StatusCodes.Status500InternalServerError)]
        public ActionResult<EmployeeResponseDto> Post(EmployeeDto pEmployeeDto)
        {

            var company = myEmployeeService.GetCompanyById(pEmployeeDto.CompanyId);

            var dtoAdapter = new EmployeeDtoAdapter();

            Employee employee = dtoAdapter.Adapt(pEmployeeDto, company);

            myEmployeeService.AddEmployee(employee);

            var response = dtoAdapter.AdaptResponse(employee);

            return Ok(response);
        }

        /// <summary>
        /// Gets an employee by its Id
        /// </summary>
        /// <param name="pID">Id of the required employee</param>
        /// <returns code= 200> returns the requested employee</returns>
        /// <returns code= 400> returns an error explaining the invalid part of the request</returns>
        /// <returns code= 500> returns an error explaining why the server did not process the request</returns>
        [HttpGet]
        [ProducesResponseType(typeof(EmployeeResponseDto), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(Error), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(Error), StatusCodes.Status500InternalServerError)]
        public ActionResult<EmployeeResponseDto> GetByID(string pID)
        {
            var dtoAdapter = new EmployeeDtoAdapter();

            var employee = myEmployeeService.GetEmployeeById(pID);

            var response = dtoAdapter.AdaptResponse(employee);

            return Ok(response);
        }

        /// <summary>
        /// Put method to update the info of an employee
        /// </summary>
        /// <param name="pId">Id of the required employee</param>
        /// <param name="pEmployeeDto"> employee to be updated</param>
        /// <returns code= 200> returns the updated employee</returns>
        /// <returns code= 400> returns an error explaining the invalid part of the request</returns>
        /// <returns code= 500> returns an error explaining why the server did not process the request</returns>
        [HttpPut]
        [ProducesResponseType(typeof(EmployeeResponseDto), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(Error), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(Error), StatusCodes.Status500InternalServerError)]
        public ActionResult<EmployeeResponseDto> UpdateEmployee(string pId,EmployeeDto pEmployeeDto)
        {
            var company = myEmployeeService.GetCompanyById(pEmployeeDto.CompanyId);

            var dtoAdapter = new EmployeeDtoAdapter();

            Employee employee = dtoAdapter.Adapt(pEmployeeDto, company);

            var objectresponse=myEmployeeService.UpdateEmployee(pId,employee);

            var response = dtoAdapter.AdaptResponse(objectresponse);

            return Ok(response);

        }
    }
}
