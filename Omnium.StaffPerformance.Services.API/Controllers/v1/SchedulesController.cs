﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Omnium.StaffPerformance.Business.Domain.Entities;
using Omnium.StaffPerformance.Business.Domain.Services.Schedules;
using Omnium.StaffPerformance.Services.API.Adapters;
using Omnium.StaffPerformance.Services.API.Extensions;
using Omnium.StaffPerformance.Services.API.Models;
using System;
using System.Collections.Generic;

namespace Omnium.StaffPerformance.Services.API.Controllers.v1
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class SchedulesController : ControllerBase
    {
        private readonly IScheduleService myService;

        public SchedulesController(IScheduleService pService)
        {
            myService = pService;
        }

        /// <summary>
        /// Get method to receive all the schedules by employee requested
        /// </summary>
        /// <param name="employeeIds">Id's of the employees we want the schedules from (all separated with commas (,)</param>
        /// <param name="date">date of the day we want the schedules to be from</param>
        /// <returns code= 200> a list with the employeeId and all of his schedules</returns>
        /// <returns code= 400> returns an error explaining the invalid part of the request</returns>
        /// <returns code= 500> returns an error explaining why the server did not process the request</returns>
        [HttpGet]
        [ProducesResponseType(typeof(List<ScheduleResponseDto>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(Error), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(Error), StatusCodes.Status500InternalServerError)]
        public ActionResult<List<ScheduleResponseDto>> Get( string employeeIds, [DateFormatValidation(false, "dd/MM/yyyy")] string date)
        {
            var adapter = new ScheduleDtoAdapter();

            var correctedEmployeeIds = employeeIds.Replace(" ", "");

            List<string> listEmployeeIds = new List<string>(correctedEmployeeIds.Split(","));

            DateTime dateParsed = adapter.AdaptDate(date);

            var employeeSchedules = myService.GetSchedules(listEmployeeIds, dateParsed);

            var response = adapter.AdaptListResponse(employeeSchedules);

            return Ok(response);
        }

        /// <summary>
        /// Creates all the schedules sent to this endpoint
        /// </summary>
        /// <param name="pDto">List of Schedules we want to create for individual employees</param>
        /// <param name="companyId">Company we want to create these records in</param>
        /// <param name="storeId">Store we want to create these records for</param>
        /// <returns code= 200> a list with the created schedules for each employee</returns>
        /// <returns code= 400> returns an error explaining the invalid part of the request</returns>
        /// <returns code= 500> returns an error explaining why the server did not process the request</returns>
        [HttpPost]
        [ProducesResponseType(typeof(List<ScheduleResponseDto>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(Error), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(Error), StatusCodes.Status500InternalServerError)]
        public ActionResult<List<ScheduleResponseDto>> Post (List<ScheduleDto> pDto, string companyId, string storeId)
        {
            var adapter = new ScheduleDtoAdapter();

            Dictionary<string, List<Schedule>> listOfResponses = new Dictionary<string, List<Schedule>>();

            var company = myService.GetCompanyById(companyId);
            var store = myService.GetStoreyById(storeId);

            foreach (var scheduleDto in pDto)
            {
                var list = adapter.AdaptScheduleDto(scheduleDto.EmployeeSchedule, company, store, scheduleDto.EmployeeId);

                myService.CreateSchedules(list, scheduleDto.EmployeeId);

                listOfResponses.Add(scheduleDto.EmployeeId, list);
            }

            var response = adapter.AdaptListResponse(listOfResponses);
            return Ok(response);
        }

        /// <summary>
        /// Updates all the schedules sent to this endpoint
        /// </summary>
        /// <param name="pDto">List of Schedules we want to update for individual employees</param>
        /// <param name="companyId">Company we want to update these records in</param>
        /// <param name="storeId">Store we want to update these records for</param>
        /// <returns code= 200> a list with the updated schedules for each employee</returns>
        /// <returns code= 400> returns an error explaining the invalid part of the request</returns>
        /// <returns code= 500> returns an error explaining why the server did not process the request</returns>
        [HttpPut]
        [ProducesResponseType(typeof(List<ScheduleResponseDto>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(Error), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(Error), StatusCodes.Status500InternalServerError)]
        public ActionResult<List<ScheduleResponseDto>> Put(List<ScheduleUpdateDto> pDto, string companyId, string storeId)
        {
            var adapter = new ScheduleDtoAdapter();

            Dictionary<string, List<Schedule>> listOfResponses = new Dictionary<string, List<Schedule>>();

            var company = myService.GetCompanyById(companyId);
            var store = myService.GetStoreyById(storeId);

            foreach (var scheduleDto in pDto)
            {
                var list = adapter.AdaptUpdateScheduleDto(scheduleDto.EmployeeSchedule, company, store, scheduleDto.EmployeeId);

                var updatedSchedules=myService.UpdateSchedules(list);

                listOfResponses.Add(scheduleDto.EmployeeId, updatedSchedules);
            }

            var response = adapter.AdaptListResponse(listOfResponses);
            return Ok(response);
        }
    }
}
