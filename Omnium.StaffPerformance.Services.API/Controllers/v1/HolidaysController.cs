﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Omnium.StaffPerformance.Business.Domain.Services.Holidays;
using Omnium.StaffPerformance.Services.API.Adapters;
using Omnium.StaffPerformance.Services.API.Models;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace Omnium.StaffPerformance.Services.API.Controllers.v1
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class HolidaysController : ControllerBase
    {

        private readonly IHolidayService myHolidayService;

        public HolidaysController(IHolidayService pHolidayService)
        {
            myHolidayService = pHolidayService;
        }

        /// <summary>
        /// Post endpoint to import the holidays from nager and add them to the database
        /// </summary>
        /// <param name="year">year we want the holidays to be from</param>
        /// <param name="countryId">country we want the holidays to be from</param>
        /// <returns code= 200> returns the number of holidays created</returns>
        /// <returns code= 400> returns an error explaining the invalid part of the request</returns>
        /// <returns code= 404> returns an error due to not finding the gateway endpoint</returns>
        /// <returns code= 500> returns an error explaining why the server did not process the request</returns>
        [HttpPost("Import")]
        [ProducesResponseType(typeof(int), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(Error), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(Error), StatusCodes.Status404NotFound)]
        [ProducesResponseType(typeof(Error), StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<int>> GetImportAsync(int year, string countryName)
        {
            var amount = await myHolidayService.ImportHolidaysAsync(year, countryName);
            return Ok(amount);
        }
        /// <summary>
        /// Creates an Holiday in the database
        /// </summary>
        /// <param name="pDto">data transfer object with the holiday information</param>
        /// <returns code= 200> returns the created Holiday's information</returns>
        /// <returns code= 400> returns an error explaining the invalid part of the request</returns>
        /// <returns code= 500> returns an error explaining why the server did not process the request</returns>
        [HttpPost]
        [ProducesResponseType(typeof(HolidayResponseDto), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(Error), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(Error), StatusCodes.Status500InternalServerError)]
        public ActionResult<HolidayResponseDto> Post(HolidayDto pDto)
        {
            var dtoAdapter = new HolidayDtoAdapter();

            var holiday = dtoAdapter.Adapt(pDto);

            myHolidayService.AddHoliday(holiday);

            var response = dtoAdapter.AdaptResponse(holiday);

            return Ok(response);
        }

        /// <summary>
        /// Gets an Holiday by its Id
        /// </summary>
        /// <param name="id">Id of the required Holiday</param>
        /// <returns code= 200> returns the requested holiday</returns>
        /// <returns code= 400> returns an error explaining the invalid part of the request</returns>
        /// <returns code= 500> returns an error explaining why the server did not process the request</returns>
        [HttpGet]
        [Route("{id}")]
        [ProducesResponseType(typeof(HolidayResponseDto), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(Error), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(Error), StatusCodes.Status500InternalServerError)]
        public ActionResult<HolidayResponseDto> GetByID(string id)
        {
            var dtoAdapter = new HolidayDtoAdapter();

            var holiday = myHolidayService.GetHolidayById(id);

            var response = dtoAdapter.AdaptResponse(holiday);

            return Ok(response);
        }

        /// <summary>
        /// Gets all Holidays
        /// </summary>
        /// <returns code= 200> returns all the holidays</returns>
        /// <returns code= 400> returns an error explaining the invalid part of the request</returns>
        /// <returns code= 500> returns an error explaining why the server did not process the request</returns>
        [HttpGet]
        [ProducesResponseType(typeof(List<HolidayResponseDto>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(Error), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(Error), StatusCodes.Status500InternalServerError)]
        public ActionResult<List<HolidayResponseDto>> Get()
        {
            var dtoAdapter = new HolidayDtoAdapter();

            var holiday = myHolidayService.GetHolidays();

            var response = dtoAdapter.AdaptListResponse(holiday);

            return Ok(response);
        }

        /// <summary>
        /// Updates an Holiday
        /// </summary>
        /// <returns code= 200> returns the updated holiday</returns>
        /// <returns code= 400> returns an error explaining the invalid part of the request</returns>
        /// <returns code= 500> returns an error explaining why the server did not process the request</returns>
        [HttpPut]
        [ProducesResponseType(typeof(HolidayResponseDto), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(Error), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(Error), StatusCodes.Status500InternalServerError)]
        public ActionResult<HolidayResponseDto> Put(string id,HolidayDto pDto)
        {
            var dtoAdapter = new HolidayDtoAdapter();

            var holiday = dtoAdapter.Adapt(pDto);

            var holidayUpdated = myHolidayService.UpdateHoliday(id, holiday);

            var response = dtoAdapter.AdaptResponse(holidayUpdated);

            return Ok(response);
        }

    }
}
