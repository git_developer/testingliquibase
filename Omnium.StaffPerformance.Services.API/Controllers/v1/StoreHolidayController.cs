﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Omnium.StaffPerformance.Business.Domain.Services.Holidays;
using Omnium.StaffPerformance.Services.API.Adapters;
using Omnium.StaffPerformance.Services.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Omnium.StaffPerformance.Services.API.Controllers.v1
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class StoreHolidayController : ControllerBase
    {
        private readonly IHolidayService myHolidayService;

        public StoreHolidayController(IHolidayService pHolidayService)
        {
            myHolidayService = pHolidayService;
        }

        /// <summary>
        /// Associates an holiday to a store
        /// </summary>
        /// <returns code= 200> returns the created StoreHoliday</returns>
        /// <returns code= 400> returns an error explaining the invalid part of the request</returns>
        /// <returns code= 500> returns an error explaining why the server did not process the request</returns>
        [HttpPut]
        [ProducesResponseType(typeof(StoreHolidayDto), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(Error), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(Error), StatusCodes.Status500InternalServerError)]
        public ActionResult<StoreHolidayDto> PutStoreHoliday(string storeId, string holidayId)
        {
            var dtoAdapter = new HolidayDtoAdapter();

            var sh = myHolidayService.AssociateHolidayToStore(storeId, holidayId);

            var response = dtoAdapter.AdaptStoreHoliday(sh);

            return Ok(response);
        }
    }
}
