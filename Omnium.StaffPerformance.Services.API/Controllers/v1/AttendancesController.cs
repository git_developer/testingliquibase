﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Omnium.StaffPerformance.Business.Domain.Services.Attendances;
using Omnium.StaffPerformance.Services.API.Adapters;
using Omnium.StaffPerformance.Services.API.Extensions;
using Omnium.StaffPerformance.Services.API.Models;
using System.Collections.Generic;

namespace Omnium.StaffPerformance.Services.API.Controllers.v1
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class AttendancesController : ControllerBase
    {

        private readonly IAttendanceService myAttendanceService;

        public AttendancesController(IAttendanceService pService)
        {
            myAttendanceService = pService;
        }
        /// <summary>
        /// Post method to create an attendance registry
        /// </summary>
        /// <param name="attendanceDto">attendance to be added</param>
        /// <returns code= 200> returns the created attendance</returns>
        /// <returns code= 400> returns an error explaining the invalid part of the request</returns>
        /// <returns code= 500> returns an error explaining why the server did not process the request</returns>
        [HttpPost]
        [ProducesResponseType(typeof(AttendanceResponseDto), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(Error), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(Error), StatusCodes.Status500InternalServerError)]
        public ActionResult<AttendanceResponseDto> Post(AttendanceDto attendanceDto)
        {
            var company = myAttendanceService.GetCompanyById(attendanceDto.CompanyId);
            var dtoAdapter = new AttendanceDtoAdapter();

            var attendance = dtoAdapter.Adapt(attendanceDto, company);

            myAttendanceService.AddAttendance(attendance);

            var response = dtoAdapter.AdaptResponse(attendance);

            return Ok(response);
        }
        /// <summary>
        /// Get method to receive all attendances by an employee in a given interval
        /// </summary>
        /// <param name="employeeId">id of the employee</param>
        /// <param name="startDate">datetime for the start of the interval</param>
        /// <param name="finishDate">datetime for the end of the interval</param>
        /// <returns code= 200> returns the requested attendance(s)</returns>
        /// <returns code= 400> returns an error explaining the invalid part of the request</returns>
        /// <returns code= 500> returns an error explaining why the server did not process the request</returns>
        [HttpGet]
        [ProducesResponseType(typeof(List<AttendanceResponseDto>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(Error), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(Error), StatusCodes.Status500InternalServerError)]
        public ActionResult<List<AttendanceResponseDto>> Get(string employeeId, [DateFormatValidation(false, "dd/MM/yyyy-HH:mm:ss")] string startDate, [DateFormatValidation(false, "dd/MM/yyyy-HH:mm:ss")] string finishDate)
        {
            var dtoAdapter = new AttendanceDtoAdapter();

            var responseList= myAttendanceService.GetAttendancesByInterval(dtoAdapter.AdaptToDateTimeFormat(startDate), dtoAdapter.AdaptToDateTimeFormat(finishDate), employeeId);

            var response = dtoAdapter.AdaptListResponse(responseList);

            return Ok(response);
        }
    }
}
