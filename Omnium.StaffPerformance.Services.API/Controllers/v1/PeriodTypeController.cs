﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Omnium.StaffPerformance.Business.Domain.Entities;
using Omnium.StaffPerformance.Business.Domain.Services.PeriodTypes;
using Omnium.StaffPerformance.Services.API.Adapters;
using Omnium.StaffPerformance.Services.API.Models;
using System.Collections.Generic;

namespace Omnium.StaffPerformance.Services.API.Controllers.v1
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class PeriodTypeController : ControllerBase
    {

        private readonly IPeriodTypeService myService;

        public PeriodTypeController(IPeriodTypeService pService)
        {
            myService = pService;
        }
        /// <summary>
        /// Post endpoint to create a Period Type
        /// </summary>
        /// <param name="pPeriodTypeDto">data transfer object of the period type to create</param>
        /// <returns code= 200> returns the created period Type</returns>
        /// <returns code= 400> returns an error explaining the invalid part of the request</returns>
        /// <returns code= 500> returns an error explaining why the server did not process the request</returns>
        [HttpPost]
        [ProducesResponseType(typeof(PeriodTypeResponseDto), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(Error), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(Error), StatusCodes.Status500InternalServerError)]
        public ActionResult<PeriodTypeResponseDto> Post(PeriodTypeDto pPeriodTypeDto)
        {
            var company = myService.GetCompanyById(pPeriodTypeDto.CompanyId);
            var dtoAdapter = new PeriodTypeDtoAdapter();

            PeriodType periodType = dtoAdapter.Adapt(pPeriodTypeDto, company);

            myService.AddPeriodType(periodType);

            var response = dtoAdapter.AdaptResponse(periodType);

            return Ok(response);
        }
        /// <summary>
        /// Get endpoint to return a Period Type
        /// </summary>
        /// <param name="id">id of the period type we want to fetch</param>
        /// <returns code= 200> returns the existing period Type with that id</returns>
        /// <returns code= 400> returns an error explaining the invalid part of the request</returns>
        /// <returns code= 500> returns an error explaining why the server did not process the request</returns>
        [HttpGet]
        [Route("{id}")]
        [ProducesResponseType(typeof(PeriodTypeResponseDto), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(Error), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(Error), StatusCodes.Status500InternalServerError)]
        public ActionResult<PeriodTypeResponseDto> GetById(string id)
        {
            var dtoAdapter = new PeriodTypeDtoAdapter();

            var result=myService.GetPeriodTypeById(id);

            var response = dtoAdapter.AdaptResponse(result);

            return Ok(response);
        }
        /// <summary>
        /// Get endpoint to return all Period Types
        /// </summary>
        /// <returns code= 200> returns all the existing period Types</returns>
        /// <returns code= 400> returns an error explaining the invalid part of the request</returns>
        /// <returns code= 500> returns an error explaining why the server did not process the request</returns>
        [HttpGet]
        [ProducesResponseType(typeof(List<PeriodTypeResponseDto>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(Error), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(Error), StatusCodes.Status500InternalServerError)]
        public ActionResult<List<PeriodTypeResponseDto>> Get()
        {
            var dtoAdapter = new PeriodTypeDtoAdapter();

            var result = myService.GetPeriodTypes();

            var response = dtoAdapter.AdaptListResponse(result);

            return Ok(response);
        }
        /// <summary>
        /// Put endpoint to update a Period Type
        /// </summary>
        /// <param name="id">id of the period type we want to update</param>
        /// <param name="pDto">data transfer object of the period type to update</param>
        /// <returns code= 200> returns the updated period Type with that id</returns>
        /// <returns code= 400> returns an error explaining the invalid part of the request</returns>
        /// <returns code= 500> returns an error explaining why the server did not process the request</returns>
        [HttpPut]
        [ProducesResponseType(typeof(PeriodTypeResponseDto), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(Error), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(Error), StatusCodes.Status500InternalServerError)]
        public ActionResult<PeriodTypeResponseDto> Put(string id, PeriodTypeDto pDto)
        {
            var company = myService.GetCompanyById(pDto.CompanyId);
            var dtoAdapter = new PeriodTypeDtoAdapter();

            PeriodType periodType = dtoAdapter.Adapt(pDto, company);

            var result = myService.UpdatePeriodType(id, periodType);

            var response = dtoAdapter.AdaptResponse(result);

            return Ok(response);
        }
    }
}
