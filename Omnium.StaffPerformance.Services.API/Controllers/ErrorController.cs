﻿using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Omnium.StaffPerformance.Business.Exceptions;
using Omnium.StaffPerformance.Services.API.Models;
using System;

namespace Omnium.StaffPerformance.Services.API.Controllers
{
    [Route("Error")]
    [ApiController]
    [ApiExplorerSettings(IgnoreApi = true)]
    public class ErrorController : ControllerBase
    {

        public ActionResult<ErrorController> Error()
        {
            var context = HttpContext.Features.Get<IExceptionHandlerFeature>();
            var exception = context.Error;
            if(exception is AggregateException)
            {
                exception = exception.InnerException;
            }
            var error = new Error();
            ObjectResult response;

            switch (exception)
            {
                case ArgumentNullException:
                    
                    error.Code = (int)ECode.RequiredField;
                    error.Message = exception.Message;
                    response = StatusCode(400, error);
                    break;

                case EntityXAlreadyExistsCustomException:
                    var ex = exception as CustomException;
                    error.Code = (int)ECode.EntityXAlreadyExists;
                    error.Message = ex.Message;
                    response = StatusCode(202,error);
                    break;

                case AssociationXAlreadyExistsCustomException:
                    ex = exception as CustomException;
                    error.Code = (int)ECode.EntityXAlreadyExists;
                    error.Message = ex.Message;
                    response = StatusCode(202, error);
                    break;

                case WrongDateFormatCustomException:
                    ex = exception as CustomException;
                    error.Code = (int)ECode.WrongDateFormat;
                    error.Message = ex.Message;
                    response = StatusCode(400, error);
                    break;

                case EntityXNotFoundCustomException:
                    ex = exception as CustomException;
                    error.Code = (int)ECode.EntityXNotFound;
                    error.Message = ex.Message;
                    response = StatusCode(400, error);
                    break;

                case POSCustomException:
                    ex = exception as CustomException;
                    error.Code = (int)ECode.POSError;
                    error.Message = ex.Message;
                    response = StatusCode(500, error);
                    break;

                case ExternalAPICustomException:
                    ex = exception as CustomException;
                    error.Code = (int)ECode.ExtAPIError;
                    error.Message = ex.Message;
                    response = StatusCode(500, error);
                    break;

                case InvalidAttendanceTypeCustomException:
                    ex = exception as CustomException;
                    error.Code = (int)ECode.InvalidAttendanceType;
                    error.Message = ex.Message;
                    response = StatusCode(400, error);
                    break;

                case SameAttendanceTypeCustomException:
                    ex = exception as CustomException;
                    error.Code = (int)ECode.SameAttendanceType;
                    error.Message = ex.Message;
                    response = StatusCode(400, error);
                    break;

                default:
                    error.Code = (int)ECode.Unexpected;
                    error.Message = exception.Message;
                    return StatusCode(500, error);
            }


            return response;
        }

    }
}
