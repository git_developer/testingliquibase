﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Omnium.StaffPerformance.Business.Domain.Contracts;
using Omnium.StaffPerformance.Business.Domain.Repositories;
using Omnium.StaffPerformance.Business.Domain.Services.Attendances;
using Omnium.StaffPerformance.Business.Domain.Services.Companies;
using Omnium.StaffPerformance.Business.Domain.Services.Countries;
using Omnium.StaffPerformance.Business.Domain.Services.Employees;
using Omnium.StaffPerformance.Business.Domain.Services.Holidays;
using Omnium.StaffPerformance.Business.Domain.Services.PeriodTypes;
using Omnium.StaffPerformance.Business.Domain.Services.Platforms;
using Omnium.StaffPerformance.Business.Domain.Services.Schedules;
using Omnium.StaffPerformance.Data.AWS.SecretManagers;
using Omnium.StaffPerformance.Data.Nager.Holidays;
using Omnium.StaffPerformance.Data.Postgres.Context;
using Omnium.StaffPerformance.Data.Postgres.Repositories;
using Omnium.StaffPerformance.Data.Sitoo.Employees;

namespace Omnium.StaffPerformance.Services.API.Extensions
{
    public static class LocalServicesExtensions
    {
        /// <summary>
        /// Loads the local services for the application
        /// </summary>
        /// <param name="pServices">Collection of all services</param>
        /// <param name="pConfiguration">Configuration file</param>
        public static void AddLocalServices(this IServiceCollection pServices, IConfiguration pConfiguration)
        {
            pServices.AddGlobalModelValidation();
            pServices.AddDbContext<PostgresContext>(opts => opts.UseLazyLoadingProxies().UseNpgsql(pConfiguration["ConnectionString"]));
            pServices.AddSingleton<ISecretManagerGateway>(m => new AWSSecretManagerCacheGateway(pConfiguration.GetSection("SecretManager").Value));
            pServices.AddTransient<IUnitOfWork, UnitOfWork>();
            pServices.AddTransient<IEmployeeService, EmployeeService>();
            pServices.AddTransient<IAttendanceService, AttendanceService>();
            pServices.AddTransient<IHolidayService, HolidayService>();
            pServices.AddTransient<IScheduleService, ScheduleService>();
            pServices.AddTransient<IHolidayGateway, NagerHolidayGateway>(m=> new NagerHolidayGateway(pConfiguration.GetSection("NagerAPI").Value));
            pServices.AddTransient<IPeriodTypeService, PeriodTypeService>();
            pServices.AddTransient<ICompanyService, CompanyService>();
            pServices.AddTransient<IPlatformService, PlatformService>();
            pServices.AddTransient<ICountryService, CountryService>();
            pServices.AddTransient<IUserGateway, SitooUserGateway>(m=>
            {
                var secrets = m.GetService<ISecretManagerGateway>();

                return new SitooUserGateway(secrets.GetSecretAsync("SitooBaseURL").Result, secrets.GetSecretAsync("SitooAPIKey").Result);
            });
        }

    }
}
