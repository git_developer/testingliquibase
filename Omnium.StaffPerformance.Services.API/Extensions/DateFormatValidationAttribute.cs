﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Globalization;

namespace Omnium.StaffPerformance.Services.API.Extensions
{
    /// <summary>
    /// Validates the Date's format, passed in the constructor.
    /// Also allows for nullable attributes
    /// </summary>
    public class DateFormatValidationAttribute : ValidationAttribute
    {

        private readonly bool myAllowNull;
        private readonly string myDateFormat;

        public DateFormatValidationAttribute(bool pAllowNull, string pDateFormat)
        {
            myAllowNull = pAllowNull;
            myDateFormat = pDateFormat;
        }

        public override bool IsValid(object value)
        {

            if (myAllowNull && String.IsNullOrEmpty((string)value))
            {
                return true;
            }

            DateTime dt;
            return DateTime.TryParseExact((string)value, myDateFormat, CultureInfo.InvariantCulture, DateTimeStyles.None, out dt);
        }

        public override string FormatErrorMessage(string name)
        {
            return $"The attribute {name} is not in the correct format {myDateFormat}";
        }

    }
}
