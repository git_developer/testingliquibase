﻿using MassTransit;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Omnium.StaffPerformance.Services.API.Consumers.EventCarriedStateTransfer;


namespace Omnium.StaffPerformance.Services.API.Extensions
{
    public static class MassTransitExtension
    {
        public static void AddMassTransit(this IServiceCollection pServices, IConfiguration pConfiguration)
        {
            pServices.AddMassTransit(x =>
            {
                x.SetSnakeCaseEndpointNameFormatter();
                x.AddConsumersFromNamespaceContaining(typeof(CompanyConsumer));
                x.UsingRabbitMq((context, configuration) =>
                {
                    configuration.ConfigureEndpoints(context);
                    configuration.Host(pConfiguration.GetValue<string>("Rabbit:hostname"), "/", h =>
                    {
                        h.Username(pConfiguration.GetValue<string>("Rabbit:username"));
                        h.Password(pConfiguration.GetValue<string>("Rabbit:password"));
                    });

                });
            });

            pServices.AddMassTransitHostedService();
        }


    }
}
