﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Globalization;

namespace Omnium.StaffPerformance.Services.API.Extensions
{
    /// <summary>
    /// Validates if a datetime corresponds to the standard timespan passed in the constructor (in minutes)
    /// </summary>
    public class TimespanValidationAttribute : ValidationAttribute
    {

        private readonly int myTimespan;


        public TimespanValidationAttribute(int pTimespan)
        {
            myTimespan = pTimespan;
        }

        public override bool IsValid(object value)
        {
            DateTime dt = DateTime.ParseExact((string)value, AvailableDateFormatsExtension.GetDateTimeFormat(), CultureInfo.InvariantCulture);

            var minutes = dt.Minute;

            return minutes % myTimespan==0;

        }

        public override string FormatErrorMessage(string name)
        {
            return $"The attribute {name} has to be in intervals of {myTimespan} minutes";
        }
    }
}
