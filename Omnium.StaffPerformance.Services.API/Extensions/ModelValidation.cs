﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.Extensions.DependencyInjection;
using Omnium.StaffPerformance.Business.Exceptions;
using Omnium.StaffPerformance.Services.API.Models;
using System.Linq;

namespace Omnium.StaffPerformance.Services.API.Extensions
{
    public static class ModelValidation
    {

        /// <summary>
        /// Adds a global model validation to the API pipeline. 
        /// </summary>
        /// <param name="pServices"></param>
        public static void AddGlobalModelValidation(this IServiceCollection pServices)
        {
            pServices.Configure<ApiBehaviorOptions>(options =>
            {
                options.InvalidModelStateResponseFactory = context =>
                {
                    var error = context.ModelState.GetError();
                    return new BadRequestObjectResult(error);
                };
            });
        }

        private static Error GetError(this ModelStateDictionary pModelStateDictionary)
        {
            var error = new Error();

            var firstError = pModelStateDictionary.Values.FirstOrDefault(c => c.Errors.Count > 0);
            var errorValue = firstError.Errors.FirstOrDefault();

            if (errorValue.ErrorMessage.Contains("required"))
            {
                error.Message = errorValue.ErrorMessage;
                error.Code = (int)ECode.RequiredField;
            }
            else
            {
                error.Message = errorValue.ErrorMessage;
                error.Code = (int)ECode.InvalidField;
            }

            return error;
        }

    }
}
