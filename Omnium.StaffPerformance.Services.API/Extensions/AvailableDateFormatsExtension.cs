﻿
namespace Omnium.StaffPerformance.Services.API.Extensions
{
    public static class AvailableDateFormatsExtension
    {

        /// <summary>
        /// Returns the correct format for only date attributes
        /// </summary>
        /// <returns>the correct format for only date attributes </returns>
        public static string GetDateFormat()
        {
            return "dd/MM/yyyy";
        }
        /// <summary>
        /// Returns the correct format for date and time attributes
        /// </summary>
        /// <returns>the correct format for date and time attributes</returns>
        public static string GetDateTimeFormat()
        {
            return "dd/MM/yyyy-HH:mm:ss";
        }
    }
}
