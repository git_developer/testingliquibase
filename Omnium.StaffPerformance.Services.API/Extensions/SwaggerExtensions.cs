﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;

namespace Omnium.StaffPerformance.Services.API.Extensions
{
    public static class SwaggerExtensions
    {

        /// <summary>
        /// Adds the swagger documentation generator
        /// </summary>
        /// <param name="pServices"></param>
        public static void AddSwaggerGenerator(this IServiceCollection pServices)
        {
            pServices.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Omnium.StaffPerformance.Services.API", Version = "v1" });
            });
        }

        /// <summary>
        /// Enables middleware to serve generated Swagger as a JSON endpoint and with a UI.
        /// </summary>
        /// <param name="pApp"></param>
        public static void UseSwaggerWithUI(this IApplicationBuilder pApp)
        {
            pApp.UseSwagger();
            pApp.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Omnium.StaffPerformance.Services.API v1"));
        }

    }
}
